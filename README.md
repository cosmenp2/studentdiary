# Student diary

Student diary is **now** a project in development with the objective of helping students to manage their grades and their homework in an easy and simple way.

<img src="android.png" alt="Student diary" width="300px" /> <img src="android2.png" alt="Student diary" width="300px" />

# Actual state

The actual project is a *working* android app that manage school years, evaluations, subjects, blocks, sub blocks and different types of elements. Their structure, which can be seen in the [documentation](./documentation/Student\ diary documentation.pdf), is very complex for developing and for the final user. The actual goal is simplify the application to achieve a simple management of subjects, grades and task that can be easily understand the final user.

Also we want to make this app multi-platform with **Kotlin multi-platform**. This require a deep redesign to make the architecture fully compatible with Kotlin platform with the recommended practices. And on the way we can learn more about this awesome language and technologies.

# Contribute

If you want to contribute you're welcome. You can participate in issues and collaborate to the project doing a fork of the project, doing your changes based on the develop branch and opening a pull request with the issue reference. Don't forget to tell us in what issue are you working!!

Note that now we are working in design and with architecture so the code style are the best practices recommended by Kotlin, Google and IOs. We appreciate your contribution and if you want you will be able to sign off your contribution but for now we have to prepare this system too.

Probably the license will change after the redesign to the [APACHEv2.0](https://www.apache.org/licenses/LICENSE-2.0)
so all of us will can use the code for any purpose without the restrictions of the GPL license.

In the end Thank you for help to improve this project!!

Have a nice day!! - @cosmenp2 (Maintainer of the project)