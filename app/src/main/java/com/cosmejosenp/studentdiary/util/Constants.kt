/*
 * MIT License
 *
 * Copyright (c) 2022 Cosme José Nieto Pérez
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.cosmejosenp.studentdiary.util

const val TYPE_EXAM = 1
const val TYPE_WORK = 2
const val TYPE_HOMEWORK = 3
const val TYPE_OTHER = 10
const val LINEAR_LAYOUT_ELEMENTS = 1
const val GRID_LAYOUT_ELEMENTS = 2

interface DetailSchoolYearFragmentConstants {
    val ARG_OBJECT: String
        get() = "schoolYear"
}

interface DetailEvaluationFragmentConstants {
    val ARG_OBJECT: String
        get() = "evaluation"
}

interface DetailSubjectFragmentConstants {
    val ARG_OBJECT: String
        get() = "subject"
}

interface DetailSubSubjectFragmentConstants {
    val ARG_OBJECT: String
        get() = "subSubject"
}

interface EditBlockDialogConstants {
    val ARG_SUB_SUBJECT: String
        get() = "subSubject"
    val ARG_BLOCK: String
        get() = "block"
    val ARG_SUB_BLOCK: String
        get() = "subBlock"
    val ARG_COLOR: String
        get() = "color"
}

interface PageListSchoolYearConstants {
    val ARG_OBJECT: String
        get() = "active"
}

interface PageDetailSchoolYearConstants {
    val ARG_OBJECT: String
        get() = "schoolYear"
}

interface PageDetailEvaluationConstants {
    val ARG_OBJECT: String
        get() = "evaluation"
}

interface PageDetailSubjectConstants {
    val ARG_OBJECT: String
        get() = "subject"
}

interface PageDetailSubSubjectConstants {
    val ARG_OBJECT: String
        get() = "subSubject"
}

interface PageDetailBlockConstants {
    val ARG_OBJECT: String
        get() = "block"
}

interface PageElementConstants {
    val ARG_TYPE_LIST: String
        get() = "typeList"
    val ARG_SUBJECT: String
        get() = "subject"
    val ARG_SUB_SUBJECT: String
        get() = "subSubject"
    val ARG_BLOCK: String
        get() = "block"
    val ARG_SUB_BLOCK: String
        get() = "subBlock"
    val ARG_TYPED_BLOCK: String
        get() = "typedBlock"
}

enum class ListElementsTypes {
    ACTIVE, PENDING, COMPLETED, PENDING_GRADES
}