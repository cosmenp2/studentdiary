/*
 * MIT License
 *
 * Copyright (c) 2022 Cosme José Nieto Pérez
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */



package com.cosmejosenp.studentdiary.util

import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.cosmejosenp.studentdiary.R
import java.util.*

/**
 * Adapt the start date
 */
@BindingAdapter("startDateString")
fun TextView.setStartDateString(item: Calendar?) {
    if (item != null) {
        text = context.getString(R.string.startDate, formatDate(item, context.resources))
        visibility = VISIBLE
    } else {
        visibility = GONE
    }
}

/**
 * Adapt the finish date
 */
@BindingAdapter("finishDateString")
fun TextView.setFinishDateString(item: Calendar?) {
    if (item != null) {
        text =
            context.getString(
                R.string.finishDate,
                formatDate(item, context.resources)
            )
        visibility = VISIBLE
    } else {
        visibility = GONE
    }
}

/**
 * Adapt the delivery date
 */
@BindingAdapter("deliveryDateString")
fun TextView.setDeliveryDateString(item: Calendar?) {
    if (item != null) {
        text = context.getString(
            R.string.deliveryDateWithDate,
            "${formatDate(item, context.resources)} " +
                    String.format(
                        "%02d:%02d",
                        item.get(Calendar.HOUR_OF_DAY),
                        item.get(Calendar.MINUTE)
                    )
        )
        visibility = VISIBLE
    } else {
        visibility = GONE
    }
}

/**
 * Adapt the delivered date
 */
@BindingAdapter("completedDateString")
fun TextView.setCompletedDateString(item: Calendar?) {
    if (item != null) {
        text = context.getString(
            R.string.deliveredDateWithDate,
            "${formatDate(item, context.resources)} " +
                    String.format(
                        "%02d:%02d",
                        item.get(Calendar.HOUR_OF_DAY),
                        item.get(Calendar.MINUTE)
                    )
        )
        visibility = VISIBLE
    } else {
        visibility = GONE
    }
}

/**
 * Adapt the integer type color of subject to a backgroundColor for views
 */
@BindingAdapter("typeToBackgroundColor")
fun View.setBackgroundColorByType(type: Int) {
    val color = obtainColour(type, context.resources, context)
    setBackgroundColor(color.color)
}

/**
 * Adapt the type of the elements, block and subBlock
 */
@BindingAdapter("elementType")
fun ImageView.setElementTypeImage(type: Int?) {
    if (type != null) {
        val imageType = obtainImageType(type, context.resources, context)
        setImageDrawable(imageType.icon)
        contentDescription = imageType.description
        visibility = VISIBLE
    } else
        visibility = GONE
}

/**
 * Adapt the type of the elements, block and subBlock
 */
@BindingAdapter("percent")
fun TextView.setPercentString(percent: Int?) {
    if (percent != null) {
        text = context.getString(R.string.percent, percent)
        visibility = VISIBLE
    } else {
        visibility = GONE
    }
}

/**
 * Adapt the text with visibility changes
 */
@BindingAdapter("textAndVisibility")
fun TextView.setTextString(textString: String?) {
    if (textString != null && textString.isNotEmpty()) {
        text = textString
        visibility = VISIBLE
    } else {
        visibility = GONE
    }
}