/*
 * MIT License
 *
 * Copyright (c) 2022 Cosme José Nieto Pérez
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */



package com.cosmejosenp.studentdiary.util

import android.content.Context
import android.content.res.Resources
import androidx.core.content.ContextCompat
import com.cosmejosenp.studentdiary.R
import java.util.*
import kotlin.collections.ArrayList

/**
 * Transform a calendar date to a string according to the translate of regional date
 */
fun formatDate(date: Calendar, resources: Resources): String {
    return resources.getString(
        R.string.dateFormatted,
        date.get(Calendar.MONTH) + 1,
        date.get(Calendar.DAY_OF_MONTH),
        date.get(Calendar.YEAR)
    )
}

/**
 * @return all list of colours available
 */
fun obtainListOfColours(resources: Resources, context: Context): MutableList<Colour>? {
    val colours = ArrayList<Colour>()
    for (i in 0..10) {
        colours.add(obtainColour(i, resources, context))
    }
    return colours.toMutableList()
}

/**
 * @return the color class depending of the type
 */
fun obtainColour(type: Int, resources: Resources, context: Context): Colour =
    when (type) {
        1 -> Colour(
            ContextCompat.getColor(context, R.color.palette1),
            resources.getString(R.string.salmonColour)
        )
        2 -> Colour(
            ContextCompat.getColor(context, R.color.palette2),
            resources.getString(R.string.pinkColour)
        )
        3 -> Colour(
            ContextCompat.getColor(context, R.color.palette3),
            resources.getString(R.string.violetColour)
        )
        4 -> Colour(
            ContextCompat.getColor(context, R.color.palette4),
            resources.getString(R.string.blueColour)
        )
        5 -> Colour(
            ContextCompat.getColor(context, R.color.palette5),
            resources.getString(R.string.greenColour)
        )
        6 -> Colour(
            ContextCompat.getColor(context, R.color.palette6),
            resources.getString(R.string.yellowColour)
        )
        7 -> Colour(
            ContextCompat.getColor(context, R.color.palette7),
            resources.getString(R.string.amberColour)
        )
        8 -> Colour(
            ContextCompat.getColor(context, R.color.palette8),
            resources.getString(R.string.orangeColour)
        )
        9 -> Colour(
            ContextCompat.getColor(context, R.color.palette9),
            resources.getString(R.string.deepOrangeColour)
        )
        10 -> Colour(
            ContextCompat.getColor(context, R.color.palette10),
            resources.getString(R.string.grayColour)
        )
        else -> Colour(
            ContextCompat.getColor(context, R.color.palette0),
            resources.getString(R.string.defaultColour)
        )
    }

/**
 * @return the type of color depending of the color of the object [colour]
 */
fun obtainTypeColour(colour: Colour, context: Context): Int =
    when (colour.color) {
        ContextCompat.getColor(context, R.color.palette1) -> 1
        ContextCompat.getColor(context, R.color.palette2) -> 2
        ContextCompat.getColor(context, R.color.palette3) -> 3
        ContextCompat.getColor(context, R.color.palette4) -> 4
        ContextCompat.getColor(context, R.color.palette5) -> 5
        ContextCompat.getColor(context, R.color.palette6) -> 6
        ContextCompat.getColor(context, R.color.palette7) -> 7
        ContextCompat.getColor(context, R.color.palette8) -> 8
        ContextCompat.getColor(context, R.color.palette9) -> 9
        ContextCompat.getColor(context, R.color.palette10) -> 10
        else -> 0
    }

/**
 * @return the ImageType class depending of the type of the content
 */
fun obtainImageType(type: Int, resources: Resources, context: Context): ImageType =
    when (type) {
        TYPE_EXAM -> ImageType(
            ContextCompat.getDrawable(context, R.drawable.ic_exam_black_24dp),
            resources.getString(R.string.examsName)
        )
        TYPE_WORK -> ImageType(
            ContextCompat.getDrawable(context, R.drawable.ic_work_black_24dp),
            resources.getString(R.string.worksName)
        )
        TYPE_HOMEWORK -> ImageType(
            ContextCompat.getDrawable(context, R.drawable.ic_home_work_black_24dp),
            resources.getString(R.string.homeworksName)
        )
        else -> ImageType(
            ContextCompat.getDrawable(context, R.drawable.ic_unknown_black_24dp),
            resources.getString(R.string.otherName)
        )
    }

/**
 * @return the type of content depending of the icon [imageType]
 */
fun obtainTypeImage(imageType: ImageType, context: Context): Int =
    when (imageType.icon) {
        ContextCompat.getDrawable(context, R.drawable.ic_exam_black_24dp) -> TYPE_EXAM
        ContextCompat.getDrawable(context, R.drawable.ic_work_black_24dp) -> TYPE_WORK
        ContextCompat.getDrawable(context, R.drawable.ic_home_work_black_24dp) -> TYPE_HOMEWORK
        else -> TYPE_OTHER
    }