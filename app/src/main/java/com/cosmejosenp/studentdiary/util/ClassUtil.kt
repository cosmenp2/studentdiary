/*
 * MIT License
 *
 * Copyright (c) 2022 Cosme José Nieto Pérez
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */



package com.cosmejosenp.studentdiary.util

import android.graphics.drawable.Drawable
import androidx.core.widget.NestedScrollView
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton

/**
 * To hide an show the fab icon when the nestedScrollView is scrolled
 */
class NestedScrollListener(private val fab: FloatingActionButton) :
    NestedScrollView.OnScrollChangeListener {

    override fun onScrollChange(
        v: NestedScrollView?,
        scrollX: Int,
        scrollY: Int,
        oldScrollX: Int,
        oldScrollY: Int
    ) {
        if (scrollY > oldScrollY) {
            fab.hide()
        } else {
            fab.show()
        }
    }
}

/**
 * To notify hide an show the fab icon to the parent when the recyclerView is scrolled
 */
class RecyclerViewListener(private val scrollListener: ScrollListener) :
    RecyclerView.OnScrollListener() {
    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        super.onScrolled(recyclerView, dx, dy)
        scrollListener.hideButton(dy > 0)
    }
}

/**
 * Interface to implements the communication between the fragment and the adapter when the action mode
 * is activated
 */
interface ListenersOfActionMode<T> {
    val selectAll: MutableLiveData<Boolean>
    val actionModeEnabled: MutableLiveData<Boolean>
    var selectedItems: ArrayList<T>

    fun activateActionMode()
    fun actionModeDone()
    fun changeSelectedAllTo(value: Boolean)
    fun activateActionMode(items: ArrayList<T>)
}

/**
 * Interface to implement if the adapter have to change the background colour
 */
interface BackgroundTranslucentListener {
    val translucent: MutableLiveData<Boolean>

    fun setTranslucentBackground()
}

/**
 * Interface to implement if the element adapter have to show the type icons of the elements
 */
interface TypedElementsListener {
    val typedElements: MutableLiveData<Boolean>

    fun setTypedElements()
}

/**
 * Interface to implement what element is active
 */
interface ActiveFlagListener<T> {
    val activeItems: MutableLiveData<List<T>>

    fun updateActiveItem(items: List<T>)
}

/**
 * Interface to implements the communication between the fragment and the adapter when only need to
 * communicate the selected items
 */
interface ListenerOfSelectMode<T> {
    val selectedItems: ArrayList<T>
    val selectChanged: MutableLiveData<Boolean>

    fun changeSelected(items: ArrayList<T>)
}

/**
 * Necessary to use the recyclerViewListener
 * The class that want to communicate to parent need to implement a listener class that implement
 * this interface
 */
interface ScrollListener {
    fun hideButton(scrolledDown: Boolean)
}

/**
 * Util class for communicate from fragment page listener to parent fragment events
 */
class PageListener(
    private val startNavigate: (id: Long) -> Unit,
    private val showSnackBarMessage: (message: String, function: (() -> Unit)?) -> Unit,
    private val hideFabButton: (hide: Boolean) -> Unit
) : ScrollListener {
    fun navigate(id: Long) = startNavigate(id)
    fun showMessage(message: String, function: (() -> Unit)? = null) =
        showSnackBarMessage(message, function)
    override fun hideButton(scrolledDown: Boolean) = hideFabButton(scrolledDown)
}

/**
 * Util class for communicate a click or a long click from a viewHolder to the fragment
 */
class HolderListener<T>(
    private val clickListener: (id: Long) -> Unit,
    private val onLongClickListener: (item: T) -> Unit
) {
    fun onClick(id: Long) = clickListener(id)
    fun onLongClick(item: T) = onLongClickListener(item)
}

data class Colour(val color: Int, val description: String)

data class ImageType(val icon: Drawable?, val description: String)