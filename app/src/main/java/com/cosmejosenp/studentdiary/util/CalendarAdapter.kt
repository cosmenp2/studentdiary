/*
 * MIT License
 *
 * Copyright (c) 2022 Cosme José Nieto Pérez
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.cosmejosenp.studentdiary.util

import android.content.res.Resources
import android.os.Build
import com.cosmejosenp.studentdiary.R
import java.util.*

/**
 * Adapt the calendar with the spinners items.
 *
 * @property calendar value.
 * @property resources for get the string representation.
 * @property representation for put a personalized text in the spinner.
 */
data class CalendarAdapter(
    var calendar: Calendar,
    val resources: Resources,
    var representation: String? = null
) {
    @Suppress("DEPRECATION")
    override fun toString(): String =
        representation
            ?: resources.getString(
                R.string.dateOf,
                calendar.getDisplayName(
                    Calendar.DAY_OF_WEEK,
                    Calendar.SHORT,
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
                        resources.configuration.locales[0]
                    else
                        resources.configuration.locale
                ),
                calendar.get(Calendar.DAY_OF_MONTH),
                calendar.getDisplayName(
                    Calendar.MONTH,
                    Calendar.SHORT,
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
                        resources.configuration.locales[0]
                    else
                        resources.configuration.locale
                )
            )

}