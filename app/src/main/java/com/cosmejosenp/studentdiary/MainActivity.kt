/*
 * MIT License
 *
 * Copyright (c) 2022 Cosme José Nieto Pérez
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.cosmejosenp.studentdiary

import android.content.Context
import android.content.SharedPreferences
import android.os.Build
import android.os.Bundle
import android.view.Menu
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.view.GravityCompat
import androidx.databinding.DataBindingUtil
import androidx.drawerlayout.widget.DrawerLayout
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.NavDestination
import androidx.navigation.NavOptions
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.NavigationUI
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupWithNavController
import com.cosmejosenp.studentdiary.data.db.EduAppDatabase
import com.cosmejosenp.studentdiary.data.entity.SchoolYearWithSubjects
import com.cosmejosenp.studentdiary.databinding.ActivityMainBinding
import com.cosmejosenp.studentdiary.ui.DetailSubjectFragmentArgs
import com.cosmejosenp.studentdiary.util.GRID_LAYOUT_ELEMENTS
import com.cosmejosenp.studentdiary.util.LINEAR_LAYOUT_ELEMENTS
import com.cosmejosenp.studentdiary.viewmodels.MainActivityViewModel
import com.cosmejosenp.studentdiary.viewmodels.factories.MainActivityViewModelFactory
import com.google.android.material.navigation.NavigationView

class MainActivity : AppCompatActivity() {
    val SP_ELEMENT_LAYOUT = "elementLayout"
    private lateinit var drawerLayout: DrawerLayout
    private lateinit var _sharedPreferences: SharedPreferences
    val sharedPreferences: SharedPreferences
        get() = _sharedPreferences
    private lateinit var editor: SharedPreferences.Editor
    lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var viewModel: MainActivityViewModel
    private lateinit var navController: NavController
    private lateinit var navView: NavigationView
    private var _hideActionMode = MutableLiveData<Boolean>()
    val hideActionMode: LiveData<Boolean>
        get() = _hideActionMode
    private var _notifyLayoutElementsChange = MutableLiveData(false)
    val notifyLayoutElementsChange: LiveData<Boolean>
        get() = _notifyLayoutElementsChange

    /**
     * Perform initialization of main activity and prepare the general behaviour of the app.
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding =
            DataBindingUtil.setContentView<ActivityMainBinding>(this, R.layout.activity_main)
        drawerLayout = binding.drawerLayout
        navView = binding.navView
        val navHostFragment = supportFragmentManager.findFragmentById(R.id.myNavHostFragment) as NavHostFragment
        navController = navHostFragment.navController
        appBarConfiguration = AppBarConfiguration(navController.graph, drawerLayout)
        _sharedPreferences = getPreferences(Context.MODE_PRIVATE)
        val dataSource = EduAppDatabase.getInstance(application)
        val viewModelFactory = MainActivityViewModelFactory(
            dataSource.schoolYearDao,
            application
        )
        viewModel =
            ViewModelProvider(this, viewModelFactory).get(MainActivityViewModel::class.java)

        _hideActionMode.value = false
        binding.lifecycleOwner = this
        binding.navView.setupWithNavController(navController)
        addObservers()
        addListeners()
    }

    /**
     * Add the necessary observers to update the view with the data and to perform events.
     *
     */
    private fun addObservers() {
        viewModel.schoolYearAndSubjects.observe(this, Observer {
            if (it != null) {
                createMenu(it)
            }
        })

        viewModel.navigateToDetailSubject.observe(this, Observer {
            if (it != null && it[0] != 0L && it[1] != 0L) {
                val args = DetailSubjectFragmentArgs(it[0], it[1]).toBundle()
                val navOptions = NavOptions.Builder()
                navOptions.setPopUpTo(R.id.mainFragment, false)
                navController.navigate(R.id.detailSubjectFragment, args, navOptions.build())
                viewModel.doneNavigateToDetailSubject()
            }
        })
    }

    /**
     * Load dynamically the drawer menu depending of the [schoolYearsWithSubjects].
     *
     * @param schoolYearsWithSubjects
     */
    private fun createMenu(schoolYearsWithSubjects: List<SchoolYearWithSubjects>) {
        navView.menu.clear()
        navView.inflateMenu(R.menu.nav_view_menu)
        val menu = navView.menu
        for (schoolYearWithSubjects in schoolYearsWithSubjects) {
            val subMenu = menu.addSubMenu(
                R.id.quickAccessGroup,
                Menu.NONE,
                Menu.NONE,
                schoolYearWithSubjects.schoolYear.name
            )
            for (subject in schoolYearWithSubjects.subjects) {
                val item = subMenu.add(
                    R.id.quickAccessGroup,
                    subject.id.toInt(),
                    Menu.NONE,
                    subject.name
                )
                item.setIcon(R.drawable.ic_book_black_24dp)
                item.isCheckable = true
            }
        }
        if (viewModel.firstCheck) {
            navView.setCheckedItem(R.id.mainFragment)
            viewModel.doneFirstCheck()
        }
    }

    /**
     * Add listeners to perform actions when the user interact with the screen.
     *
     */
    private fun addListeners() {
        drawerLayout.addDrawerListener(object : DrawerLayout.DrawerListener {
            override fun onDrawerStateChanged(newState: Int) {}

            override fun onDrawerSlide(drawerView: View, slideOffset: Float) {
                _hideActionMode.value = true
            }

            override fun onDrawerClosed(drawerView: View) {
                _hideActionMode.value = false
            }

            override fun onDrawerOpened(drawerView: View) {}
        })

        navController.addOnDestinationChangedListener { _: NavController, nd: NavDestination, bundle: Bundle? ->
            if (nd.id == R.id.addSubjectFragment || nd.id == R.id.addEvaluationFragment
                || nd.id == R.id.copyFromFragment || nd.id == R.id.copyToFragment
            ) {
                drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED)
            } else {
                drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED)
            }
            if (nd.id == R.id.detailSubjectFragment) {
                if (bundle?.getString("codSubject") != null && bundle.getBoolean("activeSchoolYear")) {
                    navView.setCheckedItem(bundle.getLong("subject").toInt())
                    viewModel.setLastMenuItem(bundle.getLong("subject"))
                }
            } else if (!listOf(
                    R.id.listSchoolYearFragment, R.id.addSchoolYearFragment,
                    R.id.mainFragment, R.id.todoElementFragment, R.id.pendingGradesElementFragment
                ).contains(nd.id)
            ) {
                navView.setCheckedItem(R.id.menuNone)
                viewModel.invalidateLastMenuItem()

            } else
                viewModel.invalidateLastMenuItem()
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                this.window.statusBarColor =
                    ContextCompat.getColor(this, R.color.primaryColor)
            }
        }

        navView.setNavigationItemSelectedListener {
            val idItem = it.itemId.toLong()
            if (it.groupId == R.id.quickAccessGroup) {
                if (idItem != 0L) {
                    viewModel.startNavigateToDetailSubject(idItem)
                    drawerLayout.closeDrawer(GravityCompat.START)
                }
            } else {
                NavigationUI.onNavDestinationSelected(it, navController)
                drawerLayout.closeDrawer(GravityCompat.START)
            }
            true
        }
    }

    /**
     * Notify to all fragments finish the action mode.
     *
     */
    fun requestHideActionMode() {
        _hideActionMode.value = true
        _hideActionMode.value = false
    }

    /**
     * Notify to all fragments change the element list layout.
     *
     */
    fun changeElementsLayout() {
        editor = _sharedPreferences.edit()
        if (_sharedPreferences.getInt(
                SP_ELEMENT_LAYOUT,
                LINEAR_LAYOUT_ELEMENTS
            ) == LINEAR_LAYOUT_ELEMENTS
        )
            editor.putInt(SP_ELEMENT_LAYOUT, GRID_LAYOUT_ELEMENTS)
        else
            editor.putInt(SP_ELEMENT_LAYOUT, LINEAR_LAYOUT_ELEMENTS)
        editor.apply()
        _notifyLayoutElementsChange.value = true
    }

    /**
     * Received the notification that the change element layout have been performed.
     *
     */
    fun receivedNotifyLayoutElementChange() {
        _notifyLayoutElementsChange.value = false
    }

    /**
     * Configure the navController to handle the back navigation
     *
     * @return true if Up navigation completed successfully and this Activity was finished,
     *         false otherwise.
     */
    override fun onSupportNavigateUp(): Boolean {
        val navController = this.findNavController(R.id.myNavHostFragment)
        return navController.navigateUp(appBarConfiguration)
                || super.onSupportNavigateUp()
    }
}
