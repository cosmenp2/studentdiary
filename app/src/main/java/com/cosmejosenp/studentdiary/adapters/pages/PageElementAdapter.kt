/*
 * MIT License
 *
 * Copyright (c) 2022 Cosme José Nieto Pérez
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.cosmejosenp.studentdiary.adapters.pages

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.cosmejosenp.studentdiary.ui.pages.PageElementFragment
import com.cosmejosenp.studentdiary.util.ListElementsTypes
import com.cosmejosenp.studentdiary.util.PageElementConstants
import com.cosmejosenp.studentdiary.util.PageListener

/**
 * A viewpager adapter for [com.cosmejosenp.studentdiary.ui.MainFragment],[com.cosmejosenp.studentdiary.ui.PendingElementFragment],
 * [com.cosmejosenp.studentdiary.ui.PendingGradesElementFragment].
 *
 * @property elementListener listener for the [PageElementFragment].
 * @property typeList define what elements will be loaded, one of the [ListElementsTypes] class.
 * @constructor
 *
 * @param fragment which have the ViewPager element.
 */
class PageElementAdapter(
    fragment: Fragment,
    private val elementListener: PageListener,
    private val typeList: ListElementsTypes

) : FragmentStateAdapter(fragment), PageElementConstants {

    /**
     * @return the total fragments that will be loaded.
     */
    override fun getItemCount(): Int = 1

    /**
     * Create the fragments depending of the number of page.
     *
     * @param position of the page.
     * @return a new fragment.
     */
    override fun createFragment(position: Int): Fragment {
        val fragment: Fragment = when (position) {
            0 -> PageElementFragment(
                elementListener
            )
            else -> Fragment()
        }
        fragment.arguments = Bundle().apply {
            putSerializable(ARG_TYPE_LIST, typeList)
        }
        return fragment
    }
}
