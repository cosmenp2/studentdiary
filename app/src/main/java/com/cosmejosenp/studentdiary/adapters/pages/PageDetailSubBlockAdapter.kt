/*
 * MIT License
 *
 * Copyright (c) 2022 Cosme José Nieto Pérez
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.cosmejosenp.studentdiary.adapters.pages

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.cosmejosenp.studentdiary.ui.pages.PageElementFragment
import com.cosmejosenp.studentdiary.util.ListElementsTypes
import com.cosmejosenp.studentdiary.util.PageElementConstants
import com.cosmejosenp.studentdiary.util.PageListener

/**
 * A viewpager adapter for [com.cosmejosenp.studentdiary.ui.DetailSubBlockFragment].
 *
 * @property subBlock from which its details are loaded.
 * @property homeworkType if the type block is [com.cosmejosenp.studentdiary.util.TYPE_HOMEWORK].
 * @property elementListener listener for the [PageElementFragment].
 * @constructor
 *
 * @param fragment which have the ViewPager element.
 */
class PageDetailSubBlockAdapter(
    fragment: Fragment,
    private val subBlock: Long,
    private val homeworkType: Boolean,
    private val elementListener: PageListener

) : FragmentStateAdapter(fragment), PageElementConstants {

    /**
     * @return the total fragments that will be loaded.
     */
    override fun getItemCount(): Int = if (homeworkType) 3 else 4

    /**
     * Create the fragments depending of the number of page.
     *
     * @param position of the page.
     * @return a new fragment.
     */
    override fun createFragment(position: Int): Fragment {
        val fragment: Fragment = when (position) {
            0, 1, 2, 3 -> PageElementFragment(
                elementListener
            )
            else -> Fragment()
        }
        when (position) {
            0 -> fragment.arguments = Bundle().apply {
                putLong(ARG_SUB_BLOCK, subBlock)
                putBoolean(ARG_TYPED_BLOCK, true)
                putSerializable(ARG_TYPE_LIST, ListElementsTypes.ACTIVE)
            }
            1 -> fragment.arguments = Bundle().apply {
                putLong(ARG_SUB_BLOCK, subBlock)
                putBoolean(ARG_TYPED_BLOCK, true)
                putSerializable(ARG_TYPE_LIST, ListElementsTypes.PENDING)
            }
            2 -> if (homeworkType) loadCompletedList(fragment)
            else fragment.arguments = Bundle().apply {
                putLong(ARG_SUB_BLOCK, subBlock)
                putBoolean(ARG_TYPED_BLOCK, true)
                putSerializable(ARG_TYPE_LIST, ListElementsTypes.PENDING_GRADES)
            }
            3 -> loadCompletedList(fragment)

        }
        return fragment
    }

    /**
     * Load the arguments for the completed list type of the fragment [PageElementFragment].
     * @param fragment: the fragment to which the arguments will be added.
     */
    private fun loadCompletedList(fragment: Fragment) {
        fragment.arguments = Bundle().apply {
            putLong(ARG_SUBJECT, subBlock)
            putBoolean(ARG_TYPED_BLOCK, true)
            putSerializable(ARG_TYPE_LIST, ListElementsTypes.COMPLETED)
        }
    }
}
