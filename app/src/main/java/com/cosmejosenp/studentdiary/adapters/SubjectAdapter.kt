/*
 * MIT License
 *
 * Copyright (c) 2022 Cosme José Nieto Pérez
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */



package com.cosmejosenp.studentdiary.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.cosmejosenp.studentdiary.data.entity.Subject
import com.cosmejosenp.studentdiary.databinding.ListItemSubjectBinding
import com.cosmejosenp.studentdiary.util.ActiveFlagListener
import com.cosmejosenp.studentdiary.util.HolderListener
import com.cosmejosenp.studentdiary.util.ListenersOfActionMode
import java.util.*

/**
 * Adapter for subject list items.
 *
 * @property clickListener for handle the click actions of items.
 * @property viewLifecycleOwner for observe values.
 */
class SubjectAdapter(
    private val clickListener: HolderListener<Subject>,
    private val viewLifecycleOwner: LifecycleOwner
) : ListAdapter<Subject, SubjectAdapter.ViewHolder>(SubjectDiffCallback()),
    ListenersOfActionMode<Subject>, ActiveFlagListener<Subject> {
    override val selectAll = MutableLiveData<Boolean>()
    override val actionModeEnabled = MutableLiveData<Boolean>()
    override var selectedItems = ArrayList<Subject>()
    override val activeItems = MutableLiveData<List<Subject>>()

    /**
     * Called by RecyclerView to display the data at the specified position.
     *
     * @param holder The ViewHolder which should be updated to represent the contents of the
     *        item at the given position in the data set.
     * @param position The position of the item within the adapter's data set.
     */
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = getItem(position)
        holder.bind(item, clickListener, viewLifecycleOwner)
    }

    /**
     * Called when RecyclerView needs a new [ViewHolder] of the given type to represent
     * an item.
     *
     * @param parent The ViewGroup into which the new View will be added after it is bound to
     *               an adapter position.
     * @param viewType The view type of the new View.
     * @return A new ViewHolder that holds a View of the given view type.
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent, this, this)
    }

    /**
     * Change the selected state for all elements of the list.
     *
     * @param value if all elements should be selected.
     */
    override fun changeSelectedAllTo(value: Boolean) {
        if (value) {
            selectedItems.clear()
            selectedItems.addAll(currentList)
        }
        selectAll.value = value
    }

    /**
     * Activate other behaviours that should be doing in this mode
     * and clear the [selectedItems] list for prevent older item selections
     * will be selected again.
     *
     */
    override fun activateActionMode() {
        selectedItems.clear()
        actionModeEnabled.value = true
    }

    /**
     * Activate other behaviours that should be doing in this mode,
     * clear the [selectedItems] list and restore the list.
     *
     * This method is used for restoring states when a configuration device change.
     *
     * @param items selected.
     */
    override fun activateActionMode(items: ArrayList<Subject>) {
        selectedItems.clear()
        selectedItems.addAll(items)
        actionModeEnabled.value = true
    }

    /**
     * Called for restore default behaviours when this mode end.
     *
     */
    override fun actionModeDone() {
        actionModeEnabled.value = false
        selectAll.value = false
    }

    /**
     * Update which are the active subjects.
     *
     * @param items the active subjects.
     */
    override fun updateActiveItem(items: List<Subject>) {
        activeItems.value = items
    }

    /**
     * Describe an item view, metadata and behaviour about its place within the RecyclerView.
     *
     * @property binding the view which the item will be loaded.
     * @property listenersOfActionMode for changes modes and selected items state.
     * @property activeFlagListener for make active label visible.
     */
    class ViewHolder private constructor(
        private val binding: ListItemSubjectBinding,
        private val listenersOfActionMode: ListenersOfActionMode<Subject>,
        private val activeFlagListener: ActiveFlagListener<Subject>
    ) : RecyclerView.ViewHolder(binding.root) {
        private var actionModeEnabled = false

        /**
         * Attach data to the view, listen for events and define their behaviour.
         *
         * @param item the item will be rendered.
         * @param clickListener for single click and long click actions.
         * @param lifecycleOwner for observe events.
         */
        fun bind(
            item: Subject,
            clickListener: HolderListener<Subject>,
            lifecycleOwner: LifecycleOwner
        ) {
            binding.subject = item
            if (activeFlagListener.activeItems.value != null && activeFlagListener.activeItems.value!!.contains(
                    item
                )
            )
                binding.listItemSubjectActiveText.visibility = View.VISIBLE
            else
                binding.listItemSubjectActiveText.visibility = View.GONE

            binding.listItemSubjectItemContainer.setOnClickListener {
                if (actionModeEnabled) {
                    longClick(clickListener, item)
                } else {
                    clickListener.onClick(item.id)
                }
            }
            binding.listItemSubjectItemContainer.setOnLongClickListener {
                longClick(clickListener, item)
                true
            }
            binding.executePendingBindings()
            listenersOfActionMode.actionModeEnabled.observe(lifecycleOwner, Observer {
                actionModeEnabled = if (it) {
                    if (listenersOfActionMode.selectedItems.contains(item)) {
                        select()
                    }
                    true
                } else {
                    deselect()
                    false
                }
            })
            listenersOfActionMode.selectAll.observe(lifecycleOwner, Observer {
                if (it) {
                    select()
                }
            })
            activeFlagListener.activeItems.observe(lifecycleOwner, Observer {
                if (it != null && it.contains(item))
                    binding.listItemSubjectActiveText.visibility = View.VISIBLE
                else
                    binding.listItemSubjectActiveText.visibility = View.GONE
            })
            if (listenersOfActionMode.actionModeEnabled.value == true && listenersOfActionMode.selectedItems.contains(
                    item
                )
            ) {
                select()
            } else {
                deselect()
            }
        }

        /**
         * The action when the user perform a long click to the item.
         *
         * @param clickListener for notify to parent to handle the click action.
         * @param item of the view clicked.
         */
        private fun longClick(
            clickListener: HolderListener<Subject>,
            item: Subject
        ) {
            clickListener.onLongClick(item)
            if (binding.listItemSubjectItemContainer.isSelected) {
                deselect()
                listenersOfActionMode.selectedItems.remove(item)
            } else if (listenersOfActionMode.actionModeEnabled.value == true) {
                select()
                listenersOfActionMode.selectedItems.add(item)
            }
        }

        /**
         * Change state of the elements and its view into a selected state.
         *
         */
        private fun select() {
            binding.listItemSubjectItemContainer.isSelected = true
            binding.listItemSubjectTitle.isSelected = true
            binding.listItemSubjectImageSelected.visibility = View.VISIBLE
        }

        /**
         * Change state of the elements and its view into a unselected state.
         *
         */
        private fun deselect() {
            binding.listItemSubjectItemContainer.isSelected = false
            binding.listItemSubjectTitle.isSelected = false
            binding.listItemSubjectImageSelected.visibility = View.GONE
        }

        companion object {
            /**
             * Static function for load and create appropriately the [ViewHolder].
             *
             * @param parent The ViewGroup into which the new View will be added after it is bound to
             *               an adapter position.
             * @param listenersOfActionMode for observe events of the adapter.
             * @param activeFlagListener for make active label visible.
             * @return a new [ViewHolder].
             */
            fun from(
                parent: ViewGroup,
                listenersOfActionMode: ListenersOfActionMode<Subject>,
                activeFlagListener: ActiveFlagListener<Subject>
            ): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = ListItemSubjectBinding.inflate(layoutInflater, parent, false)
                return ViewHolder(binding, listenersOfActionMode, activeFlagListener)
            }
        }
    }
}

/**
 * Diff class to compare the difference between two items
 *
 */
class SubjectDiffCallback : DiffUtil.ItemCallback<Subject>() {
    /**
     * @param oldItem
     * @param newItem
     * @return if the items are the same
     */
    override fun areItemsTheSame(oldItem: Subject, newItem: Subject): Boolean =
        oldItem.id == newItem.id

    /**
     * @param oldItem
     * @param newItem
     * @return if the content of the items are the same
     */
    override fun areContentsTheSame(oldItem: Subject, newItem: Subject): Boolean =
        oldItem == newItem

}