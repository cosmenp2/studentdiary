/*
 * MIT License
 *
 * Copyright (c) 2022 Cosme José Nieto Pérez
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.cosmejosenp.studentdiary.ui

import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.activity.addCallback
import androidx.appcompat.widget.Toolbar
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import androidx.recyclerview.widget.RecyclerView
import com.cosmejosenp.studentdiary.MainActivity
import com.cosmejosenp.studentdiary.R
import com.cosmejosenp.studentdiary.adapters.BlockWithSubBlockAdapter
import com.cosmejosenp.studentdiary.data.db.EduAppDatabase
import com.cosmejosenp.studentdiary.data.entity.Evaluation
import com.cosmejosenp.studentdiary.databinding.FragmentCopyFromBinding
import com.cosmejosenp.studentdiary.util.Colour
import com.cosmejosenp.studentdiary.util.obtainColour
import com.cosmejosenp.studentdiary.viewmodels.CopyFromViewModel
import com.cosmejosenp.studentdiary.viewmodels.factories.CopyFromViewModelFactory
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.snackbar.Snackbar


class CopyFromFragment : Fragment() {
    private val BUNDLE_CLONE_MODE = "clone_mode"
    private lateinit var mainActivity: MainActivity
    private lateinit var viewModel: CopyFromViewModel
    private lateinit var toolbar: Toolbar
    private lateinit var structureAdapter: BlockWithSubBlockAdapter
    private var cloneMode: CheckBox? = null
    private lateinit var evaluationSpinner: Spinner
    private lateinit var structureRecyclerView: RecyclerView
    private lateinit var descriptionTextView: TextView
    private lateinit var navController: NavController
    private lateinit var args: CopyFromFragmentArgs
    private lateinit var coordinatorLayout: CoordinatorLayout
    private var arrayAdapter: ArrayAdapter<Evaluation>? = null

    /**
     * Prepare the view and make the necessary changes to attach correctly
     * the fragment with the viewModel.
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return root view.
     */
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding: FragmentCopyFromBinding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_copy_from, container, false
        )
        val application = requireNotNull(this.activity).application
        mainActivity = requireActivity() as MainActivity
        toolbar = binding.copyFromToolbar
        cloneMode = binding.copyFromCloneModeCheckBox
        evaluationSpinner = binding.copyFromSpinner
        structureRecyclerView = binding.copyFromStructureRecyclerView
        coordinatorLayout = binding.copyFromCoordinatorLayout
        descriptionTextView = binding.copyFromDescriptionCloneMode
        binding.lifecycleOwner = viewLifecycleOwner
        navController = mainActivity.findNavController(R.id.myNavHostFragment)
        args = CopyFromFragmentArgs.fromBundle(requireArguments())
        val dataSource = EduAppDatabase.getInstance(application)
        val viewModelFactory = CopyFromViewModelFactory(
            dataSource,
            args.subject,
            args.subSubject,
            application
        )
        viewModel =
            ViewModelProvider(this, viewModelFactory).get(CopyFromViewModel::class.java)
        structureAdapter = BlockWithSubBlockAdapter()

        toolbar.setupWithNavController(navController, mainActivity.appBarConfiguration)
        toolbar.navigationIcon =
            ContextCompat.getDrawable(requireContext(), R.drawable.ic_close_black_24dp)
        binding.copyFromStructureRecyclerView.adapter = structureAdapter
        binding.viewModel = viewModel
        binding.lifecycleOwner = this
        addObservers()
        restoreContent(savedInstanceState)
        addListeners()

        return binding.root
    }

    /**
     * Add the necessary observers to update the view with the data and to perform events.
     *
     */
    private fun addObservers() {
        viewModel.subject.observe(viewLifecycleOwner, Observer {
            it?.let {
                changeAppearanceColor(obtainColour(it.colour, resources, requireContext()))
            }
        })

        viewModel.structure.observe(viewLifecycleOwner, Observer {
            it?.let {
                structureAdapter.submitList(it)
            }
        })

        viewModel.navigateToDetailSubSubject.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                navController.navigate(
                    CopyFromFragmentDirections.actionCopyFromFragmentToDetailSubSubjectFragment(
                        subSubject = args.subSubject,
                        subject = args.subject
                    )
                )
                viewModel.doneNavigatingToDetailSubject()
            }
        })

        viewModel.navigateToBackStack.observe(viewLifecycleOwner, Observer {
            if (it) {
                navController.popBackStack()
                viewModel.doneNavigatingToBackStack()
            }
        })

        viewModel.evaluations.observe(viewLifecycleOwner, Observer {
            it?.let {
                val swap = ArrayList<Evaluation>()
                swap.add(Evaluation(0L, "", 0))
                swap.addAll(it)
                arrayAdapter = ArrayAdapter(
                    this@CopyFromFragment.requireContext(),
                    android.R.layout.simple_spinner_item,
                    swap
                ).also { adapter ->
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                    evaluationSpinner.adapter = adapter
                }
            }
        })

        viewModel.showDialog.observe(viewLifecycleOwner, Observer {
            if (it) {
                MaterialAlertDialogBuilder(requireContext())
                    .setTitle(getString(R.string.enableCloneModeQuestion))
                    .setMessage(getString(R.string.enableCloneModeExplanation))
                    .setPositiveButton(getString(R.string.yes)) { _, _ ->
                        viewModel.updateSelectedItem(true)
                    }
                    .setNegativeButton(getString(R.string.no)) { _, _ ->
                        viewModel.startDisableCloneMode()
                    }
                    .show()
                viewModel.showDialogDone()
            }
        })

        viewModel.disableCloneMode.observe(viewLifecycleOwner, Observer {
            if (it) {
                cloneMode?.isChecked = false
                viewModel.doneDisableCloneMode()
            }
        })

        viewModel.showSnackBarMessage.observe(viewLifecycleOwner, Observer {
            it?.let {
                Snackbar.make(
                    mainActivity.findViewById(android.R.id.content),
                    it,
                    Snackbar.LENGTH_SHORT
                ).show()
                viewModel.showSnackBarMessageDone()
            }
        })

        viewModel.showToastMessage.observe(viewLifecycleOwner, Observer {
            it?.let {
                Toast.makeText(requireContext(), it, Toast.LENGTH_LONG).show()
                viewModel.showToastMessageDone()
            }
        })

        viewModel.restoreSelectedItem.observe(viewLifecycleOwner, Observer {
            if (it) {
                if (arrayAdapter != null) {
                    evaluationSpinner.setSelection(arrayAdapter!!.getPosition(viewModel.selectedItem.value))
                    viewModel.restoreContentDone()
                }
            }
        })

        viewModel.validData.observe(viewLifecycleOwner, Observer {
            toolbar.menu.findItem(R.id.copyMenuCopy).isEnabled = it
        })
    }

    /**
     * Restore information on configuration changes.
     *
     * @param savedInstanceState
     */
    private fun restoreContent(savedInstanceState: Bundle?) {
        savedInstanceState?.let {
            cloneMode?.isChecked = it.getBoolean(BUNDLE_CLONE_MODE)
            viewModel.startRestoreContent()
        }
        if (cloneMode!!.isChecked) {
            descriptionTextView.text = getString(R.string.enabledCloneModeExplanation)
        } else {
            descriptionTextView.text = getString(R.string.disabledCloneModeExplanation)
        }
    }

    /**
     * Add listeners to perform actions when the user interact with the screen.
     *
     */
    private fun addListeners() {

        toolbar.setOnMenuItemClickListener {
            viewModel.startCopy(cloneMode!!.isChecked)
            true
        }

        toolbar.setNavigationOnClickListener {
            handleBackButton()
        }

        evaluationSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                viewModel.updateSelectedItem(Evaluation(0L, "", 0), cloneMode!!.isChecked)
            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                viewModel.updateSelectedItem(
                    parent?.getItemAtPosition(position) as Evaluation,
                    cloneMode!!.isChecked
                )
            }
        }

        cloneMode?.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                viewModel.startShowDialog()
                descriptionTextView.text = getString(R.string.enabledCloneModeExplanation)
            } else {
                viewModel.updateSelectedItem(false)
                descriptionTextView.text = getString(R.string.disabledCloneModeExplanation)
            }
        }

        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner) {
            handleBackButton()
        }
    }

    /**
     * Change the color appearance according to the color selected.
     *
     * @param colour
     */
    private fun changeAppearanceColor(colour: Colour) {
        toolbar.setBackgroundColor(colour.color)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            requireActivity().window.statusBarColor = colour.color
        }
        coordinatorLayout.setBackgroundColor(colour.color)
    }

    /**
     * Handle the action when back button is pressed.
     *
     */
    private fun handleBackButton() {
        viewModel.startNavigateToBackStack()
    }

    /**
     * Save the volatile data when configuration change.
     *
     * @param outState
     */
    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        cloneMode?.isChecked?.let { outState.putBoolean(BUNDLE_CLONE_MODE, it) }
    }
}
