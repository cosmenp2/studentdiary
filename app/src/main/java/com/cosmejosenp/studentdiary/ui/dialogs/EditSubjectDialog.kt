/*
 * MIT License
 *
 * Copyright (c) 2022 Cosme José Nieto Pérez
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.cosmejosenp.studentdiary.ui.dialogs

import android.app.Dialog
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.view.inputmethod.InputMethodManager
import android.widget.CheckBox
import android.widget.TextView
import androidx.activity.addCallback
import androidx.appcompat.widget.Toolbar
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.content.ContextCompat.getSystemService
import androidx.core.widget.addTextChangedListener
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import com.cosmejosenp.studentdiary.MainActivity
import com.cosmejosenp.studentdiary.R
import com.cosmejosenp.studentdiary.adapters.ColorAdapter
import com.cosmejosenp.studentdiary.data.db.EduAppDatabase
import com.cosmejosenp.studentdiary.databinding.DialogEditSubjectBinding
import com.cosmejosenp.studentdiary.util.*
import com.cosmejosenp.studentdiary.viewmodels.dialogs.EditSubjectViewModel
import com.cosmejosenp.studentdiary.viewmodels.dialogs.factories.EditSubjectViewModelFactory
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.snackbar.Snackbar


class EditSubjectDialog : DialogFragment(), DetailSubjectFragmentConstants {
    private lateinit var viewModel: EditSubjectViewModel
    private lateinit var toolbar: Toolbar
    private lateinit var coordinatorLayout: CoordinatorLayout
    private var continuousCheckBox: CheckBox? = null
    private lateinit var recyclerView: RecyclerView
    private lateinit var colorAdapter: ColorAdapter
    private var titleTextView: TextView? = null
    private val BUNDLE_TITLE = "title"
    private val BUNDLE_CONTINUOUS_CHECKBOX = "continuous"

    /**
     * Create the dialog and apply no title attribute to windows
     * because we provide a toolbar instead.
     *
     * @param savedInstanceState
     * @return a new Dialog.
     */
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        return dialog
    }

    /**
     * Prepare the view and make the necessary changes to attach correctly
     * the fragment with the viewModel.
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return root view.
     */
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding: DialogEditSubjectBinding = DataBindingUtil.inflate(
            inflater, R.layout.dialog_edit_subject, container, false
        )
        val application = requireNotNull(this.activity).application
        val dataSource = EduAppDatabase.getInstance(application)
        val viewModelFactory =
            EditSubjectViewModelFactory(
                dataSource.subjectDao,
                requireArguments().getLong(ARG_OBJECT),
                application
            )
        val colours = obtainListOfColours(resources, requireContext())
        toolbar = binding.editSubjectToolbar
        coordinatorLayout = binding.editSubjectCoordinatorLayout
        toolbar.title = getString(R.string.edit)
        titleTextView = binding.editSubjectName
        continuousCheckBox = binding.editSubjectContinuousAssessment
        recyclerView = binding.editSubjectColorRecyclerView
        colorAdapter = ColorAdapter(HolderListener({
            // For simplicity this function is not implemented, instead we use the long click for a single click
        }, {
            viewModel.selectColour(it)
        }), viewLifecycleOwner)
        binding.lifecycleOwner = viewLifecycleOwner
        colorAdapter.submitList(colours)
        recyclerView.adapter = colorAdapter
        viewModel =
            ViewModelProvider(this, viewModelFactory).get(EditSubjectViewModel::class.java)
        binding.lifecycleOwner = this

        addObservers()
        restoreContent(savedInstanceState)
        addListeners()

        return binding.root
    }

    /**
     * Add the necessary observers to update the view with the data and to perform events.
     *
     */
    private fun addObservers() {
        viewModel.subject.observe(viewLifecycleOwner, Observer {
            if (viewModel.firstConfiguration && context != null) {
                val colour = obtainColour(it.colour, resources, requireContext())
                val listColour = ArrayList<Colour>()
                listColour.add(colour)
                titleTextView?.text = it.name
                continuousCheckBox?.isChecked = it.continuousAssessment
                colorAdapter.changeSelected(listColour)
                viewModel.selectColour(colour)
                viewModel.doneFirstConfiguration()
            }
        })

        viewModel.selectedColour.observe(viewLifecycleOwner, Observer {
            it?.let {
                changeAppearanceColor(it)
                viewModel.evaluateData(
                    titleTextView!!.text.toString(),
                    continuousCheckBox!!.isChecked
                )
                val colorList = ArrayList<Colour>()
                colorList.add(it)
                colorAdapter.changeSelected(colorList)
            }
        })

        viewModel.navigateToBackStack.observe(viewLifecycleOwner, Observer {
            if (it) {
                hideKeyboard()
                viewModel.doneNavigatingToBackStack()
                changeAppearanceColor(
                    obtainColour(
                        viewModel.subject.value!!.colour,
                        resources,
                        requireContext()
                    )
                )
                this@EditSubjectDialog.dismiss()
            }
        })

        viewModel.showDialog.observe(viewLifecycleOwner, Observer {
            if (it) {
                MaterialAlertDialogBuilder(requireContext())
                    .setMessage(getString(R.string.discardQuestion))
                    .setPositiveButton(
                        getString(R.string.continueEditing)
                        , null
                    )
                    .setNegativeButton(getString(R.string.discard)) { _, _ ->
                        viewModel.startNavigateToBackStack()
                    }
                    .show()
                viewModel.showDialogDone()
            }
        })

        viewModel.showSnackBarMessage.observe(viewLifecycleOwner, Observer {
            it?.let {
                Snackbar.make(
                    (activity as MainActivity).findViewById(android.R.id.content),
                    it,
                    Snackbar.LENGTH_SHORT
                ).show()
                viewModel.showSnackBarMessageDone()
            }
        })

        viewModel.validData.observe(viewLifecycleOwner, Observer {
            toolbar.menu.findItem(R.id.saveMenuSave).isEnabled = it
        })

        viewModel.clearData.observe(viewLifecycleOwner, Observer {
            if (it) {
                clear()
                viewModel.clearDataDone()
            }
        })
    }

    /**
     * Hide the keyboard before exit from dialog for prevent a keyboard opened.
     *
     */
    private fun hideKeyboard() {
        val iim: InputMethodManager =
            getSystemService(requireContext(), InputMethodManager::class.java)!!
        iim.hideSoftInputFromWindow(requireView().windowToken, 0)
    }

    /**
     * Change the color appearance according to the color of the subject selected.
     *
     */
    private fun changeAppearanceColor(colour: Colour) {
        toolbar.setBackgroundColor(colour.color)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            requireActivity().window.statusBarColor = colour.color
        }
        coordinatorLayout.setBackgroundColor(colour.color)
        toolbar.setBackgroundColor(colour.color)
    }

    /**
     * Clear the text.
     *
     */
    private fun clear() {
        titleTextView?.text = ""
    }

    /**
     * Restore information on configuration changes.
     *
     * @param savedInstanceState
     */
    private fun restoreContent(savedInstanceState: Bundle?) {
        savedInstanceState?.let {
            titleTextView?.text = it.getString(BUNDLE_TITLE)
            continuousCheckBox?.isChecked = it.getBoolean(BUNDLE_CONTINUOUS_CHECKBOX)
        }
    }

    /**
     * Add listeners to perform actions when the user interact with the screen.
     *
     */
    private fun addListeners() {
        titleTextView?.addTextChangedListener {
            viewModel.evaluateData(it.toString(), continuousCheckBox!!.isChecked)
        }

        continuousCheckBox?.setOnCheckedChangeListener { _, isChecked ->
            viewModel.evaluateData(titleTextView?.text.toString(), isChecked)
        }

        toolbar.setOnMenuItemClickListener {
            viewModel.edit(titleTextView?.text.toString(), continuousCheckBox!!.isChecked)
            true
        }

        toolbar.setNavigationOnClickListener {
            handleBackButton()
        }

        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner) {
            handleBackButton()
        }
    }

    /**
     * Handle the action when back button is pressed.
     *
     */
    private fun handleBackButton() {
        if (viewModel.validData.value!!) {
            viewModel.startShowDialog()
        } else {
            viewModel.startNavigateToBackStack()
        }
    }

    /**
     * Save the volatile data when configuration change.
     *
     * @param outState
     */
    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        if (titleTextView != null)
            outState.putString(BUNDLE_TITLE, titleTextView!!.text.toString())
        if (continuousCheckBox != null)
            outState.putBoolean(BUNDLE_CONTINUOUS_CHECKBOX, continuousCheckBox!!.isChecked)
    }
}
