/*
 * MIT License
 *
 * Copyright (c) 2022 Cosme José Nieto Pérez
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.cosmejosenp.studentdiary.ui.pages

import android.os.Build
import android.os.Bundle
import android.view.*
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.cosmejosenp.studentdiary.MainActivity
import com.cosmejosenp.studentdiary.R
import com.cosmejosenp.studentdiary.adapters.SubjectAdapter
import com.cosmejosenp.studentdiary.data.db.EduAppDatabase
import com.cosmejosenp.studentdiary.databinding.FragmentPageSubjectBinding
import com.cosmejosenp.studentdiary.util.*
import com.cosmejosenp.studentdiary.viewmodels.pages.PageSubjectViewModel
import com.cosmejosenp.studentdiary.viewmodels.pages.factories.PageSubjectViewModelFactory
import com.google.android.material.dialog.MaterialAlertDialogBuilder

class PageSubjectFragment(val listener: PageListener? = null) : Fragment(),
    PageDetailSchoolYearConstants, PageDetailEvaluationConstants {
    override val ARG_OBJECT: String
        get() = super<PageDetailSchoolYearConstants>.ARG_OBJECT

    private val ARG_OBJECT_EV: String
        get() = super<PageDetailEvaluationConstants>.ARG_OBJECT

    private lateinit var viewModel: PageSubjectViewModel
    private lateinit var adapter: SubjectAdapter
    private lateinit var mainActivity: MainActivity
    private lateinit var window: Window
    private var actionMode: ActionMode? = null
    private lateinit var actionModeCallback: ActionMode.Callback

    /**
     * Prepare the view and make the necessary changes to attach correctly
     * the fragment with the viewModel.
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return root view.
     */
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding: FragmentPageSubjectBinding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_page_subject,
            container,
            false
        )
        actionModeCallback = obtainActionModeObject()
        window = requireActivity().window
        mainActivity = requireActivity() as MainActivity
        val application = requireNotNull(this.activity).application
        val dataSource = EduAppDatabase.getInstance(application)
        val viewModelFactory =
            PageSubjectViewModelFactory(
                dataSource.subjectDao,
                dataSource.subSubjectDao,
                arguments?.getLong(ARG_OBJECT)!!,
                arguments?.getLong(ARG_OBJECT_EV)!!,
                application
            )
        viewModel =
            ViewModelProvider(this, viewModelFactory).get(PageSubjectViewModel::class.java)
        if (listener != null) {
            viewModel.setListener(listener)
        }
        adapter = SubjectAdapter(
            HolderListener({ codSubject ->
                viewModel.onSubjectClicked(codSubject)
            }, { subject ->
                viewModel.activateActionMode()
                viewModel.toggleItemSelected(subject)
            }),
            viewLifecycleOwner
        )
        binding.viewModel = viewModel
        binding.listSubjectRecyclerView.adapter = adapter
        if (viewModel.listener != null)
            binding.listSubjectRecyclerView.addOnScrollListener(RecyclerViewListener(viewModel.listener!!))
        binding.lifecycleOwner = this
        addObservers()

        return binding.root
    }

    /**
     * Add the necessary observers to update the view with the data and to perform events.
     *
     */
    private fun addObservers() {
        viewModel.subjects.observe(viewLifecycleOwner, Observer {
            it?.let {
                adapter.submitList(it)
            }
        })

        viewModel.activeItems.observe(viewLifecycleOwner, Observer {
            it?.let {
                adapter.updateActiveItem(it)
            }
        })

        viewModel.navigateToDetailSubject.observe(viewLifecycleOwner, Observer {
            if (it != 0L) {
                viewModel.listener!!.navigate(it)
                viewModel.doneNavigatingToDetailSubject()
            }
        })

        viewModel.showSnackBarMessage.observe(viewLifecycleOwner, Observer {
            it?.let {
                viewModel.listener!!.showMessage(it)
                viewModel.showSnackBarDone()
            }
        })

        viewModel.actionModeActivated.observe(viewLifecycleOwner, Observer {
            if (it) {
                if (actionMode == null) {
                    actionMode = activity?.startActionMode(actionModeCallback)
                    if (viewModel.selectedItems.isEmpty())
                        adapter.activateActionMode()
                    else {
                        adapter.activateActionMode(viewModel.selectedItems)
                        viewModel.notifyChangeTitleActionMode()
                    }
                }
            } else {
                if (actionMode != null) {
                    adapter.actionModeDone()
                    actionMode!!.finish()
                }
            }
        })

        viewModel.selectAllItems.observe(viewLifecycleOwner, Observer {
            adapter.changeSelectedAllTo(it)
        })

        viewModel.showDialogDelete.observe(viewLifecycleOwner, Observer {
            if (it) {
                MaterialAlertDialogBuilder(requireContext())
                    .setTitle(getString(R.string.deleteQuestion))
                    .setMessage(viewModel.messageDelete)
                    .setPositiveButton(
                        getString(R.string.yes)
                    ) { _, _ ->
                        viewModel.delete()
                    }
                    .setNegativeButton(getString(R.string.no), null)
                    .show()
                viewModel.dialogDeleteDone()
            }
        })

        viewModel.changeTitleActionMode.observe(viewLifecycleOwner, Observer {
            if (actionMode != null && it) {
                if (viewModel.titleActionMode > 1)
                    actionMode!!.title =
                        resources.getString(R.string.selectedTitlePlural, viewModel.titleActionMode)
                else
                    actionMode!!.title = resources.getString(R.string.selectedTitle, 1)
                viewModel.changeTitleActionModeDone()
            }
        })

        mainActivity.hideActionMode.observe(viewLifecycleOwner, Observer {
            if (it) {
                viewModel.actionModeDone()
            }
        })
    }

    /**
     * @return an ActionMode.Callback object.
     */
    private fun obtainActionModeObject(): ActionMode.Callback =
        object : ActionMode.Callback {

            /**
             * Called when action mode is first created. The menu supplied will be used to
             * generate action buttons for the action mode and change appearance.
             *
             * @param mode ActionMode being created.
             * @param menu Menu used to populate action buttons.
             * @return true if the action mode should be created, false if entering this
             *              mode should be aborted.
             */
            override fun onCreateActionMode(mode: ActionMode?, menu: Menu?): Boolean {
                val inflater = mode?.menuInflater
                inflater?.inflate(R.menu.contextual_delete_finish, menu)
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    window.statusBarColor =
                        ContextCompat.getColor(context!!, R.color.secondaryLightColor)
                }
                menu?.removeItem(R.id.menuMarkAsComplete)
                return true
            }

            /**
             * Called to refresh an action mode's action menu whenever it is invalidated.
             *
             * @param mode ActionMode being prepared.
             * @param menu Menu used to populate action buttons.
             * @return true if the menu or action mode was updated, false otherwise.
             */
            override fun onPrepareActionMode(mode: ActionMode?, menu: Menu?): Boolean {
                return false
            }

            /**
             * Called to report a user click on an action button.
             *
             * @param mode The current ActionMode.
             * @param item The item that was clicked.
             * @return true if this callback handled the event, false if the standard MenuItem
             *          invocation should continue.
             */
            override fun onActionItemClicked(mode: ActionMode?, item: MenuItem?): Boolean {
                when (item?.itemId) {
                    R.id.menuSelectAll -> {
                        viewModel.selectAll()
                        viewModel.selectAllDone()
                    }
                    R.id.menuDelete -> {
                        viewModel.startDialogDelete()
                    }
                }
                return true
            }

            /**
             * Called when an action mode is about to be exited and destroyed.
             * Appearance color is restored too.
             *
             * @param mode The current ActionMode being destroyed.
             */
            override fun onDestroyActionMode(mode: ActionMode?) {
                actionMode = null
                viewModel.actionModeDone()
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    window.statusBarColor = ContextCompat.getColor(context!!, R.color.primaryColor)
                }
                adapter.actionModeDone()
            }
        }
}