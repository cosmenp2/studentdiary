/*
 * MIT License
 *
 * Copyright (c) 2022 Cosme José Nieto Pérez
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.cosmejosenp.studentdiary.ui.pages

import android.os.Build
import android.os.Bundle
import android.view.*
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.cosmejosenp.studentdiary.MainActivity
import com.cosmejosenp.studentdiary.R
import com.cosmejosenp.studentdiary.adapters.SchoolYearAdapter
import com.cosmejosenp.studentdiary.data.db.EduAppDatabase
import com.cosmejosenp.studentdiary.databinding.FragmentPageSchoolYearBinding
import com.cosmejosenp.studentdiary.util.HolderListener
import com.cosmejosenp.studentdiary.util.PageListSchoolYearConstants
import com.cosmejosenp.studentdiary.util.PageListener
import com.cosmejosenp.studentdiary.util.RecyclerViewListener
import com.cosmejosenp.studentdiary.viewmodels.pages.PageSchoolYearViewModel
import com.cosmejosenp.studentdiary.viewmodels.pages.factories.PageSchoolYearViewModelFactory
import com.google.android.material.dialog.MaterialAlertDialogBuilder

class PageSchoolYearFragment(val listener: PageListener? = null) : Fragment(),
    PageListSchoolYearConstants {

    private lateinit var viewModel: PageSchoolYearViewModel
    private lateinit var adapter: SchoolYearAdapter
    private lateinit var mainActivity: MainActivity
    private lateinit var window: Window
    private var actionMode: ActionMode? = null
    private lateinit var actionModeCallback: ActionMode.Callback

    /**
     * Prepare the view and make the necessary changes to attach correctly
     * the fragment with the viewModel.
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return root view.
     */
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding: FragmentPageSchoolYearBinding =
            DataBindingUtil.inflate(
                inflater,
                R.layout.fragment_page_school_year,
                container,
                false
            )
        actionModeCallback = obtainActionModeObject()
        window = requireActivity().window
        mainActivity = requireActivity() as MainActivity
        val application = requireNotNull(this.activity).application
        val dataSource = EduAppDatabase.getInstance(
            application
        ).schoolYearDao
        val viewModelFactory =
            PageSchoolYearViewModelFactory(
                dataSource,
                arguments?.getBoolean(ARG_OBJECT)!!,
                application
            )
        viewModel =
            ViewModelProvider(this, viewModelFactory)
                .get(PageSchoolYearViewModel::class.java)
        if (listener != null) {
            viewModel.setListener(listener)
        }
        adapter = SchoolYearAdapter(
            HolderListener({ codSchoolYear ->
                viewModel.startNavigateToDetailSchoolYear(codSchoolYear)
            }, { schoolYear ->
                viewModel.activateActionMode()
                viewModel.toggleItemSelected(schoolYear)
            }),
            viewLifecycleOwner
        )
        binding.viewModel = viewModel
        binding.listSchoolYearRecyclerView.adapter = adapter
        if (viewModel.listener != null)
            binding.listSchoolYearRecyclerView.addOnScrollListener(
                RecyclerViewListener(
                    viewModel.listener!!
                )
            )
        binding.lifecycleOwner = this
        addObservers()

        return binding.root
    }

    /**
     * Add the necessary observers to update the view with the data and to perform events.
     *
     */
    private fun addObservers() {
        viewModel.schoolYears.observe(viewLifecycleOwner, Observer {
            it?.let {
                adapter.submitList(it)
            }
        })

        viewModel.navigateToDetailSchoolYear.observe(viewLifecycleOwner, Observer {
            if (it != 0L) {
                viewModel.listener!!.navigate(it)
                viewModel.doneNavigatingToDetailSchoolYear()
            }
        })

        viewModel.showSnackBarMessage.observe(viewLifecycleOwner, Observer {
            it?.let {
                viewModel.listener!!.showMessage(it)
                viewModel.showSnackBarDone()
            }
        })

        viewModel.actionModeActivated.observe(viewLifecycleOwner, Observer {
            if (it) {
                if (actionMode == null) {
                    actionMode = activity?.startActionMode(actionModeCallback)
                    if (viewModel.selectedItems.isEmpty())
                        adapter.activateActionMode()
                    else {
                        adapter.activateActionMode(viewModel.selectedItems)
                        viewModel.notifyChangeTitleActionMode()
                    }
                }
            } else {
                if (actionMode != null) {
                    adapter.actionModeDone()
                    actionMode!!.finish()
                }
            }
        })

        viewModel.selectAllItems.observe(viewLifecycleOwner, Observer {
            adapter.changeSelectedAllTo(it)
        })

        viewModel.showDialogUpdate.observe(viewLifecycleOwner, Observer {
            if (it) {
                val messageResource = if (viewModel.active)
                    getString(R.string.updateToFinishSchoolYearExplanation)
                else
                    getString(R.string.updateToActiveSchoolYearExplanation)
                val titleResource = if (viewModel.active)
                    getString(R.string.finishQuestion)
                else
                    getString(R.string.incompleteQuestion)

                MaterialAlertDialogBuilder(
                    requireContext()
                )
                    .setTitle(titleResource)
                    .setMessage(messageResource)
                    .setPositiveButton(
                        getString(R.string.yes)
                    ) { _, _ ->
                        viewModel.update()
                    }
                    .setNegativeButton(getString(R.string.no), null)
                    .show()
                viewModel.dialogUpdateDone()
            }
        })

        viewModel.showDialogDelete.observe(viewLifecycleOwner, Observer {
            if (it) {
                MaterialAlertDialogBuilder(
                    requireContext()
                )
                    .setTitle(getString(R.string.deleteQuestion))
                    .setMessage(getString(R.string.deleteSchoolYearExplanation))
                    .setPositiveButton(
                        getString(R.string.yes)
                    ) { _, _ ->
                        viewModel.delete()
                    }
                    .setNegativeButton(getString(R.string.no), null)
                    .show()
                viewModel.dialogDeleteDone()
            }
        })

        viewModel.changeTitleActionMode.observe(viewLifecycleOwner, Observer {
            if (actionMode != null && it) {
                if (viewModel.titleActionMode > 1)
                    actionMode!!.title =
                        resources.getString(R.string.selectedTitlePlural, viewModel.titleActionMode)
                else
                    actionMode!!.title = resources.getString(R.string.selectedTitle, 1)
                viewModel.changeTitleActionModeDone()
            }
        })

        mainActivity.hideActionMode.observe(viewLifecycleOwner, Observer {
            if (it) {
                viewModel.actionModeDone()
            }
        })
    }

    /**
     * @return an ActionMode.Callback object.
     */
    private fun obtainActionModeObject(): ActionMode.Callback =
        object : ActionMode.Callback {

            /**
             * Called when action mode is first created. The menu supplied will be used to
             * generate action buttons for the action mode and change appearance.
             *
             * @param mode ActionMode being created.
             * @param menu Menu used to populate action buttons.
             * @return true if the action mode should be created, false if entering this
             *              mode should be aborted.
             */
            override fun onCreateActionMode(mode: ActionMode?, menu: Menu?): Boolean {
                val inflater = mode?.menuInflater
                inflater?.inflate(R.menu.contextual_delete_finish, menu)
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    window.statusBarColor =
                        ContextCompat.getColor(
                            context!!,
                            R.color.secondaryLightColor
                        )
                }
                if (!viewModel.active) {
                    val menuItem = menu?.findItem(R.id.menuMarkAsComplete)
                    menuItem?.icon =
                        ResourcesCompat.getDrawable(
                            resources,
                            R.drawable.ic_assignment_black_24dp,
                            null
                        )
                    menuItem?.setTitle(R.string.markAsIncomplete)
                }
                return true
            }

            /**
             * Called to refresh an action mode's action menu whenever it is invalidated.
             *
             * @param mode ActionMode being prepared.
             * @param menu Menu used to populate action buttons.
             * @return true if the menu or action mode was updated, false otherwise.
             */
            override fun onPrepareActionMode(mode: ActionMode?, menu: Menu?): Boolean {
                return false
            }

            /**
             * Called to report a user click on an action button.
             *
             * @param mode The current ActionMode.
             * @param item The item that was clicked.
             * @return true if this callback handled the event, false if the standard MenuItem
             *          invocation should continue.
             */
            override fun onActionItemClicked(mode: ActionMode?, item: MenuItem?): Boolean {
                when (item?.itemId) {
                    R.id.menuSelectAll -> {
                        viewModel.selectAll()
                        viewModel.selectAllDone()
                    }
                    R.id.menuMarkAsComplete -> {
                        viewModel.startDialogUpdate()
                    }
                    R.id.menuDelete -> {
                        viewModel.startDialogDelete()
                    }
                }
                return true
            }

            /**
             * Called when an action mode is about to be exited and destroyed.
             * Appearance color is restored too.
             *
             * @param mode The current ActionMode being destroyed.
             */
            override fun onDestroyActionMode(mode: ActionMode?) {
                actionMode = null
                viewModel.actionModeDone()
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    window.statusBarColor =
                        ContextCompat.getColor(
                            context!!,
                            R.color.primaryColor
                        )
                }
                adapter.actionModeDone()
            }
        }
}