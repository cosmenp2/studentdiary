/*
 * MIT License
 *
 * Copyright (c) 2022 Cosme José Nieto Pérez
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.cosmejosenp.studentdiary.ui.pages

import android.os.Build
import android.os.Bundle
import android.view.*
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.cosmejosenp.studentdiary.MainActivity
import com.cosmejosenp.studentdiary.R
import com.cosmejosenp.studentdiary.adapters.ElementAdapter
import com.cosmejosenp.studentdiary.data.db.EduAppDatabase
import com.cosmejosenp.studentdiary.data.entity.Element
import com.cosmejosenp.studentdiary.databinding.FragmentPageElementBinding
import com.cosmejosenp.studentdiary.util.*
import com.cosmejosenp.studentdiary.viewmodels.pages.PageElementViewModel
import com.cosmejosenp.studentdiary.viewmodels.pages.factories.PageElementViewModelFactory
import com.google.android.material.dialog.MaterialAlertDialogBuilder

class PageElementFragment(val listener: PageListener? = null) : Fragment(),
    PageElementConstants {
    private lateinit var viewModel: PageElementViewModel
    private lateinit var adapter: ElementAdapter
    private lateinit var mainActivity: MainActivity
    private lateinit var window: Window
    private lateinit var recyclerView: RecyclerView
    private var actionMode: ActionMode? = null
    private lateinit var actionModeCallback: ActionMode.Callback

    /**
     * Prepare the view and make the necessary changes to attach correctly
     * the fragment with the viewModel.
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return root view.
     */
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding: FragmentPageElementBinding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_page_element,
            container,
            false
        )
        actionModeCallback = obtainActionModeObject()
        window = requireActivity().window
        mainActivity = requireActivity() as MainActivity
        recyclerView = binding.listElementRecyclerView
        val application = requireNotNull(this.activity).application
        val dataSource = EduAppDatabase.getInstance(application)
        val viewModelFactory =
            PageElementViewModelFactory(
                dataSource.elementDao,
                arguments?.getSerializable(ARG_TYPE_LIST)!! as ListElementsTypes,
                arguments?.getLong(ARG_SUBJECT)!!,
                arguments?.getLong(ARG_SUB_SUBJECT)!!,
                arguments?.getLong(ARG_BLOCK)!!,
                arguments?.getLong(ARG_SUB_BLOCK)!!,
                application
            )
        viewModel =
            ViewModelProvider(this, viewModelFactory).get(PageElementViewModel::class.java)
        if (listener != null) {
            viewModel.setListener(listener)
        }
        adapter = ElementAdapter(
            HolderListener({ codElement ->
                viewModel.startNavigateToDetailElement(codElement)
            }, { element ->
                viewModel.activateActionMode()
                viewModel.toggleItemSelected(element)
            }),
            dataSource.subjectDao,
            viewLifecycleOwner
        )
        if (arguments?.getLong(ARG_SUBJECT) == 0L && arguments?.getLong(ARG_SUB_SUBJECT) == 0L
            && arguments?.getLong(ARG_BLOCK) == 0L && arguments?.getLong(ARG_SUB_BLOCK) == 0L
        ) {
            ItemTouchHelper(obtainItemTouchHelper()).attachToRecyclerView(recyclerView)
            changeLayoutManager()
        } else {
            adapter.setTranslucentBackground()
        }
        if (arguments?.getBoolean(ARG_TYPED_BLOCK) != null && arguments?.getBoolean(ARG_TYPED_BLOCK)!!)
            adapter.setTypedElements()
        binding.viewModel = viewModel
        recyclerView.adapter = adapter
        if (viewModel.listener != null)
            recyclerView.addOnScrollListener(RecyclerViewListener(viewModel.listener!!))
        binding.lifecycleOwner = this
        addObservers()

        return binding.root
    }

    /**
     * Change the layout manager for elements list adapter.
     *
     */
    private fun changeLayoutManager() {
        if (mainActivity.sharedPreferences.getInt(
                mainActivity.SP_ELEMENT_LAYOUT,
                1
            ) == LINEAR_LAYOUT_ELEMENTS
        ) {
            recyclerView.layoutManager = LinearLayoutManager(requireContext())
        } else {
            recyclerView.layoutManager =
                StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL)
        }
    }

    /**
     * Add the necessary observers to update the view with the data and to perform events.
     *
     */
    private fun addObservers() {
        viewModel.elements.observe(viewLifecycleOwner, Observer {
            it?.let {
                adapter.submitList(it)
                if (arguments?.getLong(ARG_SUBJECT) == 0L && arguments?.getLong(
                        ARG_SUB_SUBJECT
                    ) == 0L
                    && arguments?.getLong(ARG_BLOCK) == 0L && arguments?.getLong(ARG_SUB_BLOCK) == 0L
                )
                    viewModel.startUpdateService()
            }
        })

        viewModel.navigateToDetailElement.observe(viewLifecycleOwner, Observer {
            if (it != 0L) {
                viewModel.listener!!.navigate(it)
                viewModel.doneNavigatingToDetailElement()
            }
        })

        viewModel.showSnackBarMessage.observe(viewLifecycleOwner, Observer {
            it?.let {
                viewModel.listener!!.showMessage(it) {
                    viewModel.undoLastAction()
                }
                viewModel.showSnackBarDone()
            }
        })

        viewModel.actionModeActivated.observe(viewLifecycleOwner, Observer {
            if (it) {
                if (actionMode == null) {
                    actionMode = activity?.startActionMode(actionModeCallback)
                    if (viewModel.selectedItems.isEmpty())
                        adapter.activateActionMode()
                    else {
                        adapter.activateActionMode(viewModel.selectedItems)
                        viewModel.notifyChangeTitleActionMode()
                    }
                }
            } else {
                if (actionMode != null) {
                    adapter.actionModeDone()
                    actionMode!!.finish()
                }
            }
        })

        viewModel.selectAllItems.observe(viewLifecycleOwner, Observer {
            adapter.changeSelectedAllTo(it)
        })

        viewModel.showDialogDelete.observe(viewLifecycleOwner, Observer {
            if (it) {
                MaterialAlertDialogBuilder(requireContext())
                    .setTitle(getString(R.string.deleteQuestion))
                    .setMessage(getString(R.string.deleteElementExplanation))
                    .setPositiveButton(
                        getString(R.string.yes)
                    ) { _, _ ->
                        viewModel.delete()
                    }
                    .setNegativeButton(getString(R.string.no), null)
                    .show()
                viewModel.dialogDeleteDone()
            }
        })

        viewModel.changeTitleActionMode.observe(viewLifecycleOwner, Observer {
            if (actionMode != null && it) {
                if (viewModel.titleActionMode > 1)
                    actionMode!!.title =
                        resources.getString(R.string.selectedTitlePlural, viewModel.titleActionMode)
                else
                    actionMode!!.title = resources.getString(R.string.selectedTitle, 1)
                viewModel.changeTitleActionModeDone()
            }
        })

        mainActivity.hideActionMode.observe(viewLifecycleOwner, Observer {
            if (it) {
                viewModel.actionModeDone()
            }
        })

        mainActivity.notifyLayoutElementsChange.observe(viewLifecycleOwner, Observer {
            if (it) {
                changeLayoutManager()
                mainActivity.receivedNotifyLayoutElementChange()
            }
        })
    }

    /**
     * @return an ActionMode.Callback object.
     */
    private fun obtainActionModeObject(): ActionMode.Callback =
        object : ActionMode.Callback {

            /**
             * Called when action mode is first created. The menu supplied will be used to
             * generate action buttons for the action mode and change appearance.
             *
             * @param mode ActionMode being created.
             * @param menu Menu used to populate action buttons.
             * @return true if the action mode should be created, false if entering this
             *              mode should be aborted.
             */
            override fun onCreateActionMode(mode: ActionMode?, menu: Menu?): Boolean {
                val inflater = mode?.menuInflater
                inflater?.inflate(R.menu.contextual_delete_finish, menu)
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    viewModel.setColour(window.statusBarColor)
                    window.statusBarColor =
                        ContextCompat.getColor(context!!, R.color.secondaryLightColor)
                }
                if (!viewModel.menuFinishElement) {
                    val menuItem = menu?.findItem(R.id.menuMarkAsComplete)
                    menuItem?.icon =
                        ResourcesCompat.getDrawable(
                            resources,
                            R.drawable.ic_assignment_black_24dp,
                            null
                        )
                    menuItem?.setTitle(R.string.markAsIncomplete)
                }
                return true
            }

            /**
             * Called to refresh an action mode's action menu whenever it is invalidated.
             *
             * @param mode ActionMode being prepared.
             * @param menu Menu used to populate action buttons.
             * @return true if the menu or action mode was updated, false otherwise.
             */
            override fun onPrepareActionMode(mode: ActionMode?, menu: Menu?): Boolean {
                return false
            }

            /**
             * Called to report a user click on an action button.
             *
             * @param mode The current ActionMode.
             * @param item The item that was clicked.
             * @return true if this callback handled the event, false if the standard MenuItem
             *          invocation should continue.
             */
            override fun onActionItemClicked(mode: ActionMode?, item: MenuItem?): Boolean {
                when (item?.itemId) {
                    R.id.menuSelectAll -> {
                        viewModel.selectAll()
                        viewModel.selectAllDone()
                    }
                    R.id.menuDelete -> {
                        viewModel.startDialogDelete()
                    }
                    R.id.menuMarkAsComplete -> {
                        viewModel.update()
                    }
                }
                return true
            }

            /**
             * Called when an action mode is about to be exited and destroyed.
             * Appearance color is restored too.
             *
             * @param mode The current ActionMode being destroyed.
             */
            override fun onDestroyActionMode(mode: ActionMode?) {
                actionMode = null
                viewModel.actionModeDone()
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    window.statusBarColor = viewModel.colour
                }
                adapter.actionModeDone()
            }
        }

    /**
     * @return an ItemTouchHelper.SimpleCallback object.
     */
    private fun obtainItemTouchHelper(): ItemTouchHelper.SimpleCallback = object :
        ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT) {

        /**
         * When the item change their position. In this case is not handled.
         *
         * @param recyclerView The RecyclerView to which ItemTouchHelper is attached to.
         * @param viewHolder The ViewHolder which is being dragged by the user.
         * @param target The ViewHolder over which the currently active item is being
         *               dragged.
         * @return always false in this case.
         */
        override fun onMove(
            recyclerView: RecyclerView,
            viewHolder: RecyclerView.ViewHolder,
            target: RecyclerView.ViewHolder
        ): Boolean {
            return false
        }

        /**
         * Called when a ViewHolder is swiped by the user.
         * Only if the [swipeDir] is from left, right, end or start this method will be called.
         *
         * @param viewHolder The ViewHolder which has been swiped by the user.
         * @param swipeDir  The direction to which the ViewHolder is swiped.
         */
        override fun onSwiped(viewHolder: RecyclerView.ViewHolder, swipeDir: Int) {
            val position = viewHolder.adapterPosition //get position which is swipe
            val element = adapter.currentList[position] as Element
            if (swipeDir == ItemTouchHelper.LEFT || swipeDir == ItemTouchHelper.RIGHT
                || swipeDir == ItemTouchHelper.START || swipeDir == ItemTouchHelper.END
            ) {
                viewModel.update(element)
            }
        }
    }
}