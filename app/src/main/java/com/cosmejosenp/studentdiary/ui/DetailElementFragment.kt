/*
 * MIT License
 *
 * Copyright (c) 2022 Cosme José Nieto Pérez
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.cosmejosenp.studentdiary.ui

import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.Toolbar
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import com.cosmejosenp.studentdiary.MainActivity
import com.cosmejosenp.studentdiary.R
import com.cosmejosenp.studentdiary.data.db.EduAppDatabase
import com.cosmejosenp.studentdiary.data.entity.Subject
import com.cosmejosenp.studentdiary.databinding.FragmentDetailElementBinding
import com.cosmejosenp.studentdiary.util.TYPE_HOMEWORK
import com.cosmejosenp.studentdiary.util.obtainColour
import com.cosmejosenp.studentdiary.viewmodels.DetailElementViewModel
import com.cosmejosenp.studentdiary.viewmodels.factories.DetailElementViewModelFactory


class DetailElementFragment : Fragment() {
    private lateinit var mainActivity: MainActivity
    private lateinit var viewModel: DetailElementViewModel
    private lateinit var toolbar: Toolbar
    private lateinit var navController: NavController
    private lateinit var args: DetailElementFragmentArgs
    private lateinit var coordinatorLayout: CoordinatorLayout

    /**
     * Prepare the view and make the necessary changes to attach correctly
     * the fragment with the viewModel.
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return root view.
     */
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding: FragmentDetailElementBinding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_detail_element, container, false
        )
        val application = requireNotNull(this.activity).application
        mainActivity = requireActivity() as MainActivity
        toolbar = binding.detailElementToolbar
        coordinatorLayout = binding.detailElementCoordinatorLayout
        binding.lifecycleOwner = viewLifecycleOwner
        navController = mainActivity.findNavController(R.id.myNavHostFragment)
        args = DetailElementFragmentArgs.fromBundle(requireArguments())
        val dataSource = EduAppDatabase.getInstance(application)
        val viewModelFactory = DetailElementViewModelFactory(
            dataSource.subjectDao,
            dataSource.elementDao,
            args.element,
            application
        )
        viewModel =
            ViewModelProvider(this, viewModelFactory).get(DetailElementViewModel::class.java)

        toolbar.setupWithNavController(navController, mainActivity.appBarConfiguration)
        binding.viewModel = viewModel
        binding.lifecycleOwner = this
        addObservers()
        addListeners()
        return binding.root
    }

    /**
     * Add the necessary observers to update the view with the data and to perform events.
     *
     */
    private fun addObservers() {
        viewModel.subject.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                changeAppearance(it)
            }
        })

        viewModel.element.observe(viewLifecycleOwner, Observer {
            if (it != null)
                inflateToolbarMenu()
        })

        viewModel.navigateToEditFragment.observe(viewLifecycleOwner, Observer {
            if (it) {
                //TODO
                viewModel.doneNavigatingToEditFragment()
            }
        })

        viewModel.navigateToEditGrade.observe(viewLifecycleOwner, Observer {
            if (it) {
                //TODO
                viewModel.doneNavigatingToEditGrade()
            }
        })
    }

    /**
     * Change the color appearance according to the subject.
     *
     * @param subject
     */
    private fun changeAppearance(subject: Subject) {
        val colour = obtainColour(subject.colour, resources, requireContext())
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            requireActivity().window.statusBarColor = colour.color
        }
        toolbar.setBackgroundColor(colour.color)
        coordinatorLayout.setBackgroundColor(colour.color)
    }

    /**
     * Inflate the toolbar menu.
     * Remove the add grade item if the element have [TYPE_HOMEWORK] type.
     *
     */
    private fun inflateToolbarMenu() {
        toolbar.menu.clear()
        toolbar.inflateMenu(R.menu.contextual_edit_add_grade)
        if (viewModel.element.value != null && viewModel.element.value!!.type == TYPE_HOMEWORK) {
            toolbar.menu.removeItem(R.id.menuEditAddGradeAdd)
        }
    }

    /**
     * Add listeners to perform actions when the user interact with the screen.
     *
     */
    private fun addListeners() {
        toolbar.setOnMenuItemClickListener {
            when (it.itemId) {
                R.id.menuEditAddGradeEdit -> viewModel.startNavigateToEditFragment()
                else -> viewModel.startNavigateToEditGrade()
            }
            true
        }
    }
}
