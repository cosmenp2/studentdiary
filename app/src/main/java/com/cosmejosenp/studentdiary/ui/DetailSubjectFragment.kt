/*
 * MIT License
 *
 * Copyright (c) 2022 Cosme José Nieto Pérez
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.cosmejosenp.studentdiary.ui

import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import androidx.appcompat.widget.Toolbar
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.setupWithNavController
import androidx.viewpager2.widget.ViewPager2
import com.cosmejosenp.studentdiary.MainActivity
import com.cosmejosenp.studentdiary.R
import com.cosmejosenp.studentdiary.adapters.pages.PageDetailSubjectAdapter
import com.cosmejosenp.studentdiary.data.db.EduAppDatabase
import com.cosmejosenp.studentdiary.databinding.FragmentDetailSubjectBinding
import com.cosmejosenp.studentdiary.ui.dialogs.EditSubjectDialog
import com.cosmejosenp.studentdiary.util.DetailSubjectFragmentConstants
import com.cosmejosenp.studentdiary.util.PageListener
import com.cosmejosenp.studentdiary.util.obtainColour
import com.cosmejosenp.studentdiary.viewmodels.DetailSubjectViewModel
import com.cosmejosenp.studentdiary.viewmodels.factories.DetailSubjectViewModelFactory
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator

/**
 * A simple [Fragment] subclass.
 */
class DetailSubjectFragment : Fragment(), DetailSubjectFragmentConstants {
    private lateinit var mainActivity: MainActivity
    private lateinit var toolbar: Toolbar
    private lateinit var coordinatorLayout: CoordinatorLayout
    private lateinit var viewModel: DetailSubjectViewModel
    private lateinit var viewPager: ViewPager2
    private lateinit var fabButton: FloatingActionButton
    private lateinit var tabLayout: TabLayout
    private lateinit var navController: NavController
    private lateinit var args: DetailSubjectFragmentArgs

    /**
     * Prepare the view and make the necessary changes to attach correctly
     * the fragment with the viewModel.
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return root view.
     */
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding: FragmentDetailSubjectBinding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_detail_subject, container, false
        )
        args = DetailSubjectFragmentArgs.fromBundle(requireArguments())
        coordinatorLayout = binding.detailSubjectCoordinatorLayout
        toolbar = binding.detailSubjectToolbar
        mainActivity = requireActivity() as MainActivity
        fabButton = binding.detailSubjectAddButton
        tabLayout = binding.detailSubjectTabLayout
        navController = mainActivity.findNavController(R.id.myNavHostFragment)
        val application = requireNotNull(this.activity).application
        val evaluationListener = PageListener(startNavigate = {
            viewModel.startNavigateToDetailSubSubject(it)
        }, showSnackBarMessage = { it, _ ->
            viewModel.startSnackBarMessage(it)
        }, hideFabButton = { hide ->
            viewModel.hideFabButton(hide)
        })
        val elementListener = PageListener(startNavigate = {
            viewModel.startNavigateToDetailElement(it)
        }, showSnackBarMessage = { it, function ->
            viewModel.updateFunction(function)
            viewModel.startSnackBarMessage(it)
        }, hideFabButton = { hide ->
            viewModel.hideFabButton(hide)
        })
        val pageAdapter =
            PageDetailSubjectAdapter(
                this,
                args.subject,
                elementListener,
                evaluationListener
            )
        viewPager = binding.detailSubjectViewPager
        val dataSource = EduAppDatabase.getInstance(application)
        val viewModelFactory = DetailSubjectViewModelFactory(
            dataSource.subjectDao,
            dataSource.evaluationDao,
            dataSource.subSubjectDao,
            args.subject,
            args.schoolYear,
            application
        )
        viewModel =
            ViewModelProvider(this, viewModelFactory).get(DetailSubjectViewModel::class.java)

        toolbar.setupWithNavController(navController, mainActivity.appBarConfiguration)
        toolbar.inflateMenu(R.menu.contextual_edit)
        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        viewPager.adapter = pageAdapter
        TabLayoutMediator(tabLayout, viewPager) { tab, position ->
            when (position) {
                0 -> tab.setText(R.string.activePageElements)
                1 -> tab.setText(R.string.delayedPageElements)
                2 -> tab.setText(R.string.withoutGradePageElements)
                3 -> tab.setText(R.string.finishedPageElements)
                4 -> tab.setText(R.string.evaluations)
            }
        }.attach()
        addObservers()
        addListeners()

        return binding.root
    }

    /**
     * Add the necessary observers to update the view with the data and to perform events.
     *
     */
    private fun addObservers() {
        viewModel.subject.observe(viewLifecycleOwner, Observer {
            val colour = obtainColour(it.colour, resources, requireContext())
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                requireActivity().window.statusBarColor = colour.color
            }
            toolbar.setBackgroundColor(colour.color)
            tabLayout.setBackgroundColor(colour.color)
            coordinatorLayout.setBackgroundColor(colour.color)

        })

        viewModel.subSubjectForAddElements.observe(viewLifecycleOwner, Observer {
            tryShowButton()
        })

        viewModel.navigateToAddSubSubjectFromSubject.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                navController.navigate(
                    DetailSubjectFragmentDirections.actionDetailSubjectFragmentToAddSubjectFragment(
                        it.schoolYear,
                        it.id
                    )
                )
                viewModel.doneNavigatingToAddSubSubjectFromSubject()
            }
        })

        viewModel.navigateToAddElement.observe(viewLifecycleOwner, Observer {
            if (it != 0L) {
                navController.navigate(
                    DetailSubjectFragmentDirections.actionDetailSubjectFragmentToAddElementFragment(
                        args.subject,
                        it,
                        0L,
                        0L,
                        0L
                    )
                )
                viewModel.doneNavigatingToAddElement()
            }
        })

        viewModel.navigateToDetailSubSubject.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                navController.navigate(
                    DetailSubjectFragmentDirections.actionDetailSubjectFragmentToDetailSubSubjectFragment(
                        it.subject,
                        it.id,
                        viewModel.activeSchoolYear.value!!
                    )
                )
                viewModel.doneNavigatingToDetailSubSubject()
            }
        })

        viewModel.navigateToDetailElement.observe(viewLifecycleOwner, Observer {
            if (it != 0L) {
                navController.navigate(
                    DetailSubjectFragmentDirections.actionDetailSubjectFragmentToDetailElementFragment(
                        it
                    )
                )
                viewModel.doneNavigatingToDetailElement()
            }
        })

        viewModel.active.observe(viewLifecycleOwner, Observer {
            tryShowButton()
        })

        viewModel.activeSchoolYear.observe(viewLifecycleOwner, Observer {
            tryShowButton()
        })

        viewModel.showEditDialog.observe(viewLifecycleOwner, Observer {
            if (it) {
                val fragmentManager = mainActivity.supportFragmentManager
                val bundle = Bundle()
                val dialogFragment =
                    EditSubjectDialog()
                val transaction = fragmentManager.beginTransaction()
                bundle.putLong(ARG_OBJECT, args.subject)
                dialogFragment.arguments = bundle

                transaction.setCustomAnimations(
                    R.anim.enter_save,
                    0,
                    0,
                    R.anim.pop_back_stack_exit_save
                )
                transaction
                    .add(android.R.id.content, dialogFragment)
                    .addToBackStack(null)
                    .commit()

                viewModel.doneShowEditDialog()
            }
        })

        viewModel.showSnackBarMessage.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                if (viewModel.function == null)
                    Snackbar.make(
                        coordinatorLayout,
                        it,
                        Snackbar.LENGTH_SHORT
                    ).show()
                else
                    Snackbar.make(
                        coordinatorLayout,
                        it,
                        Snackbar.LENGTH_SHORT
                    ).setAction(R.string.undo) {
                        viewModel.function!!.invoke()
                        viewModel.doneUseFunction()
                    }.show()
                viewModel.showSnackBarDone()
            }
        })

        viewModel.hideButton.observe(viewLifecycleOwner, Observer {
            if (it)
                fabButton.hide()
            else
                fabButton.show()
        })
    }

    /**
     * Try to show fab button hiding and showing for make the necessary checks.
     *
     */
    private fun tryShowButton() {
        viewModel.hideFabButton(true)
        viewModel.hideFabButton(false)
    }

    /**
     * Add listeners to perform actions when the user interact with the screen.
     *
     */
    private fun addListeners() {
        viewPager.registerOnPageChangeCallback(obtainViewPagerCallbackObject())
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner) {
            if (viewPager.currentItem > 0) {
                viewPager.currentItem = 0
            } else {
                this@DetailSubjectFragment.findNavController().popBackStack()
            }
        }

        toolbar.setOnMenuItemClickListener {
            when (it.itemId) {
                R.id.menuEdit -> viewModel.startShowEditDialog()
            }
            true
        }
    }

    /**
     * @return an ViewPager2.OnPageChangeCallback object.
     */
    private fun obtainViewPagerCallbackObject(): ViewPager2.OnPageChangeCallback =
        object : ViewPager2.OnPageChangeCallback() {

            /**
             * Perform the necessary action to maintain the correct behaviour.
             *
             * @param position of the actual page. Start in 0.
             */
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                if (viewModel.oldPosition != position) {
                    mainActivity.requestHideActionMode()
                    viewModel.setLastPosition(position)
                    tryShowButton()
                }
            }
        }
}
