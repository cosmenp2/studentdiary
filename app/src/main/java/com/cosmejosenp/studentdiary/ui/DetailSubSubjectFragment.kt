/*
 * MIT License
 *
 * Copyright (c) 2022 Cosme José Nieto Pérez
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.cosmejosenp.studentdiary.ui

import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import androidx.appcompat.widget.Toolbar
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.setupWithNavController
import androidx.viewpager2.widget.ViewPager2
import com.cosmejosenp.studentdiary.MainActivity
import com.cosmejosenp.studentdiary.R
import com.cosmejosenp.studentdiary.adapters.pages.PageDetailSubSubjectAdapter
import com.cosmejosenp.studentdiary.data.db.EduAppDatabase
import com.cosmejosenp.studentdiary.databinding.FragmentDetailSubSubjectBinding
import com.cosmejosenp.studentdiary.util.DetailSubSubjectFragmentConstants
import com.cosmejosenp.studentdiary.util.PageListener
import com.cosmejosenp.studentdiary.util.obtainColour
import com.cosmejosenp.studentdiary.viewmodels.DetailSubSubjectViewModel
import com.cosmejosenp.studentdiary.viewmodels.factories.DetailSubSubjectViewModelFactory
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator

/**
 * A simple [Fragment] subclass.
 */
class DetailSubSubjectFragment : Fragment(), DetailSubSubjectFragmentConstants {
    private lateinit var mainActivity: MainActivity
    private lateinit var toolbar: Toolbar
    private lateinit var coordinatorLayout: CoordinatorLayout
    private lateinit var viewModel: DetailSubSubjectViewModel
    private lateinit var viewPager: ViewPager2
    private lateinit var fabButton: FloatingActionButton
    private lateinit var tabLayout: TabLayout
    private lateinit var navController: NavController
    private lateinit var args: DetailSubSubjectFragmentArgs

    /**
     * Prepare the view and make the necessary changes to attach correctly
     * the fragment with the viewModel.
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return root view.
     */
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding: FragmentDetailSubSubjectBinding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_detail_sub_subject, container, false
        )
        args = DetailSubSubjectFragmentArgs.fromBundle(requireArguments())
        coordinatorLayout = binding.detailSubSubjectCoordinatorLayout
        toolbar = binding.detailSubSubjectToolbar
        mainActivity = requireActivity() as MainActivity
        fabButton = binding.detailSubSubjectAddButton
        tabLayout = binding.detailSubSubjectTabLayout
        navController = mainActivity.findNavController(R.id.myNavHostFragment)
        val application = requireNotNull(this.activity).application
        val blockListener = PageListener(startNavigate = {
            viewModel.startNavigateToDetailBlock(it)
        }, showSnackBarMessage = { it, _ ->
            viewModel.startSnackBarMessage(it)
        }, hideFabButton = { hide ->
            viewModel.hideFabButton(hide)
        })
        val elementListener = PageListener(startNavigate = {
            viewModel.startNavigateToDetailElement(it)
        }, showSnackBarMessage = { it, function ->
            viewModel.updateFunction(function)
            viewModel.startSnackBarMessage(it)
        }, hideFabButton = { hide ->
            viewModel.hideFabButton(hide)
        })
        val pageAdapter =
            PageDetailSubSubjectAdapter(
                this,
                args.subSubject,
                elementListener,
                blockListener
            )
        viewPager = binding.detailSubSubjectViewPager
        val dataSource = EduAppDatabase.getInstance(application)
        val viewModelFactory = DetailSubSubjectViewModelFactory(
            dataSource.subjectDao,
            dataSource.subSubjectDao,
            dataSource.evaluationDao,
            args.active,
            args.subject,
            args.subSubject,
            application
        )
        viewModel =
            ViewModelProvider(this, viewModelFactory).get(DetailSubSubjectViewModel::class.java)

        toolbar.setupWithNavController(navController, mainActivity.appBarConfiguration)
        toolbar.inflateMenu(R.menu.contextual_copy_structure)
        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        viewPager.adapter = pageAdapter
        TabLayoutMediator(tabLayout, viewPager) { tab, position ->
            when (position) {
                0 -> tab.setText(R.string.activePageElements)
                1 -> tab.setText(R.string.delayedPageElements)
                2 -> tab.setText(R.string.withoutGradePageElements)
                3 -> tab.setText(R.string.finishedPageElements)
                4 -> tab.setText(R.string.blocks)
            }
        }.attach()
        addObservers()
        addListeners()

        return binding.root
    }

    /**
     * Add the necessary observers to update the view with the data and to perform events.
     *
     */
    private fun addObservers() {
        viewModel.subject.observe(viewLifecycleOwner, Observer {
            val colour = obtainColour(it.colour, resources, requireContext())
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                requireActivity().window.statusBarColor = colour.color
            }
            toolbar.setBackgroundColor(colour.color)
            tabLayout.setBackgroundColor(colour.color)
            coordinatorLayout.setBackgroundColor(colour.color)
            tryShowButton()
        })

        viewModel.canAddElements.observe(viewLifecycleOwner, Observer {
            if (it != null)
                tryShowButton()
        })

        viewModel.active.observe(viewLifecycleOwner, Observer {
            inflateToolbarMenu()
            tryShowButton()
        })

        viewModel.existOtherEvaluations.observe(viewLifecycleOwner, Observer {
            inflateToolbarMenu()
        })

        viewModel.existOtherValidStructureEvaluations.observe(viewLifecycleOwner, Observer {
            inflateToolbarMenu()
        })

        viewModel.navigateToAddBlock.observe(viewLifecycleOwner, Observer {
            if (it != 0L) {
                navController.navigate(
                    DetailSubSubjectFragmentDirections.actionDetailSubSubjectFragmentToAddBlockFragment(
                        args.subject,
                        args.subSubject,
                        0L
                    )
                )
                viewModel.doneNavigatingToAddBlock()
            }
        })

        viewModel.navigateToAddElement.observe(viewLifecycleOwner, Observer {
            if (it != 0L) {
                navController.navigate(
                    DetailSubSubjectFragmentDirections.actionDetailSubSubjectFragmentToAddElementFragment(
                        args.subject,
                        it,
                        0L,
                        0L,
                        0L
                    )
                )
                viewModel.doneNavigatingToAddElement()
            }
        })

        viewModel.navigateToDetailBlock.observe(viewLifecycleOwner, Observer {
            if (it != 0L) {
                navController.navigate(
                    DetailSubSubjectFragmentDirections.actionDetailSubSubjectFragmentToDetailBlockFragment(
                        args.subject,
                        it,
                        viewModel.activeSchoolYear
                    )
                )
                viewModel.doneNavigatingToDetailBlock()
            }
        })

        viewModel.navigateToDetailElement.observe(viewLifecycleOwner, Observer {
            if (it != 0L) {
                navController.navigate(
                    DetailSubSubjectFragmentDirections.actionDetailSubSubjectFragmentToDetailElementFragment(
                        it
                    )
                )
                viewModel.doneNavigatingToDetailElement()
            }
        })

        viewModel.navigateToCopyTo.observe(viewLifecycleOwner, Observer {
            if (it != 0L) {
                navController.navigate(
                    DetailSubSubjectFragmentDirections.actionDetailSubSubjectFragmentToCopyToFragment(
                        args.subject,
                        it
                    )
                )
                viewModel.doneNavigatingToCopyTo()
            }
        })

        viewModel.navigateToCopyFrom.observe(viewLifecycleOwner, Observer {
            if (it != 0L) {
                navController.navigate(
                    DetailSubSubjectFragmentDirections.actionDetailSubSubjectFragmentToCopyFromFragment(
                        args.subject,
                        it
                    )
                )
                viewModel.doneNavigatingToCopyFrom()
            }
        })

        viewModel.notifyUpdateUI.observe(viewLifecycleOwner, Observer {
            if (it) {
                tryShowButton()
                inflateToolbarMenu()
                viewModel.doneUpdateUI()
            }
        })

        viewModel.showSnackBarMessage.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                if (viewModel.function == null)
                    Snackbar.make(
                        coordinatorLayout,
                        it,
                        Snackbar.LENGTH_SHORT
                    ).show()
                else
                    Snackbar.make(
                        coordinatorLayout,
                        it,
                        Snackbar.LENGTH_SHORT
                    ).setAction(R.string.undo) {
                        viewModel.function!!.invoke()
                        viewModel.doneUseFunction()
                    }.show()
                viewModel.showSnackBarDone()
            }
        })

        viewModel.hideButton.observe(viewLifecycleOwner, Observer {
            if (it)
                fabButton.hide()
            else
                fabButton.show()
        })
    }

    /**
     * Try to show fab button hiding and showing for make the necessary checks.
     *
     */
    private fun tryShowButton() {
        viewModel.hideFabButton(true)
        viewModel.hideFabButton(false)
    }

    /**
     * Inflate the toolbar menu.
     * Perform the necessary checks and add or remove menu items.
     *
     */
    private fun inflateToolbarMenu() {
        toolbar.menu.clear()
        toolbar.inflateMenu(R.menu.contextual_copy_structure)
        viewModel.active.value?.let {
            if (it && viewModel.activeSchoolYear)
                toolbar.menu.add(R.string.markAsFinishedAndActiveNext)
            else
                toolbar.menu.add(R.string.markAsActive)
        }
        if (viewModel.existOtherEvaluations.value == null || !viewModel.existOtherEvaluations.value!!
            || !viewModel.activeSchoolYear || viewModel.oldPosition != 4
        ) {
            toolbar.menu.removeItem(R.id.menuCopyStructureTo)
        }
        if (viewModel.existOtherValidStructureEvaluations.value == null
            || !viewModel.existOtherValidStructureEvaluations.value!!
            || !viewModel.activeSchoolYear || viewModel.oldPosition != 4
        )
            toolbar.menu.removeItem(R.id.menuCopyStructureFrom)
    }

    /**
     * Add listeners to perform actions when the user interact with the screen.
     *
     */
    private fun addListeners() {
        viewPager.registerOnPageChangeCallback(obtainViewPagerCallbackObject())
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner) {
            if (viewPager.currentItem > 0) {
                viewPager.currentItem = 0
            } else {
                this@DetailSubSubjectFragment.findNavController().popBackStack()
            }
        }

        toolbar.setOnMenuItemClickListener {
            when (it.itemId) {
                R.id.menuCopyStructureFrom -> viewModel.startNavigateToCopyFrom()
                R.id.menuCopyStructureTo -> viewModel.startNavigateToCopyTo()
                else -> viewModel.startUpdate()
            }
            true
        }
    }

    /**
     * @return an ViewPager2.OnPageChangeCallback object.
     */
    private fun obtainViewPagerCallbackObject(): ViewPager2.OnPageChangeCallback =
        object : ViewPager2.OnPageChangeCallback() {
            /**
             * Perform the necessary action to maintain the correct behaviour.
             *
             * @param position of the actual page. Start in 0.
             */
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                if (viewModel.oldPosition != position) {
                    mainActivity.requestHideActionMode()
                    viewModel.setLastPosition(position)
                    inflateToolbarMenu()
                    tryShowButton()
                }
            }
        }
}
