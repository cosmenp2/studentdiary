/*
 * MIT License
 *
 * Copyright (c) 2022 Cosme José Nieto Pérez
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.cosmejosenp.studentdiary.ui

import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.activity.addCallback
import androidx.appcompat.widget.Toolbar
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.content.ContextCompat
import androidx.core.content.ContextCompat.getSystemService
import androidx.core.widget.addTextChangedListener
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import com.cosmejosenp.studentdiary.MainActivity
import com.cosmejosenp.studentdiary.R
import com.cosmejosenp.studentdiary.adapters.ColorAdapter
import com.cosmejosenp.studentdiary.adapters.EvaluationAdapterForAddSubject
import com.cosmejosenp.studentdiary.data.db.EduAppDatabase
import com.cosmejosenp.studentdiary.databinding.FragmentAddSubjectBinding
import com.cosmejosenp.studentdiary.util.Colour
import com.cosmejosenp.studentdiary.util.HolderListener
import com.cosmejosenp.studentdiary.util.obtainColour
import com.cosmejosenp.studentdiary.util.obtainListOfColours
import com.cosmejosenp.studentdiary.viewmodels.AddSubjectViewModel
import com.cosmejosenp.studentdiary.viewmodels.factories.AddSubjectViewModelFactory
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.snackbar.Snackbar


class AddSubjectFragment : Fragment() {
    private val BUNDLE_TITLE = "title"
    private val BUNDLE_CONTINUOUS_ASSESSMENT = "continuous_assessment"
    private val BUNDLE_RADIO_STANDARD = "radio_standard"
    private val BUNDLE_RADIO_UNITS_STANDARD = "radio_units_standard"
    private val BUNDLE_RADIO_PERSONALIZED = "radio_personalized"
    private val BUNDLE_RADIO_EXT_STANDARD = "radio_ext_standard"
    private val BUNDLE_RADIO_EXT_EXAM_WORK = "radio_ext_exam_work"
    private val BUNDLE_EDIT_TEXT_EXAM = "edit_text_exam"
    private val BUNDLE_EDIT_TEXT_WORK = "edit_text_work"
    private val BUNDLE_EDIT_TEXT_HOMEWORK = "edit_text_homework"
    private val BUNDLE_EDIT_TEXT_EXT_EXAM = "edit_text_ext_exam"
    private val BUNDLE_EDIT_TEXT_EXT_WORK_EXAM = "edit_text_ext_exam_work"
    private lateinit var mainActivity: MainActivity
    private lateinit var viewModel: AddSubjectViewModel
    private lateinit var toolbar: Toolbar
    private lateinit var adapter: EvaluationAdapterForAddSubject
    private var titleTextView: TextView? = null
    private var continuousAssessment: CheckBox? = null
    private lateinit var navController: NavController
    private lateinit var args: AddSubjectFragmentArgs
    private var radioStandard: RadioButton? = null
    private var radioUnitsStandard: RadioButton? = null
    private var radioPersonalized: RadioButton? = null
    private var radioExtStandard: RadioButton? = null
    private var radioExtWorkAndExams: RadioButton? = null
    private var editTextExams: EditText? = null
    private var editTextWork: EditText? = null
    private var editTextHomework: EditText? = null
    private var editTextExtExams: EditText? = null
    private var editTextExtWork: EditText? = null
    private lateinit var horizontalLayoutStandard: LinearLayout
    private lateinit var horizontalLayoutExtraordinary: LinearLayout
    private lateinit var radioGroup: RadioGroup
    private lateinit var radioGroupExtraordinary: RadioGroup
    private lateinit var colorAdapter: ColorAdapter
    private lateinit var coordinatorLayout: CoordinatorLayout

    /**
     * Prepare the view and make the necessary changes to attach correctly
     * the fragment with the viewModel.
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return root view.
     */
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding: FragmentAddSubjectBinding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_add_subject, container, false
        )
        val application = requireNotNull(this.activity).application
        val colours = obtainListOfColours(resources, requireContext())
        mainActivity = requireActivity() as MainActivity
        toolbar = binding.addSubjectToolbar
        titleTextView = binding.addSubjectName
        continuousAssessment = binding.addSubjectContinuousAssessment
        radioStandard = binding.addSubjectStandardStructureButton
        radioExtStandard = binding.addSubjectStandardStructureButtonExt
        radioUnitsStandard = binding.addSubjectUnitsWithStandardStructureButton
        radioExtWorkAndExams = binding.addSubjectExamsAndWork
        radioPersonalized = binding.addSubjectPersonalizedButton
        horizontalLayoutStandard = binding.addSubjectPercentageGroup
        horizontalLayoutExtraordinary = binding.addSubjectPercentageGroupExt
        editTextExams = binding.addSubjectNumberExams
        editTextExtExams = binding.addSubjectNumberExamsExt
        editTextWork = binding.addSubjectNumberWorks
        editTextExtWork = binding.addSubjectNumberWorksExt
        editTextHomework = binding.addSubjectNumberHomework
        radioGroup = binding.addSubjectStructureGroup
        radioGroupExtraordinary = binding.addSubjectStructureGroupExt
        coordinatorLayout = binding.addSubjectCoordinatorLayout
        binding.lifecycleOwner = viewLifecycleOwner
        navController = mainActivity.findNavController(R.id.myNavHostFragment)
        args = AddSubjectFragmentArgs.fromBundle(requireArguments())
        val dataSource = EduAppDatabase.getInstance(application)
        val viewModelFactory = AddSubjectViewModelFactory(
            dataSource,
            args.schoolYear,
            args.subject,
            application
        )
        viewModel =
            ViewModelProvider(this, viewModelFactory).get(AddSubjectViewModel::class.java)
        adapter = EvaluationAdapterForAddSubject(
            HolderListener({
                // For simplicity this function is not implemented, instead we use the long click for a single click
            }, {
                viewModel.toggleItemSelected(it)
                evaluateData()
            }),
            viewLifecycleOwner
        )
        colorAdapter = ColorAdapter(HolderListener({
            // For simplicity this function is not implemented, instead we use the long click for a single click
        }, {
            viewModel.selectColour(it)
        }), viewLifecycleOwner)

        toolbar.setupWithNavController(navController, mainActivity.appBarConfiguration)
        toolbar.navigationIcon =
            ContextCompat.getDrawable(requireContext(), R.drawable.ic_close_black_24dp)
        binding.addSubjectRecyclerView.adapter = adapter
        binding.addSubjectColorRecyclerView.adapter = colorAdapter
        binding.viewModel = viewModel
        binding.lifecycleOwner = this
        colorAdapter.submitList(colours)
        addObservers()
        restoreContent(savedInstanceState)
        if (savedInstanceState == null) {
            viewModel.selectColour(colours!![0])
        }
        addListeners()

        return binding.root
    }

    /**
     * Add the necessary observers to update the view with the data and to perform events.
     *
     */
    private fun addObservers() {
        viewModel.subject.observe(viewLifecycleOwner, Observer {
            it?.let {
                viewModel.selectColour(obtainColour(it.colour, resources, requireContext()))
                toolbar.title = getString(R.string.linkNewEvaluations)
            }
        })

        viewModel.navigateToDetailSubject.observe(viewLifecycleOwner, Observer {
            if (it != 0L) {
                hideKeyboard()
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    requireActivity().window.statusBarColor =
                        ContextCompat.getColor(requireContext(), R.color.primaryColor)
                }
                navController.navigate(
                    AddSubjectFragmentDirections.actionAddSubjectFragmentToDetailSubjectFragment(
                        it,
                        args.schoolYear
                    )
                )
                viewModel.doneNavigatingToDetailSubject()
            }
        })

        viewModel.navigateToBackStack.observe(viewLifecycleOwner, Observer {
            if (it) {
                hideKeyboard()
                navController.popBackStack()
                viewModel.doneNavigatingToBackStack()
            }
        })

        viewModel.evaluations.observe(viewLifecycleOwner, Observer {
            it?.let {
                adapter.submitList(it)
                if (viewModel.selectAll.value!!) {
                    viewModel.selectAllEvaluations()
                    evaluateData()
                    viewModel.doneSelectAll()
                }
            }
        })

        viewModel.showDialog.observe(viewLifecycleOwner, Observer {
            if (it) {
                MaterialAlertDialogBuilder(requireContext())
                    .setMessage(getString(R.string.discardQuestion))
                    .setPositiveButton(
                        getString(R.string.continueEditing)
                        , null
                    )
                    .setNegativeButton(getString(R.string.discard)) { _, _ ->
                        viewModel.startNavigateToBackStack()
                    }
                    .show()
                viewModel.showDialogDone()
            }
        })

        viewModel.showSnackBarMessage.observe(viewLifecycleOwner, Observer {
            it?.let {
                Snackbar.make(
                    mainActivity.findViewById(android.R.id.content),
                    it,
                    Snackbar.LENGTH_SHORT
                ).show()
                viewModel.showSnackBarMessageDone()
            }
        })

        viewModel.showToastMessage.observe(viewLifecycleOwner, Observer {
            it?.let {
                Toast.makeText(requireContext(), it, Toast.LENGTH_LONG).show()
                viewModel.showToastMessageDone()
            }
        })

        viewModel.restoreSelectedItems.observe(viewLifecycleOwner, Observer {
            if (it) {
                adapter.changeSelected(viewModel.selectedItems)
                viewModel.restoreContentDone()
            }
        })

        viewModel.showErrorPercentages.observe(viewLifecycleOwner, Observer {
            if (it)
                editTextHomework?.error = getString(R.string.errorSumPercentages)
            else
                editTextHomework?.error = null

        })

        viewModel.showErrorExtPercentages.observe(viewLifecycleOwner, Observer {
            if (it)
                editTextExtWork?.error = getString(R.string.errorSumPercentages)
            else
                editTextExtWork?.error = null

        })

        viewModel.validData.observe(viewLifecycleOwner, Observer {
            toolbar.menu.findItem(R.id.saveMenuSave).isEnabled = it
        })

        viewModel.selectedColour.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                val colorList = ArrayList<Colour>()
                colorList.add(it)
                colorAdapter.changeSelected(colorList)
                changeAppearanceColor(it)
            }
        })
    }

    /**
     * Hide the keyboard before exit from dialog for prevent a keyboard opened.
     *
     */
    private fun hideKeyboard() {
        val iim: InputMethodManager =
            getSystemService(requireContext(), InputMethodManager::class.java)!!
        iim.hideSoftInputFromWindow(requireView().windowToken, 0)
    }

    /**
     * Restore information on configuration changes.
     *
     * @param savedInstanceState
     */
    private fun restoreContent(savedInstanceState: Bundle?) {
        savedInstanceState?.let {
            titleTextView?.text = it.getString(BUNDLE_TITLE)
            continuousAssessment?.isChecked = it.getBoolean(BUNDLE_CONTINUOUS_ASSESSMENT)
            radioStandard?.isChecked = it.getBoolean(BUNDLE_RADIO_STANDARD)
            radioExtStandard?.isChecked = it.getBoolean(BUNDLE_RADIO_EXT_STANDARD)
            radioUnitsStandard?.isChecked = it.getBoolean(BUNDLE_RADIO_UNITS_STANDARD)
            radioExtWorkAndExams?.isChecked = it.getBoolean(BUNDLE_RADIO_EXT_EXAM_WORK)
            radioPersonalized?.isChecked = it.getBoolean(BUNDLE_RADIO_PERSONALIZED)
            editTextExams?.setText(it.getString(BUNDLE_EDIT_TEXT_EXAM))
            editTextExtExams?.setText(it.getString(BUNDLE_EDIT_TEXT_EXT_EXAM))
            editTextWork?.setText(it.getString(BUNDLE_EDIT_TEXT_WORK))
            editTextExtWork?.setText(it.getString(BUNDLE_EDIT_TEXT_EXT_WORK_EXAM))
            editTextHomework?.setText(it.getString(BUNDLE_EDIT_TEXT_HOMEWORK))
            viewModel.startRestoreContent()
        }
    }

    /**
     * Add listeners to perform actions when the user interact with the screen.
     *
     */
    private fun addListeners() {
        titleTextView?.addTextChangedListener { evaluateData() }

        toolbar.setOnMenuItemClickListener {
            viewModel.saveSubject(
                titleTextView?.text.toString(),
                continuousAssessment!!.isChecked,
                radioStandard!!.isChecked,
                radioUnitsStandard!!.isChecked,
                radioExtStandard!!.isChecked,
                editTextExams?.text.toString(),
                editTextWork?.text.toString(),
                editTextHomework?.text.toString(),
                editTextExtExams?.text.toString(),
                editTextExtWork?.text.toString()
            )
            true
        }

        radioGroup.setOnCheckedChangeListener { _, _ ->
            viewModel.checkVisibilityStandard(radioPersonalized!!.isChecked)
            evaluateData()
        }

        radioGroupExtraordinary.setOnCheckedChangeListener { _, _ ->
            viewModel.checkVisibilityExtraordinary(radioExtWorkAndExams!!.isChecked)
            evaluateData()
        }

        editTextExams?.addTextChangedListener { evaluateData() }
        editTextWork?.addTextChangedListener { evaluateData() }
        editTextHomework?.addTextChangedListener { evaluateData() }
        editTextExtExams?.addTextChangedListener { evaluateData() }
        editTextExtWork?.addTextChangedListener { evaluateData() }

        toolbar.setNavigationOnClickListener {
            handleBackButton()
        }

        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner) {
            handleBackButton()
        }
    }

    /**
     * Simply call to viewModel evaluate data function for do the checks
     *
     */
    private fun evaluateData() {
        viewModel.evaluateData(
            titleTextView?.text.toString(),
            editTextExams?.text.toString(),
            editTextWork?.text.toString(),
            editTextHomework?.text.toString(),
            editTextExtExams?.text.toString(),
            editTextExtWork?.text.toString()
        )
    }

    /**
     * Change the color appearance according to the color selected.
     *
     * @param colour
     */
    private fun changeAppearanceColor(colour: Colour) {
        toolbar.setBackgroundColor(colour.color)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            requireActivity().window.statusBarColor = colour.color
        }
        coordinatorLayout.setBackgroundColor(colour.color)
    }

    /**
     * Handle the action when back button is pressed.
     *
     */
    private fun handleBackButton() {
        if (viewModel.validData.value!!) {
            viewModel.startShowDialog()
        } else {
            viewModel.startNavigateToBackStack()
        }
    }

    /**
     * Save the volatile data when configuration change.
     *
     * @param outState
     */
    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.let {
            it.putString(BUNDLE_TITLE, titleTextView?.text.toString())
            continuousAssessment?.isChecked?.let { it1 ->
                it.putBoolean(
                    BUNDLE_CONTINUOUS_ASSESSMENT,
                    it1
                )
            }
            radioStandard?.isChecked?.let { it1 -> it.putBoolean(BUNDLE_RADIO_STANDARD, it1) }
            radioExtStandard?.isChecked?.let { it1 ->
                it.putBoolean(
                    BUNDLE_RADIO_EXT_STANDARD,
                    it1
                )
            }
            radioUnitsStandard?.isChecked?.let { it1 ->
                it.putBoolean(
                    BUNDLE_RADIO_UNITS_STANDARD,
                    it1
                )
            }
            radioExtWorkAndExams?.isChecked?.let { it1 ->
                it.putBoolean(
                    BUNDLE_RADIO_EXT_EXAM_WORK,
                    it1
                )
            }
            radioPersonalized?.isChecked?.let { it1 ->
                it.putBoolean(
                    BUNDLE_RADIO_PERSONALIZED,
                    it1
                )
            }
            it.putString(BUNDLE_EDIT_TEXT_EXAM, editTextExams?.text.toString())
            it.putString(BUNDLE_EDIT_TEXT_EXT_EXAM, editTextExtExams?.text.toString())
            it.putString(BUNDLE_EDIT_TEXT_WORK, editTextWork?.text.toString())
            it.putString(BUNDLE_EDIT_TEXT_EXT_WORK_EXAM, editTextExtWork?.text.toString())
            it.putString(BUNDLE_EDIT_TEXT_HOMEWORK, editTextHomework?.text.toString())
        }
    }
}
