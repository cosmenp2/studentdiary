/*
 * MIT License
 *
 * Copyright (c) 2022 Cosme José Nieto Pérez
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.cosmejosenp.studentdiary.ui

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.activity.addCallback
import androidx.appcompat.widget.Toolbar
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.content.ContextCompat
import androidx.core.content.ContextCompat.getSystemService
import androidx.core.widget.addTextChangedListener
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import com.cosmejosenp.studentdiary.MainActivity
import com.cosmejosenp.studentdiary.R
import com.cosmejosenp.studentdiary.data.db.EduAppDatabase
import com.cosmejosenp.studentdiary.data.entity.Block
import com.cosmejosenp.studentdiary.data.entity.SubBlock
import com.cosmejosenp.studentdiary.data.entity.Subject
import com.cosmejosenp.studentdiary.databinding.FragmentAddElementBinding
import com.cosmejosenp.studentdiary.util.*
import com.cosmejosenp.studentdiary.viewmodels.AddElementViewModel
import com.cosmejosenp.studentdiary.viewmodels.factories.AddElementViewModelFactory
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.snackbar.Snackbar
import java.util.*


class AddElementFragment : Fragment() {
    private val BUNDLE_TITLE = "title"
    private val BUNDLE_DESCRIPTION = "description"
    private lateinit var mainActivity: MainActivity
    private lateinit var viewModel: AddElementViewModel
    private lateinit var toolbar: Toolbar
    private lateinit var navController: NavController
    private lateinit var args: AddElementFragmentArgs
    private lateinit var coordinatorLayout: CoordinatorLayout
    private lateinit var radioExam: RadioButton
    private lateinit var radioWork: RadioButton
    private lateinit var radioHomework: RadioButton
    private lateinit var radioGroup: RadioGroup
    private lateinit var subjectSpinner: Spinner
    private var subjectSpinnerAdapter: ArrayAdapter<Subject>? = null
    private lateinit var blocksSpinner: Spinner
    private var blocksSpinnerAdapter: ArrayAdapter<Block>? = null
    private lateinit var subBlocksSpinner: Spinner
    private var subBlocksSpinnerAdapter: ArrayAdapter<SubBlock>? = null
    private var titleTextView: EditText? = null
    private lateinit var dateSpinner: Spinner
    private var dateSpinnerAdapter: ArrayAdapter<CalendarAdapter>? = null
    private lateinit var timeSpinner: Spinner
    private var timeSpinnerAdapter: ArrayAdapter<TimeAdapter>? = null
    private var descriptionTextView: EditText? = null
    private var lock = false

    /**
     * Prepare the view and make the necessary changes to attach correctly
     * the fragment with the viewModel.
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return root view.
     */
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding: FragmentAddElementBinding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_add_element, container, false
        )
        val application = requireNotNull(this.activity).application
        mainActivity = requireActivity() as MainActivity
        toolbar = binding.addElementToolbar
        coordinatorLayout = binding.addElementCoordinatorLayout
        radioExam = binding.addElementExamsButton
        radioWork = binding.addElementWorkButton
        radioHomework = binding.addElementHomeWorkButton
        radioGroup = binding.addElementGroup
        titleTextView = binding.addElementName
        subjectSpinner = binding.addElementSubjectSpinner
        blocksSpinner = binding.addElementBlockSpinner
        subBlocksSpinner = binding.addElementSubBlockSpinner
        dateSpinner = binding.addElementDateSpinner
        timeSpinner = binding.addElementTimeSpinner
        descriptionTextView = binding.addElementDescription
        binding.lifecycleOwner = viewLifecycleOwner
        navController = mainActivity.findNavController(R.id.myNavHostFragment)
        args = AddElementFragmentArgs.fromBundle(requireArguments())
        val dataSource = EduAppDatabase.getInstance(application)
        val viewModelFactory = AddElementViewModelFactory(
            dataSource.subjectDao,
            dataSource.subSubjectDao,
            dataSource.blockDao,
            dataSource.subBlockDao,
            dataSource.elementDao,
            args.subject,
            args.subSubject,
            args.block,
            args.subBlock,
            args.element,
            application
        )
        viewModel =
            ViewModelProvider(this, viewModelFactory).get(AddElementViewModel::class.java)

        toolbar.setupWithNavController(navController, mainActivity.appBarConfiguration)
        toolbar.navigationIcon =
            ContextCompat.getDrawable(requireContext(), R.drawable.ic_close_black_24dp)
        binding.viewModel = viewModel
        binding.lifecycleOwner = this
        addObservers()
        restoreContent(savedInstanceState)
        addListeners()
        return binding.root
    }

    /**
     * Add the necessary observers to update the view with the data and to perform events.
     *
     */
    private fun addObservers() {
        viewModel.subjects.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                subjectSpinnerAdapter = ArrayAdapter(
                    this@AddElementFragment.requireContext(),
                    android.R.layout.simple_spinner_item,
                    it
                ).also { adapter ->
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                    subjectSpinner.adapter = adapter
                }
                if (it.isNotEmpty()) {
                    lock = true
                    viewModel.selectSubject(it[0])
                }
                viewModel.populateSpinners()
            }
        })

        viewModel.element.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                if (viewModel.firstLoad) {
                    titleTextView?.setText(it.name)
                    descriptionTextView?.setText(it.description)
                    viewModel.doneFirstLoad()
                    viewModel.populateSpinners()
                }
            }
        })

        viewModel.subjectByCod.observe(viewLifecycleOwner, Observer {
            it?.let {
                changeAppearance(it)
                viewModel.checkCods()
                viewModel.populateSpinners()
            }
        })

        viewModel.selectedSubject.observe(viewLifecycleOwner, Observer {
            it?.let {
                changeAppearance(it)
                viewModel.populateSpinners()
            }
        })

        viewModel.unitsBlocks.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                blocksSpinnerAdapter = ArrayAdapter(
                    this@AddElementFragment.requireContext(),
                    android.R.layout.simple_spinner_item,
                    it
                ).also { adapter ->
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                    blocksSpinner.adapter = adapter
                }
            }
        })

        viewModel.unitsSubBlocks.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                subBlocksSpinnerAdapter = ArrayAdapter(
                    this@AddElementFragment.requireContext(),
                    android.R.layout.simple_spinner_item,
                    it
                ).also { adapter ->
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                    subBlocksSpinner.adapter = adapter
                }
            }
        })

        viewModel.isHeaderValid.observe(viewLifecycleOwner, Observer {
            if (it)
                evaluateData()
        })

        viewModel.notifyUIUpdate.observe(viewLifecycleOwner, Observer {
            if (it) {
                checkRadioButton()
                viewModel.doneNotifyUIUpdate()
            }
        })

        viewModel.calendarAdapters.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                dateSpinnerAdapter = ArrayAdapter(
                    requireContext(),
                    android.R.layout.simple_spinner_item,
                    it
                ).also { adapter ->
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                    dateSpinner.adapter = adapter
                }
                if (viewModel.selectedDate != null) {
                    lock = true
                    dateSpinner.setSelection(dateSpinnerAdapter!!.getPosition(viewModel.selectedDate))
                }
            }
        })

        viewModel.timeAdapters.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                timeSpinnerAdapter = ArrayAdapter(
                    requireContext(),
                    android.R.layout.simple_spinner_item,
                    it
                ).also { adapter ->
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                    timeSpinner.adapter = adapter
                }
                if (viewModel.selectedTime != null) {
                    lock = true
                    timeSpinner.setSelection(timeSpinnerAdapter!!.getPosition(viewModel.selectedTime))
                }
            }
        })

        viewModel.navigateToDetailElement.observe(viewLifecycleOwner, Observer {
            if (it != 0L) {
                hideKeyboard()
                navController.navigate(
                    AddElementFragmentDirections.actionAddElementFragmentToDetailElementFragment(it)
                )
                viewModel.doneNavigatingToDetailElement()
            }
        })

        viewModel.navigateToBackStack.observe(viewLifecycleOwner, Observer {
            if (it) {
                hideKeyboard()
                navController.popBackStack()
                viewModel.doneNavigatingToBackStack()
            }
        })

        viewModel.showDateDialog.observe(viewLifecycleOwner, Observer {
            if (it) {
                val calendar = Calendar.getInstance()
                dateSpinner.setSelection(dateSpinnerAdapter!!.getPosition(viewModel.selectedDate))
                DatePickerDialog(
                    requireContext(),
                    { _, year, month, dayOfMonth ->
                        viewModel.updatePersonalizedDate(year, month, dayOfMonth)
                    },
                    calendar.get(Calendar.YEAR),
                    calendar.get(Calendar.MONTH),
                    calendar.get(Calendar.DAY_OF_MONTH)
                ).show()
                viewModel.showDateDialogDone()
            }
        })

        viewModel.showTimeDialog.observe(viewLifecycleOwner, Observer {
            if (it) {
                val calendar = Calendar.getInstance()
                timeSpinner.setSelection(timeSpinnerAdapter!!.getPosition(viewModel.selectedTime))
                TimePickerDialog(
                    requireContext(),
                    { _, hourOfDay, minute ->
                        viewModel.updatePersonalizedTime(hourOfDay, minute)
                    },
                    calendar.get(Calendar.HOUR_OF_DAY),
                    calendar.get(Calendar.MINUTE),
                    true
                ).show()
                viewModel.showTimeDialogDone()
            }
        })

        viewModel.showExitDialog.observe(viewLifecycleOwner, Observer {
            if (it) {
                MaterialAlertDialogBuilder(requireContext())
                    .setMessage(getString(R.string.discardQuestion))
                    .setPositiveButton(
                        getString(R.string.continueEditing)
                        , null
                    )
                    .setNegativeButton(getString(R.string.discard)) { _, _ ->
                        viewModel.startNavigateToBackStack()
                    }
                    .show()
                viewModel.showExitDialogDone()
            }
        })

        viewModel.showSnackBarMessage.observe(viewLifecycleOwner, Observer {
            it?.let {
                Snackbar.make(
                    mainActivity.findViewById(android.R.id.content),
                    it,
                    Snackbar.LENGTH_SHORT
                ).show()
                viewModel.showSnackBarMessageDone()
            }
        })

        viewModel.restoreSelectedItem.observe(viewLifecycleOwner, Observer {
            if (it) {
                lock = true
                if (subjectSpinnerAdapter != null && viewModel.selectedSubject.value != null)
                    subjectSpinner.setSelection(subjectSpinnerAdapter!!.getPosition(viewModel.selectedSubject.value))
                lock = true
                if (blocksSpinnerAdapter != null && viewModel.selectedBlock != null)
                    blocksSpinner.setSelection(blocksSpinnerAdapter!!.getPosition(viewModel.selectedBlock))
                lock = true
                if (subBlocksSpinnerAdapter != null && viewModel.selectedSubBlock != null)
                    subBlocksSpinner.setSelection(subBlocksSpinnerAdapter!!.getPosition(viewModel.selectedSubBlock))
                lock = true
                if (dateSpinnerAdapter != null && viewModel.selectedDate != null)
                    dateSpinner.setSelection(dateSpinnerAdapter!!.getPosition(viewModel.selectedDate))
                lock = true
                if (timeSpinnerAdapter != null && viewModel.selectedTime != null)
                    timeSpinner.setSelection(timeSpinnerAdapter!!.getPosition(viewModel.selectedTime))
                checkRadioButton()
                viewModel.restoreContentDone()
            }
        })

        viewModel.validData.observe(viewLifecycleOwner, Observer {
            toolbar.menu.findItem(R.id.saveMenuSave).isEnabled = it
        })
    }

    /**
     * Change the color appearance according to the color of the subject.
     *
     * @param subject
     */
    private fun changeAppearance(subject: Subject) {
        val colour = obtainColour(subject.colour, resources, requireContext())
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            requireActivity().window.statusBarColor = colour.color
        }
        toolbar.setBackgroundColor(colour.color)
        coordinatorLayout.setBackgroundColor(colour.color)
    }

    /**
     * Perform auto check depending of available types
     *
     */
    private fun checkRadioButton() {
        radioGroup.check(
            when (viewModel.selectedType) {
                TYPE_EXAM -> radioExam.id
                TYPE_WORK -> radioWork.id
                TYPE_HOMEWORK -> radioHomework.id
                else -> -1
            }
        )
    }

    /**
     * Hide the keyboard before exit from dialog for prevent a keyboard opened.
     *
     */
    private fun hideKeyboard() {
        val iim: InputMethodManager =
            getSystemService(requireContext(), InputMethodManager::class.java)!!
        iim.hideSoftInputFromWindow(requireView().windowToken, 0)
    }

    /**
     * Restore information on configuration changes.
     *
     * @param savedInstanceState
     */
    private fun restoreContent(savedInstanceState: Bundle?) {
        savedInstanceState?.let {
            titleTextView?.setText(it.getString(BUNDLE_TITLE))
            viewModel.startRestoreContent()
        }
    }

    /**
     * Add listeners to perform actions when the user interact with the screen.
     *
     */
    private fun addListeners() {
        titleTextView?.addTextChangedListener { evaluateData() }
        descriptionTextView?.addTextChangedListener { evaluateData() }

        toolbar.setOnMenuItemClickListener {
            viewModel.save(
                titleTextView?.text.toString(),
                descriptionTextView?.text.toString()
            )
            true
        }

        toolbar.setNavigationOnClickListener {
            handleBackButton()
        }

        radioGroup.setOnCheckedChangeListener { _, checkedId ->
            when (checkedId) {
                radioExam.id -> viewModel.selectType(TYPE_EXAM)
                radioWork.id -> viewModel.selectType(TYPE_WORK)
                radioHomework.id -> viewModel.selectType(TYPE_HOMEWORK)
            }
        }

        subjectSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {}

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                if (!lock)
                    viewModel.selectSubject(parent?.getItemAtPosition(position) as Subject)
                else
                    lock = false
            }
        }

        blocksSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {}

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                if (!lock)
                    viewModel.selectBlock(parent?.getItemAtPosition(position) as Block)
                else
                    lock = false
            }
        }

        subBlocksSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {}

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                if (!lock)
                    viewModel.selectSubBlock(parent?.getItemAtPosition(position) as SubBlock)
                else
                    lock = false
            }
        }

        dateSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {}

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                if (!lock)
                    viewModel.updateSelectedDate(parent?.getItemAtPosition(position) as CalendarAdapter)
                else
                    lock = false
            }
        }

        timeSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {}

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                if (!lock)
                    viewModel.updateSelectedTime(parent?.getItemAtPosition(position) as TimeAdapter)
                else
                    lock = false
            }
        }

        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner) {
            handleBackButton()
        }
    }

    /**
     * Simply call to viewModel evaluate data function for do the checks
     *
     */
    private fun evaluateData() {
        viewModel.evaluateData(
            titleTextView?.text.toString(),
            descriptionTextView?.text.toString()
        )
    }

    /**
     * Handle the action when back button is pressed.
     *
     */
    private fun handleBackButton() {
        if (viewModel.validData.value!!) {
            viewModel.startShowExitDialog()
        } else {
            viewModel.startNavigateToBackStack()
        }
    }

    /**
     * Save the volatile data when configuration change.
     *
     * @param outState
     */
    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.let {
            it.putString(BUNDLE_TITLE, titleTextView?.text.toString())
            it.putString(BUNDLE_DESCRIPTION, descriptionTextView?.text.toString())
        }
    }
}
