/*
 * MIT License
 *
 * Copyright (c) 2022 Cosme José Nieto Pérez
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.cosmejosenp.studentdiary.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.Toolbar
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import androidx.viewpager2.widget.ViewPager2
import com.cosmejosenp.studentdiary.MainActivity
import com.cosmejosenp.studentdiary.R
import com.cosmejosenp.studentdiary.adapters.pages.PageElementAdapter
import com.cosmejosenp.studentdiary.data.db.EduAppDatabase
import com.cosmejosenp.studentdiary.databinding.FragmentMainBinding
import com.cosmejosenp.studentdiary.util.GRID_LAYOUT_ELEMENTS
import com.cosmejosenp.studentdiary.util.LINEAR_LAYOUT_ELEMENTS
import com.cosmejosenp.studentdiary.util.ListElementsTypes
import com.cosmejosenp.studentdiary.util.PageListener
import com.cosmejosenp.studentdiary.viewmodels.MainFragmentViewModel
import com.cosmejosenp.studentdiary.viewmodels.factories.MainFragmentViewModelFactory
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar

/**
 * A simple [Fragment] subclass.
 */
open class MainFragment : Fragment() {
    private lateinit var mainActivity: MainActivity
    private lateinit var toolbar: Toolbar
    private lateinit var coordinatorLayout: CoordinatorLayout
    private lateinit var viewModel: MainFragmentViewModel
    private lateinit var viewPager: ViewPager2
    private lateinit var fabButton: FloatingActionButton
    protected lateinit var navController: NavController
    open val typeList: ListElementsTypes = ListElementsTypes.ACTIVE
    open val canAdd: Boolean = true

    /**
     * Prepare the view and make the necessary changes to attach correctly
     * the fragment with the viewModel.
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return root view.
     */
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding: FragmentMainBinding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_main, container, false
        )
        coordinatorLayout = binding.mainFragmentCoordinatorLayout
        toolbar = binding.mainFragmentToolbar
        mainActivity = requireActivity() as MainActivity
        fabButton = binding.mainFragmentAddButton
        navController = mainActivity.findNavController(R.id.myNavHostFragment)
        val application = requireNotNull(this.activity).application
        val elementListener = PageListener(startNavigate = {
            viewModel.startNavigateToDetailElement(it)
        }, showSnackBarMessage = { it, function ->
            viewModel.updateFunction(function)
            viewModel.startSnackBarMessage(it)
        }, hideFabButton = { hide ->
            viewModel.hideFabButton(hide)
        })
        val pageAdapter =
            PageElementAdapter(
                this,
                elementListener,
                typeList
            )
        viewPager = binding.mainFragmentViewPager
        val dataSource = EduAppDatabase.getInstance(application)
        val viewModelFactory = MainFragmentViewModelFactory(
            canAdd,
            dataSource.subjectDao,
            application
        )
        viewModel =
            ViewModelProvider(this, viewModelFactory).get(MainFragmentViewModel::class.java)
        inflateToolbarMenu()
        toolbar.setupWithNavController(navController, mainActivity.appBarConfiguration)
        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        viewPager.adapter = pageAdapter
        addObservers()
        addListeners()

        return binding.root
    }

    /**
     * Add the necessary observers to update the view with the data and to perform events.
     *
     */
    private fun addObservers() {
        viewModel.cadAddElements.observe(viewLifecycleOwner, Observer {
            viewModel.hideFabButton(true)
            viewModel.hideFabButton(false)
        })

        viewModel.navigateToAddElement.observe(viewLifecycleOwner, Observer {
            if (it && canAdd) {
                navController.navigate(
                    MainFragmentDirections.actionMainFragmentToAddElementFragment(
                        0L,
                        0L,
                        0L,
                        0L,
                        0L
                    )
                )
                viewModel.doneNavigatingToAddElement()
            }
        })

        viewModel.navigateToDetailElement.observe(viewLifecycleOwner, Observer {
            if (it != 0L) {
                navigateToDetailElement(it)
                viewModel.doneNavigatingToDetailElement()
            }
        })

        viewModel.showSnackBarMessage.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                if (viewModel.function == null)
                    Snackbar.make(
                        coordinatorLayout,
                        it,
                        Snackbar.LENGTH_SHORT
                    ).show()
                else
                    Snackbar.make(
                        coordinatorLayout,
                        it,
                        Snackbar.LENGTH_SHORT
                    ).setAction(R.string.undo) {
                        viewModel.function!!.invoke()
                    }.show()
                viewModel.showSnackBarDone()
            }
        })

        viewModel.hideButton.observe(viewLifecycleOwner, Observer {
            if (it)
                fabButton.hide()
            else
                fabButton.show()
        })
    }

    /**
     * Navigate to detail element.
     *
     * @param it cod of the element.
     */
    protected open fun navigateToDetailElement(it: Long) {
        navController.navigate(
            MainFragmentDirections.actionMainFragmentToDetailElementFragment(it)
        )
    }

    /**
     * Inflate the toolbar menu depending of the actual layout.
     *
     */
    private fun inflateToolbarMenu() {
        toolbar.menu.clear()
        toolbar.inflateMenu(R.menu.layout_menu)
        if (mainActivity.sharedPreferences.getInt(
                mainActivity.SP_ELEMENT_LAYOUT,
                LINEAR_LAYOUT_ELEMENTS
            ) == GRID_LAYOUT_ELEMENTS
        ) {
            toolbar.menu.findItem(R.id.layoutMenuLayout).setTitle(R.string.listMode)
            toolbar.menu.findItem(R.id.layoutMenuLayout).setIcon(R.drawable.view_list_24px)
        }
    }

    /**
     * Add listeners to perform actions when the user interact with the screen.
     *
     */
    private fun addListeners() {
        toolbar.setOnMenuItemClickListener {
            mainActivity.changeElementsLayout()
            inflateToolbarMenu()
            true
        }
    }
}
