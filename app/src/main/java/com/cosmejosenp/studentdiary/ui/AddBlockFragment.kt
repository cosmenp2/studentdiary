/*
 * MIT License
 *
 * Copyright (c) 2022 Cosme José Nieto Pérez
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.cosmejosenp.studentdiary.ui

import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.RadioButton
import android.widget.RadioGroup
import androidx.activity.addCallback
import androidx.appcompat.widget.Toolbar
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.content.ContextCompat
import androidx.core.content.ContextCompat.getSystemService
import androidx.core.widget.addTextChangedListener
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import com.cosmejosenp.studentdiary.MainActivity
import com.cosmejosenp.studentdiary.R
import com.cosmejosenp.studentdiary.data.db.EduAppDatabase
import com.cosmejosenp.studentdiary.databinding.FragmentAddBlockBinding
import com.cosmejosenp.studentdiary.util.obtainColour
import com.cosmejosenp.studentdiary.viewmodels.AddBlockViewModel
import com.cosmejosenp.studentdiary.viewmodels.factories.AddBlockViewModelFactory
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.snackbar.Snackbar


class AddBlockFragment : Fragment() {
    private val BUNDLE_TITLE = "title"
    private val BUNDLE_RADIO_EXAM = "radio_exam"
    private val BUNDLE_RADIO_WORK = "radio_work"
    private val BUNDLE_RADIO_HOMEWORK = "radio_homework"
    private val BUNDLE_RADIO_UNIT = "radio_unit"
    private val BUNDLE_EDIT_TEXT_PERCENTAGE = "edit_text_percentage"
    private lateinit var mainActivity: MainActivity
    private lateinit var viewModel: AddBlockViewModel
    private lateinit var toolbar: Toolbar
    private lateinit var navController: NavController
    private lateinit var args: AddBlockFragmentArgs
    private var titleTextView: EditText? = null
    private var radioExam: RadioButton? = null
    private var radioWork: RadioButton? = null
    private var radioHomework: RadioButton? = null
    private var radioUnit: RadioButton? = null
    private var editTextPercentage: EditText? = null
    private lateinit var radioGroup: RadioGroup
    private lateinit var coordinatorLayout: CoordinatorLayout

    /**
     * Prepare the view and make the necessary changes to attach correctly
     * the fragment with the viewModel.
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return root view.
     */
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding: FragmentAddBlockBinding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_add_block, container, false
        )
        val application = requireNotNull(this.activity).application
        mainActivity = requireActivity() as MainActivity
        toolbar = binding.addBlockToolbar
        titleTextView = binding.addBlockName
        radioExam = binding.addBlockExamsButton
        radioWork = binding.addBlockWorkButton
        radioHomework = binding.addBlockHomeWorkButton
        radioUnit = binding.addBlockUnitButton
        editTextPercentage = binding.addBlockPercentage
        radioGroup = binding.addBlockGroup
        coordinatorLayout = binding.addBlockCoordinatorLayout
        binding.lifecycleOwner = viewLifecycleOwner
        navController = mainActivity.findNavController(R.id.myNavHostFragment)
        args = AddBlockFragmentArgs.fromBundle(requireArguments())
        val dataSource = EduAppDatabase.getInstance(application)
        val viewModelFactory = AddBlockViewModelFactory(
            dataSource.subjectDao,
            dataSource.blockDao,
            dataSource.subBlockDao,
            args.subject,
            args.subSubject,
            args.block,
            application
        )
        viewModel =
            ViewModelProvider(this, viewModelFactory).get(AddBlockViewModel::class.java)

        toolbar.setupWithNavController(navController, mainActivity.appBarConfiguration)
        toolbar.navigationIcon =
            ContextCompat.getDrawable(requireContext(), R.drawable.ic_close_black_24dp)
        binding.viewModel = viewModel
        binding.lifecycleOwner = this
        addObservers()
        restoreContent(savedInstanceState)
        addListeners()

        return binding.root
    }

    /**
     * Add the necessary observers to update the view with the data and to perform events.
     *
     */
    private fun addObservers() {
        viewModel.subject.observe(viewLifecycleOwner, Observer {
            it?.let {
                val colour = obtainColour(it.colour, resources, requireContext())
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    requireActivity().window.statusBarColor = colour.color
                }
                toolbar.setBackgroundColor(colour.color)
                coordinatorLayout.setBackgroundColor(colour.color)
            }
        })

        viewModel.block.observe(viewLifecycleOwner, Observer {
            viewModel.checkVisibility()
        })

        viewModel.blocks.observe(viewLifecycleOwner, Observer {
            viewModel.checkVisibility()
        })

        viewModel.subBlocks.observe(viewLifecycleOwner, Observer {
            viewModel.checkVisibility()
        })

        viewModel.isUnitAvailable.observe(viewLifecycleOwner, Observer {
            viewModel.checkVisibility()
        })

        viewModel.notifyUIUpdate.observe(viewLifecycleOwner, Observer {
            if (it) {
                checkRadioButton()
                viewModel.doneNotifyUIUpdate()
            }
        })

        viewModel.navigateToDetailBlock.observe(viewLifecycleOwner, Observer {
            if (it != 0L) {
                hideKeyboard()
                navController.navigate(
                    AddBlockFragmentDirections.actionAddBlockFragmentToDetailBlockFragment(
                        subject = args.subject,
                        block = it
                    )
                )
                viewModel.doneNavigatingToDetailBlock()
            }
        })

        viewModel.navigateToDetailSubBlock.observe(viewLifecycleOwner, Observer {
            if (it != 0L) {
                hideKeyboard()
                navController.navigate(
                    AddBlockFragmentDirections.actionAddBlockFragmentToDetailSubBlockFragment(
                        subject = args.subject,
                        subBlock = it
                    )
                )
                viewModel.doneNavigatingToDetailSubBlock()
            }
        })

        viewModel.navigateToBackStack.observe(viewLifecycleOwner, Observer {
            if (it) {
                hideKeyboard()
                navController.popBackStack()
                viewModel.doneNavigatingToBackStack()
            }
        })

        viewModel.showDialog.observe(viewLifecycleOwner, Observer {
            if (it) {
                MaterialAlertDialogBuilder(requireContext())
                    .setMessage(getString(R.string.discardQuestion))
                    .setPositiveButton(
                        getString(R.string.continueEditing)
                        , null
                    )
                    .setNegativeButton(getString(R.string.discard)) { _, _ ->
                        viewModel.startNavigateToBackStack()
                    }
                    .show()
                viewModel.showDialogDone()
            }
        })

        viewModel.showSnackBarMessage.observe(viewLifecycleOwner, Observer {
            it?.let {
                Snackbar.make(
                    mainActivity.findViewById(android.R.id.content),
                    it,
                    Snackbar.LENGTH_SHORT
                ).show()
                viewModel.showSnackBarMessageDone()
            }
        })

        viewModel.showErrorPercentages.observe(viewLifecycleOwner, Observer {
            if (it)
                editTextPercentage?.error = getString(R.string.errorRangePercentage)
            else
                editTextPercentage?.error = null

        })

        viewModel.validData.observe(viewLifecycleOwner, Observer {
            toolbar.menu.findItem(R.id.saveMenuSave).isEnabled = it
        })
    }

    /**
     * Perform auto check depending of available types
     *
     */
    private fun checkRadioButton() {
        if (viewModel.firstCheck && viewModel.isUnitAvailable.value != null) {
            radioGroup.check(
                when {
                    viewModel.isExamAvailable.value!! -> radioExam!!.id
                    viewModel.isWorkAvailable.value!! -> radioWork!!.id
                    viewModel.isHomeworkAvailable.value!! -> radioHomework!!.id
                    viewModel.isUnitAvailable.value!! -> radioUnit!!.id
                    else -> -1
                }
            )
            viewModel.doneFirstCheck()
        }
    }

    /**
     * Hide the keyboard before exit from dialog for prevent a keyboard opened.
     *
     */
    private fun hideKeyboard() {
        val iim: InputMethodManager =
            getSystemService(requireContext(), InputMethodManager::class.java)!!
        iim.hideSoftInputFromWindow(requireView().windowToken, 0)
    }

    /**
     * Restore information on configuration changes.
     *
     * @param savedInstanceState
     */
    private fun restoreContent(savedInstanceState: Bundle?) {
        savedInstanceState?.let {
            titleTextView?.setText(it.getString(BUNDLE_TITLE))
            radioExam?.isChecked = it.getBoolean(BUNDLE_RADIO_EXAM)
            radioUnit?.isChecked = it.getBoolean(BUNDLE_RADIO_UNIT)
            radioWork?.isChecked = it.getBoolean(BUNDLE_RADIO_WORK)
            radioHomework?.isChecked = it.getBoolean(BUNDLE_RADIO_HOMEWORK)
            editTextPercentage?.setText(it.getString(BUNDLE_EDIT_TEXT_PERCENTAGE))
        }
    }

    /**
     * Add listeners to perform actions when the user interact with the screen.
     *
     */
    private fun addListeners() {
        titleTextView?.addTextChangedListener { evaluateData() }

        toolbar.setOnMenuItemClickListener {
            viewModel.save(
                titleTextView?.text.toString(),
                radioExam!!.isChecked,
                radioWork!!.isChecked,
                radioHomework!!.isChecked,
                radioUnit!!.isChecked,
                editTextPercentage?.text.toString()
            )
            true
        }

        editTextPercentage?.addTextChangedListener { evaluateData() }

        toolbar.setNavigationOnClickListener {
            handleBackButton()
        }

        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner) {
            handleBackButton()
        }
    }

    /**
     * Simply call to viewModel evaluate data function for do the checks
     *
     */
    private fun evaluateData() {
        viewModel.evaluateData(
            titleTextView?.text.toString(),
            editTextPercentage?.text.toString()
        )
    }

    /**
     * Handle the action when back button is pressed.
     *
     */
    private fun handleBackButton() {
        if (viewModel.validData.value!!) {
            viewModel.startShowDialog()
        } else {
            viewModel.startNavigateToBackStack()
        }
    }

    /**
     * Save the volatile data when configuration change.
     *
     * @param outState
     */
    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.let {
            it.putString(BUNDLE_TITLE, titleTextView?.text.toString())
            radioExam?.isChecked?.let { it1 -> it.putBoolean(BUNDLE_RADIO_EXAM, it1) }
            radioUnit?.isChecked?.let { it1 -> it.putBoolean(BUNDLE_RADIO_UNIT, it1) }
            radioWork?.isChecked?.let { it1 -> it.putBoolean(BUNDLE_RADIO_WORK, it1) }
            radioHomework?.isChecked?.let { it1 -> it.putBoolean(BUNDLE_RADIO_HOMEWORK, it1) }
            it.putString(BUNDLE_EDIT_TEXT_PERCENTAGE, editTextPercentage?.text.toString())
        }
    }
}
