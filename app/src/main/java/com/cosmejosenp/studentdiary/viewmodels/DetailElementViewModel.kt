/*
 * MIT License
 *
 * Copyright (c) 2022 Cosme José Nieto Pérez
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.cosmejosenp.studentdiary.viewmodels

import android.app.Application
import androidx.core.content.ContextCompat
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.cosmejosenp.studentdiary.R
import com.cosmejosenp.studentdiary.data.dao.ElementDao
import com.cosmejosenp.studentdiary.data.dao.SubjectDao
import com.cosmejosenp.studentdiary.util.TYPE_EXAM
import com.cosmejosenp.studentdiary.util.TYPE_HOMEWORK
import com.cosmejosenp.studentdiary.util.TYPE_WORK
import kotlinx.coroutines.SupervisorJob
import java.util.*

class DetailElementViewModel(
    subjectDao: SubjectDao,
    elementDao: ElementDao,
    idElement: Long,
    application: Application
) : AndroidViewModel(application) {
    private val resources = application.resources
    private val context = application.applicationContext
    private var viewModelJob = SupervisorJob()
    val subject = subjectDao.getFromElement(idElement)
    val element = elementDao.get(idElement)
    val delayed = Transformations.map(element) {
        if (it.deliveryDate != null
            && it.deliveryDate!! < Calendar.getInstance() && it.type != TYPE_EXAM
            || it.deliveryDate != null && it.completedDate != null
            && it.completedDate!! < it.deliveryDate!! && it.type != TYPE_EXAM
        )
            ContextCompat.getColor(
                context,
                R.color.delayedTextColour
            )
        else
            ContextCompat.getColor(
                context,
                R.color.primaryTextColor
            )
    }
    val title = Transformations.map(element) {
        when (it.type) {
            TYPE_EXAM -> resources.getString(R.string.examName)
            TYPE_WORK -> resources.getString(R.string.workName)
            TYPE_HOMEWORK -> resources.getString(R.string.homeworkName)
            else -> resources.getString(R.string.otherName)
        }
    }
    private val _navigateToEditFragment = MutableLiveData(false)
    val navigateToEditFragment: LiveData<Boolean>
        get() = _navigateToEditFragment
    private val _navigateToEditGrade = MutableLiveData(false)
    val navigateToEditGrade: LiveData<Boolean>
        get() = _navigateToEditGrade

    /**
     * This method will be called when this ViewModel is no longer used and will be destroyed.
     */
    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }

    /**
     * Start to navigate to [com.cosmejosenp.studentdiary.ui.AddElementFragment].
     *
     */
    fun startNavigateToEditFragment() {
        _navigateToEditFragment.value = true
    }

    /**
     * Finish notification of navigation for prevent unwanted navigation.
     *
     */
    fun doneNavigatingToEditFragment() {
        _navigateToEditFragment.value = false
    }

    /**
     * Start to navigate to editGradeFragment when they will be implemented..
     *
     */
    fun startNavigateToEditGrade() {
        _navigateToEditGrade.value = true
    }

    /**
     * Finish notification of navigation for prevent unwanted navigation.
     *
     */
    fun doneNavigatingToEditGrade() {
        _navigateToEditGrade.value = false
    }
}