/*
 * MIT License
 *
 * Copyright (c) 2022 Cosme José Nieto Pérez
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.cosmejosenp.studentdiary.viewmodels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.cosmejosenp.studentdiary.R
import com.cosmejosenp.studentdiary.data.dao.EvaluationDao
import com.cosmejosenp.studentdiary.data.dao.SubSubjectDao
import com.cosmejosenp.studentdiary.data.dao.SubjectDao
import com.cosmejosenp.studentdiary.data.entity.Evaluation
import com.cosmejosenp.studentdiary.data.entity.SubSubject
import kotlinx.coroutines.*

class DetailEvaluationViewModel(
    private val evaluationDao: EvaluationDao,
    subjectDao: SubjectDao,
    private val subSubjectDao: SubSubjectDao,
    private val idEvaluation: Long,
    var activeSchoolYear: Boolean,
    application: Application
) : AndroidViewModel(application) {
    private var viewModelJob = SupervisorJob()
    private val uiScope = CoroutineScope(Dispatchers.Main + viewModelJob)
    private val resources = application.resources
    private val evaluation = evaluationDao.get(idEvaluation)
    private val listAvailableSubjects =
        subjectDao.getAllPendingFromEvaluation(idEvaluation)
    val toolbarTitle = Transformations.map(evaluation) {
        it.name
    }
    private val _navigateToAddSubSubjectFromEvaluation = MutableLiveData<Evaluation?>()
    val navigateToAddSubSubjectFromEvaluation: LiveData<Evaluation?>
        get() = _navigateToAddSubSubjectFromEvaluation
    private val _navigateToDetailSubSubject = MutableLiveData<SubSubject?>()
    val navigateToDetailSubSubject: LiveData<SubSubject?>
        get() = _navigateToDetailSubSubject
    private val _showEditDialog = MutableLiveData(false)
    val showEditDialog: LiveData<Boolean>
        get() = _showEditDialog
    private val _showSnackBarMessage = MutableLiveData<String?>()
    val showSnackBarMessage: LiveData<String?>
        get() = _showSnackBarMessage
    val activeEvaluation = Transformations.map(evaluation) {
        it.finishDate == null
    }
    val activeLink = Transformations.map(listAvailableSubjects) {
        it.isNotEmpty()
    }
    private val _hideButton = MutableLiveData<Boolean>()
    val hideButton: LiveData<Boolean>
        get() = _hideButton
    private val _showDialogUpdate = MutableLiveData<Boolean>()
    val showDialogUpdate: LiveData<Boolean>
        get() = _showDialogUpdate

    /**
     * This method will be called when this ViewModel is no longer used and will be destroyed.
     */
    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }

    /**
     * Calculate the action of the fab button.
     *
     */
    fun onAdd() {
        startNavigateToAddSubSubjectFromEvaluation()
    }

    /**
     * Start to navigate to [com.cosmejosenp.studentdiary.ui.AddSubjectFragment].
     *
     */
    private fun startNavigateToAddSubSubjectFromEvaluation() {
        _navigateToAddSubSubjectFromEvaluation.value = evaluation.value
    }

    /**
     * Finish notification of navigation for prevent unwanted navigation.
     *
     */
    fun doneNavigatingToAddSubSubjectFromEvaluation() {
        _navigateToAddSubSubjectFromEvaluation.value = null
    }

    /**
     * Start to navigate to [com.cosmejosenp.studentdiary.ui.DetailSubSubjectFragment].
     *
     * @param idSubject
     */
    fun startNavigateToDetailSubSubject(idSubject: Long) {
        uiScope.launch {
            val subSubject = withContext(Dispatchers.IO) {
                return@withContext subSubjectDao.get(idEvaluation, idSubject)
            }
            _navigateToDetailSubSubject.value = subSubject
        }
    }

    /**
     * Finish notification of navigation for prevent unwanted navigation.
     *
     */
    fun doneNavigatingToDetailSubSubject() {
        _navigateToDetailSubSubject.value = null
    }

    /**
     * Start show the edit dialog.
     *
     */
    fun startShowEditDialog() {
        _showEditDialog.value = true
    }

    /**
     * Finish notification of showing dialog for prevent unwanted behaviour.
     *
     */
    fun doneShowEditDialog() {
        _showEditDialog.value = false
    }

    /**
     * Start show message on snackBar.
     *
     * @param message
     */
    fun startSnackBarMessage(message: String) {
        _showSnackBarMessage.value = message
    }

    /**
     * Finish notification of showing snackBar for prevent unwanted messages.
     *
     */
    fun showSnackBarDone() {
        _showSnackBarMessage.value = null
    }

    /**
     * Set the hide state of the fab button making necessary checks.
     *
     * @param hide status of fab button.
     */
    fun hideFabButton(hide: Boolean) {
        if (activeEvaluation.value != null && activeEvaluation.value!!
            && activeLink.value != null && activeLink.value!! || hide
        )
            _hideButton.value = hide
    }

    /**
     * Start show the update dialog.
     *
     */
    fun startDialogUpdate() {
        _showDialogUpdate.value = true
    }

    /**
     * Finish notification of showing dialog for prevent unwanted behaviour.
     *
     */
    fun dialogUpdateDone() {
        _showDialogUpdate.value = false
    }

    /**
     * Update the state of this evaluation.
     *
     */
    fun update() {
        if (activeEvaluation.value!!)
            markAsFinished()
        else
            markAsIncomplete()
    }

    /**
     * Mark this evaluation as finished.
     *
     */
    private fun markAsFinished() {
        uiScope.launch {
            val evaluation = evaluation.value!!
            try {
                activeSchoolYear = withContext(Dispatchers.IO) {
                    evaluationDao.markAsFinished(evaluation)
                    evaluationDao.isSchoolYearActiveOnly(evaluation.schoolYear)
                }
            } catch (exception: Exception) {
                _showSnackBarMessage.value = resources.getString(R.string.errorUpdate)
            }
        }
    }

    /**
     * Mark this evaluation as active.
     *
     */
    private fun markAsIncomplete() {
        uiScope.launch {
            val evaluation = evaluation.value!!
            try {
                activeSchoolYear = withContext(Dispatchers.IO) {
                    evaluationDao.markAsActive(evaluation)
                    evaluationDao.isSchoolYearActiveOnly(evaluation.schoolYear)
                }
            } catch (exception: Exception) {
                _showSnackBarMessage.value = resources.getString(R.string.errorUpdate)
            }
        }
    }
}