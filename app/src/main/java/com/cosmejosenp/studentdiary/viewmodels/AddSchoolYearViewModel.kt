/*
 * MIT License
 *
 * Copyright (c) 2022 Cosme José Nieto Pérez
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.cosmejosenp.studentdiary.viewmodels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.cosmejosenp.studentdiary.R
import com.cosmejosenp.studentdiary.data.dao.SchoolYearDao
import com.cosmejosenp.studentdiary.data.entity.Evaluation
import com.cosmejosenp.studentdiary.data.entity.SchoolYear
import kotlinx.coroutines.*
import java.util.*
import kotlin.collections.ArrayList

class AddSchoolYearViewModel(
    private val schoolYearDao: SchoolYearDao,
    application: Application
) : AndroidViewModel(application) {
    private val resources = application.resources
    private var viewModelJob = SupervisorJob()
    private val uiScope = CoroutineScope(Dispatchers.Main + viewModelJob)
    private val _validData = MutableLiveData(false)
    val validData: LiveData<Boolean>
        get() = _validData
    private val _navigateToDetailSchoolYear = MutableLiveData<Long>()
    val navigateToDetailSchoolYear: LiveData<Long>
        get() = _navigateToDetailSchoolYear
    private val _showSnackBarMessage = MutableLiveData<String?>()
    val showSnackBarMessage: LiveData<String?>
        get() = _showSnackBarMessage
    private val _showDialog = MutableLiveData(false)
    val showDialog: LiveData<Boolean>
        get() = _showDialog
    private val _navigateToBackStack = MutableLiveData(false)
    val navigateToBackStack: LiveData<Boolean>
        get() = _navigateToBackStack

    /**
     * This method will be called when this ViewModel is no longer used and will be destroyed.
     */
    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }

    /**
     * Evaluate if the data are different and if this are valid.
     *
     * @param title
     */
    fun evaluateData(title: String) {
        _validData.value = title.trim().isNotEmpty()
    }

    /**
     * Finish notification of navigation for prevent unwanted navigation.
     *
     */
    fun doneNavigatingToDetailSchoolYear() {
        _navigateToDetailSchoolYear.value = 0L
    }

    /**
     * Start to navigate to back stack.
     *
     */
    fun startNavigateToBackStack() {
        _navigateToBackStack.value = true
    }

    /**
     * Finish notification of navigation for prevent unwanted navigation.
     *
     */
    fun doneNavigatingToBackStack() {
        _navigateToBackStack.value = false
    }

    /**
     * Start show the confirm dialog.
     *
     */
    fun startShowDialog() {
        _showDialog.value = true
    }

    /**
     * Finish notification of showing dialog for prevent unwanted behaviour.
     *
     */
    fun showDialogDone() {
        _showDialog.value = false
    }

    /**
     * Finish notification of showing snackBar for prevent unwanted messages.
     *
     */
    fun showSnackBarMessageDone() {
        _showSnackBarMessage.value = null
    }

    /**
     * Save the new school year.
     *
     * @param name of the schoolYear.
     * @param numberEvaluations
     * @param createExtraEvaluation true to create an additional extraordinary evaluation.
     */
    fun saveSchoolYear(name: String, numberEvaluations: Int, createExtraEvaluation: Boolean) {
        uiScope.launch {
            try {
                _navigateToDetailSchoolYear.value = withContext(Dispatchers.IO) {
                    val schoolYear = SchoolYear(name.trim())
                    val evaluations = ArrayList<Evaluation>()
                    for (i in 1..numberEvaluations) {
                        val evaluation = Evaluation(
                            0,
                            resources.getString(R.string.nameOfEvaluation, i),
                            i
                        )
                        if (i == 1) {
                            evaluation.startDate = Calendar.getInstance()
                        }
                        evaluations.add(evaluation)
                    }
                    if (createExtraEvaluation) {
                        val evaluation = Evaluation(
                            0,
                            resources.getString(R.string.nameOfExtraordinaryEvaluation),
                            evaluations.size + 1,
                            extraOrdinary = true
                        )
                        evaluations.add(evaluation)
                    }
                    return@withContext schoolYearDao.addWithEvaluations(schoolYear, evaluations)
                }
            } catch (exception: Exception) {
                _showSnackBarMessage.value = resources.getString(R.string.errorUpdate)
            }
        }
    }
}
