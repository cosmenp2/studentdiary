/*
 * MIT License
 *
 * Copyright (c) 2022 Cosme José Nieto Pérez
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.cosmejosenp.studentdiary.viewmodels.dialogs

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.cosmejosenp.studentdiary.R
import com.cosmejosenp.studentdiary.data.dao.BlockDao
import com.cosmejosenp.studentdiary.data.dao.SubBlockDao
import com.cosmejosenp.studentdiary.data.entity.Block
import com.cosmejosenp.studentdiary.data.entity.SubBlock
import kotlinx.coroutines.*

class EditBlockViewModel(
    private val blockDao: BlockDao,
    private val subBlockDao: SubBlockDao,
    private val subSubject: Long,
    private val idBlock: Long,
    private val idSubBlock: Long,
    application: Application
) : AndroidViewModel(application) {
    private val resources = application.resources
    private var viewModelJob = SupervisorJob()
    private val uiScope = CoroutineScope(Dispatchers.Main + viewModelJob)
    private var cacheSum: Int? = null
    private var _firstSetting = true
    val firstSetting: Boolean
        get() = _firstSetting
    val block =
        if (subSubject != 0L) blockDao.get(idBlock)
        else MutableLiveData<Block?>(null)
    val subBlock =
        if (idSubBlock != 0L) subBlockDao.get(idSubBlock)
        else MutableLiveData<SubBlock>(null)
    private val _name = MutableLiveData<String?>(null)
    val name: LiveData<String?>
        get() = _name
    private val _percentage = MutableLiveData<Int?>(null)
    val percentage: LiveData<Int?>
        get() = _percentage
    private val _validData = MutableLiveData(false)
    val validData: LiveData<Boolean>
        get() = _validData
    private val _showSnackBarMessage = MutableLiveData<String?>()
    val showSnackBarMessage: LiveData<String?>
        get() = _showSnackBarMessage
    private val _clearData = MutableLiveData(false)
    val clearData: LiveData<Boolean>
        get() = _clearData
    private val _showDialog = MutableLiveData(false)
    val showDialog: LiveData<Boolean>
        get() = _showDialog
    private val _navigateToBackStack = MutableLiveData(false)
    val navigateToBackStack: LiveData<Boolean>
        get() = _navigateToBackStack
    private val _showErrorPercentages = MutableLiveData(false)
    val showErrorPercentages: LiveData<Boolean>
        get() = _showErrorPercentages
    val messageTextView = MutableLiveData(resources.getString(R.string.percentageSuggestion))

    /**
     * This method will be called when this ViewModel is no longer used and will be destroyed.
     */
    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }

    /**
     * Evaluate if the data are different and if this are valid.
     *
     * @param title
     * @param percentage
     */
    fun evaluateData(
        title: String,
        percentage: String
    ) {
        val titleValid = title.trim().isNotEmpty() && when {
            block.value != null -> title.trim() != block.value!!.name
            else -> title.trim() != subBlock.value!!.name
        }
        val numberValid: Boolean
        if (percentage.isNotEmpty()) {
            val percentageInRange = Integer.valueOf(percentage) in 0..100
            numberValid =
                when {
                    block.value != null ->
                        if (block.value!!.percent == null
                            || Integer.valueOf(percentage) != block.value!!.percent
                        )
                            percentageInRange
                        else
                            false
                    else ->
                        if (subBlock.value!!.percent == null
                            || Integer.valueOf(percentage) != subBlock.value!!.percent
                        )
                            percentageInRange
                        else
                            false
                }
            calculatePercentage(percentage)
            _showErrorPercentages.value = !percentageInRange
        } else {
            numberValid = when {
                block.value != null -> block.value!!.percent != null
                else -> subBlock.value!!.percent != null
            }
            messageTextView.value = resources.getString(R.string.percentageSuggestion)
        }
        _validData.value = titleValid || numberValid
    }

    /**
     * Provide a helper to calculate the future total percentage.
     *
     * @param percentage value.
     */
    private fun calculatePercentage(percentage: String) {
        uiScope.launch {
            if (cacheSum == null)
                if (subSubject != 0L) {
                    val blocks = withContext(Dispatchers.IO) {
                        blockDao.getAllFromSubSubjectList(subSubject)
                    }
                    cacheSum = 0
                    for (block in blocks) {
                        if (block.percent != null && block.id != idBlock)
                            cacheSum = cacheSum!! + block.percent!!
                    }
                } else {
                    val subBlocks = withContext(Dispatchers.IO) {
                        subBlockDao.getAllFromBlockList(idBlock)
                    }
                    cacheSum = 0
                    for (subBlock in subBlocks) {
                        if (subBlock.percent != null && subBlock.id != idSubBlock)
                            cacheSum = cacheSum!! + subBlock.percent!!
                    }
                }
            if (cacheSum != null)
                messageTextView.value = resources.getString(
                    R.string.percentageAdd,
                    cacheSum!! + Integer.valueOf(percentage)
                )
        }
    }

    /**
     * Start to navigate to back stack.
     *
     */
    fun startNavigateToBackStack() {
        _clearData.value = true
        _navigateToBackStack.value = true
    }

    /**
     * Finish notification of navigation for prevent unwanted navigation.
     *
     */
    fun doneNavigatingToBackStack() {
        _navigateToBackStack.value = false
    }

    /**
     * Start show the confirm dialog.
     *
     */
    fun startShowDialog() {
        _showDialog.value = true
    }

    /**
     * Finish notification of showing dialog for prevent unwanted behaviour.
     *
     */
    fun showDialogDone() {
        _showDialog.value = false
    }

    /**
     * Finish notification of showing snackBar for prevent unwanted messages.
     *
     */
    fun showSnackBarMessageDone() {
        _showSnackBarMessage.value = null
    }

    /**
     * Finish notification of clear data for prevent unwanted behaviour.
     *
     */
    fun clearDataDone() {
        _clearData.value = false
    }

    /**
     * For first load. Load the name and percent of the block or subBlock.
     *
     */
    fun updateNameAndPercentage() {
        if (block.value != null) {
            _name.value = block.value!!.name
            _percentage.value = block.value!!.percent
        } else if (subBlock.value != null) {
            _name.value = subBlock.value!!.name
            _percentage.value = subBlock.value!!.percent
        }
    }

    /**
     * Finish first setting for prevent unwanted behaviour.
     *
     */
    fun doneFirstSetting() {
        _firstSetting = false
    }

    /**
     * Edit and save changes of block or subBlocks
     * depending of the parameters received in the constructor.
     *
     * @param newName
     * @param percentageString
     */
    fun edit(newName: String, percentageString: String) {
        uiScope.launch {
            val percentage =
                if (percentageString.trim().isNotEmpty())
                    Integer.valueOf(percentageString)
                else null
            if (subSubject != 0L)
                saveBlock(newName, percentage)
            else if (idSubBlock != 0L)
                saveSubBlock(newName, percentage)
        }
    }

    /**
     * Save block changes
     *
     * @param newName
     * @param percentage
     */
    private suspend fun saveBlock(newName: String, percentage: Int?) {
        val block = block.value!!
        block.name = newName.trim()
        block.percent = percentage
        try {
            withContext(Dispatchers.IO) {
                blockDao.update(block)
            }
            startNavigateToBackStack()
        } catch (exception: Exception) {
            _showSnackBarMessage.value = resources.getString(R.string.errorUpdate)
        }
    }

    /**
     * Save subBlock changes
     *
     * @param newName
     * @param percentage
     */
    private suspend fun saveSubBlock(newName: String, percentage: Int?) {
        val subBlock = subBlock.value!!
        subBlock.name = newName.trim()
        subBlock.percent = percentage
        try {
            withContext(Dispatchers.IO) {
                subBlockDao.update(subBlock)
            }
            startNavigateToBackStack()
        } catch (exception: Exception) {
            _showSnackBarMessage.value = resources.getString(R.string.errorUpdate)
        }
    }
}
