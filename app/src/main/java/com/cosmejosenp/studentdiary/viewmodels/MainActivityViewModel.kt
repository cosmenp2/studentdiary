/*
 * MIT License
 *
 * Copyright (c) 2022 Cosme José Nieto Pérez
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.cosmejosenp.studentdiary.viewmodels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.cosmejosenp.studentdiary.data.dao.SchoolYearDao

/**
 * ViewModel for ListActiveSchoolYearFragment
 */
class MainActivityViewModel(
    schoolYearDao: SchoolYearDao,
    application: Application
) : AndroidViewModel(application) {
    val schoolYearAndSubjects = schoolYearDao.getActiveWithSubjects()
    private val _navigateToDetailSubject = MutableLiveData<Array<Long>?>(null)
    val navigateToDetailSubject: LiveData<Array<Long>?>
        get() = _navigateToDetailSubject
    private var _lastMenuId = 0L
    private val lastMenuId: Long
        get() = _lastMenuId
    private var _firstCheck = true
    val firstCheck: Boolean
        get() = _firstCheck

    /**
     * Start to navigate to [com.cosmejosenp.studentdiary.ui.DetailSubjectFragment].
     *
     * @param id
     */
    fun startNavigateToDetailSubject(id: Long) {
        if (lastMenuId != id) {
            _lastMenuId = id
            _navigateToDetailSubject.value = arrayOf(id, searchCodSchoolYear(id))
        }
    }

    /**
     * Search the school year id of the [idSubject].
     *
     * @param idSubject
     * @return id of school year.
     */
    private fun searchCodSchoolYear(idSubject: Long): Long {
        val schoolYearsWithSubjects = schoolYearAndSubjects.value!!
        var idSchoolYear = 0L
        var found = false
        var i = 0
        while (i < schoolYearsWithSubjects.size && !found) {
            var j = 0
            while (j < schoolYearsWithSubjects[i].subjects.size && !found) {
                if (schoolYearsWithSubjects[i].subjects[j].id == idSubject) {
                    found = true
                    idSchoolYear = schoolYearsWithSubjects[i].schoolYear.id
                }
                j++
            }
            i++
        }
        return idSchoolYear
    }

    /**
     * Finish notification of navigation for prevent unwanted navigation.
     *
     */
    fun doneNavigateToDetailSubject() {
        _navigateToDetailSubject.value = null
    }

    /**
     * Finish notification of first check of nav drawer for prevent unwanted behaviour.
     *
     */
    fun doneFirstCheck() {
        _firstCheck = false
    }

    /**
     * When the user navigate to other part that is not [com.cosmejosenp.studentdiary.ui.DetailSubjectFragment].
     *
     */
    fun invalidateLastMenuItem() {
        _lastMenuId = 0
    }

    /**
     * Set the last menu item checked if this menu is for go to [com.cosmejosenp.studentdiary.ui.DetailSubjectFragment].
     *
     * @param id
     */
    fun setLastMenuItem(id: Long) {
        _lastMenuId = id
    }
}