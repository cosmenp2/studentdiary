/*
 * MIT License
 *
 * Copyright (c) 2022 Cosme José Nieto Pérez
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.cosmejosenp.studentdiary.viewmodels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.cosmejosenp.studentdiary.R
import com.cosmejosenp.studentdiary.data.dao.SchoolYearDao
import kotlinx.coroutines.*

/**
 * ViewModel for ListActiveSchoolYearFragment
 */
class DetailSchoolYearViewModel(
    private val dataSource: SchoolYearDao,
    private val idSchoolYear: Long,
    application: Application
) : AndroidViewModel(application) {
    private var viewModelJob = SupervisorJob()
    private val uiScope = CoroutineScope(Dispatchers.Main + viewModelJob)
    private val resources = application.resources
    private val schoolYear = dataSource.get(idSchoolYear)
    val active = Transformations.map(schoolYear) {
        it.active
    }
    val toolbarTitle = Transformations.map(schoolYear) {
        it.name
    }
    private val _navigateToAddSubject = MutableLiveData<Long>()
    val navigateToAddSubject: LiveData<Long>
        get() = _navigateToAddSubject
    private val _navigateToDetailSubject = MutableLiveData<Long>()
    val navigateToDetailSubject: LiveData<Long>
        get() = _navigateToDetailSubject
    private val _navigateToAddEvaluation = MutableLiveData<Long>()
    val navigateToAddEvaluation: LiveData<Long>
        get() = _navigateToAddEvaluation
    private val _navigateToDetailEvaluation = MutableLiveData<Long>()
    val navigateToDetailEvaluation: LiveData<Long>
        get() = _navigateToDetailEvaluation
    private val _showEditDialog = MutableLiveData(false)
    val showEditDialog: LiveData<Boolean>
        get() = _showEditDialog
    private val _showSnackBarMessage = MutableLiveData<String?>()
    val showSnackBarMessage: LiveData<String?>
        get() = _showSnackBarMessage
    private var _oldPosition = 0
    val oldPosition: Int
        get() = _oldPosition
    private val _hideButton = MutableLiveData<Boolean>()
    val hideButton: LiveData<Boolean>
        get() = _hideButton
    private val _showDialogUpdate = MutableLiveData<Boolean>()
    val showDialogUpdate: LiveData<Boolean>
        get() = _showDialogUpdate

    /**
     * This method will be called when this ViewModel is no longer used and will be destroyed.
     */
    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }

    /**
     * Calculate the action of the fab button depending of the actual page.
     *
     */
    fun onAdd() {
        when (oldPosition) {
            0 -> startNavigateToAddSubject()
            1 -> startNavigateToAddEvaluation()
        }
    }

    /**
     * Start to navigate to [com.cosmejosenp.studentdiary.ui.AddSubjectFragment].
     *
     */
    private fun startNavigateToAddSubject() {
        _navigateToAddSubject.value = idSchoolYear
    }

    /**
     * Finish notification of navigation for prevent unwanted navigation.
     *
     */
    fun doneNavigatingToAddSubject() {
        _navigateToAddSubject.value = 0L
    }

    /**
     * Start to navigate to [com.cosmejosenp.studentdiary.ui.DetailSubjectFragment].
     *
     * @param idSubject
     */
    fun startNavigateToDetailSubject(idSubject: Long) {
        _navigateToDetailSubject.value = idSubject
    }

    /**
     * Finish notification of navigation for prevent unwanted navigation.
     *
     */
    fun doneNavigatingToDetailSubject() {
        _navigateToDetailSubject.value = 0L
    }

    /**
     * Start to navigate to [com.cosmejosenp.studentdiary.ui.AddEvaluationFragment].
     *
     */
    private fun startNavigateToAddEvaluation() {
        _navigateToAddEvaluation.value = idSchoolYear
    }

    /**
     * Finish notification of navigation for prevent unwanted navigation.
     *
     */
    fun doneNavigatingToAddEvaluation() {
        _navigateToAddEvaluation.value = 0L
    }

    /**
     * Start to navigate to [com.cosmejosenp.studentdiary.ui.DetailEvaluationFragment].
     *
     * @param idEvaluation
     */
    fun startNavigateToDetailEvaluation(idEvaluation: Long) {
        _navigateToDetailEvaluation.value = idEvaluation
    }

    /**
     * Finish notification of navigation for prevent unwanted navigation.
     *
     */
    fun doneNavigatingToDetailEvaluation() {
        _navigateToDetailEvaluation.value = 0L
    }

    /**
     * Start show the edit dialog.
     *
     */
    fun startShowEditDialog() {
        _showEditDialog.value = true
    }

    /**
     * Finish notification of showing dialog for prevent unwanted behaviour.
     *
     */
    fun doneShowEditDialog() {
        _showEditDialog.value = false
    }

    /**
     * Start show message on snackBar.
     *
     * @param message
     */
    fun startSnackBarMessage(message: String) {
        _showSnackBarMessage.value = message
    }

    /**
     * Finish notification of showing snackBar for prevent unwanted messages.
     *
     */
    fun showSnackBarDone() {
        _showSnackBarMessage.value = null
    }

    /**
     * Update the actual page position.
     *
     * @param position of the viewPager.
     */
    fun setLastPosition(position: Int) {
        _oldPosition = position
    }

    /**
     * Set the hide state of the fab button making necessary checks.
     *
     * @param hide status of fab button.
     */
    fun hideFabButton(hide: Boolean) {
        if (active.value!! || hide)
            _hideButton.value = hide
    }

    /**
     * Start show the update dialog.
     *
     */
    fun startDialogUpdate() {
        _showDialogUpdate.value = true
    }

    /**
     * Finish notification of showing dialog for prevent unwanted behaviour.
     *
     */
    fun dialogUpdateDone() {
        _showDialogUpdate.value = false
    }

    /**
     * Update the state of this schoolYear.
     *
     */
    fun update() {
        if (active.value!!)
            markAsFinished()
        else
            markAsIncomplete()
    }

    /**
     * Mark this schoolYear as finished.
     *
     */
    private fun markAsFinished() {
        uiScope.launch {
            val schoolYear = schoolYear.value!!
            try {
                withContext(Dispatchers.IO) {
                    dataSource.finishSchoolYear(schoolYear)
                }
            } catch (exception: Exception) {
                _showSnackBarMessage.value = resources.getString(R.string.errorUpdate)
            }
        }
    }

    /**
     * Mark this schoolYear as active.
     *
     */
    private fun markAsIncomplete() {
        uiScope.launch {
            val schoolYear = schoolYear.value!!
            try {
                withContext(Dispatchers.IO) {
                    dataSource.markAsActiveSchoolYear(schoolYear)
                }
            } catch (exception: Exception) {
                _showSnackBarMessage.value = resources.getString(R.string.errorUpdate)
            }
        }
    }
}