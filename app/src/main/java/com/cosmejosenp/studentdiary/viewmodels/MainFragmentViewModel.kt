/*
 * MIT License
 *
 * Copyright (c) 2022 Cosme José Nieto Pérez
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.cosmejosenp.studentdiary.viewmodels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.cosmejosenp.studentdiary.data.dao.SubjectDao

class MainFragmentViewModel(
    private val canAdd: Boolean,
    dataSource: SubjectDao,
    application: Application
) : AndroidViewModel(application) {
    private var _canExecuteFunction = false
    val cadAddElements = dataSource.getAnySubjectCanAddElements()
    private val _navigateToAddElement = MutableLiveData(false)
    val navigateToAddElement: LiveData<Boolean>
        get() = _navigateToAddElement
    private val _navigateToDetailElement = MutableLiveData<Long>()
    val navigateToDetailElement: LiveData<Long>
        get() = _navigateToDetailElement
    private val _showSnackBarMessage = MutableLiveData<String?>()
    val showSnackBarMessage: LiveData<String?>
        get() = _showSnackBarMessage
    private val _hideButton = MutableLiveData<Boolean>()
    val hideButton: LiveData<Boolean>
        get() = _hideButton
    private var _function: (() -> Unit)? = null
    val function
        get() = _function

    /**
     * Calculate the action of the fab button depending of the actual page.
     *
     */
    fun onAdd() {
        startNavigateToAddElement()
    }

    /**
     * Start to navigate to [com.cosmejosenp.studentdiary.ui.AddElementFragment].
     *
     */
    private fun startNavigateToAddElement() {
        _navigateToAddElement.value = true
    }

    /**
     * Finish notification of navigation for prevent unwanted navigation.
     *
     */
    fun doneNavigatingToAddElement() {
        _navigateToAddElement.value = false
    }

    /**
     * Start to navigate to [com.cosmejosenp.studentdiary.ui.DetailElementFragment].
     *
     * @param idElement
     */
    fun startNavigateToDetailElement(idElement: Long) {
        _navigateToDetailElement.value = idElement
    }

    /**
     * Finish notification of navigation for prevent unwanted navigation.
     *
     */
    fun doneNavigatingToDetailElement() {
        _navigateToDetailElement.value = 0L
    }

    /**
     * Start show message on snackBar and check the if the undo action should be shown.
     *
     * @param message
     */
    fun startSnackBarMessage(message: String) {
        if (_canExecuteFunction) {
            _canExecuteFunction = false
        } else {
            _function = null
        }
        _showSnackBarMessage.value = message
    }

    /**
     * Update the undo function to execute when the snackBar message is shown.
     *
     * @param function
     */
    fun updateFunction(function: (() -> Unit)?) {
        _function = function
        _canExecuteFunction = true
    }

    /**
     * Finish notification of showing snackBar for prevent unwanted messages.
     *
     */
    fun showSnackBarDone() {
        _showSnackBarMessage.value = null
    }

    /**
     * Set the hide state of the fab button making necessary checks.
     *
     * @param hide status of fab button.
     */
    fun hideFabButton(hide: Boolean) {
        if (canAdd && cadAddElements.value != null && cadAddElements.value!! || hide)
            _hideButton.value = hide
    }
}