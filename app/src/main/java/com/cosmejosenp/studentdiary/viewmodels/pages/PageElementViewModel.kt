/*
 * MIT License
 *
 * Copyright (c) 2022 Cosme José Nieto Pérez
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.cosmejosenp.studentdiary.viewmodels.pages

import android.app.Application
import android.view.View
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.cosmejosenp.studentdiary.R
import com.cosmejosenp.studentdiary.data.dao.ElementDao
import com.cosmejosenp.studentdiary.data.entity.Element
import com.cosmejosenp.studentdiary.util.ListElementsTypes
import com.cosmejosenp.studentdiary.util.PageListener
import com.cosmejosenp.studentdiary.util.TYPE_EXAM
import kotlinx.coroutines.*
import java.util.*
import kotlin.collections.ArrayList

class PageElementViewModel(
    private val elementDao: ElementDao,
    private val typeList: ListElementsTypes,
    subject: Long,
    subSubject: Long,
    block: Long,
    subBlock: Long,
    application: Application
) : AndroidViewModel(application) {
    private var viewModelJob = SupervisorJob()
    private val uiScope = CoroutineScope(Dispatchers.Main + viewModelJob)
    private val resources = application.resources
    val elements = elementDao.getAll(typeList, subject, subSubject, block, subBlock)
    val menuFinishElement: Boolean
        get() = when (typeList) {
            ListElementsTypes.COMPLETED, ListElementsTypes.PENDING_GRADES -> false
            ListElementsTypes.PENDING, ListElementsTypes.ACTIVE -> true
        }
    private val _navigateToDetailElement = MutableLiveData<Long>()
    val navigateToDetailElement: LiveData<Long>
        get() = _navigateToDetailElement
    private val _showSnackBarMessage = MutableLiveData<String?>()
    val showSnackBarMessage: LiveData<String?>
        get() = _showSnackBarMessage
    private val _actionModeActivated = MutableLiveData<Boolean>()
    val actionModeActivated: LiveData<Boolean>
        get() = _actionModeActivated
    private val _selectAllItems = MutableLiveData<Boolean>()
    val selectAllItems: LiveData<Boolean>
        get() = _selectAllItems
    private val _showDialogDelete = MutableLiveData<Boolean>()
    val showDialogDelete: LiveData<Boolean>
        get() = _showDialogDelete
    var selectedItems = ArrayList<Element>()
    private var selectedItemsOld = ArrayList<Element>()
    val emptyTextViewVisibility = Transformations.map(elements) {
        if (it.isEmpty()) {
            View.VISIBLE
        } else {
            View.GONE
        }
    }
    val messageEmptyList: String
        get() =
            when (typeList) {
                ListElementsTypes.ACTIVE -> resources.getString(R.string.emptyActiveElements)
                ListElementsTypes.PENDING -> resources.getString(R.string.emptyActiveDelayedElements)
                ListElementsTypes.PENDING_GRADES -> resources.getString(R.string.emptyPendingGradesElements)
                ListElementsTypes.COMPLETED -> resources.getString(R.string.emptyCompletedElements)
            }
    private val _changeTitleActionMode = MutableLiveData(false)
    val changeTitleActionMode: LiveData<Boolean>
        get() = _changeTitleActionMode
    private var _colour = 0
    val colour
        get() = _colour
    val titleActionMode: Int
        get() = selectedItems.size
    private var _listener: PageListener? = null
    val listener: PageListener?
        get() = _listener

    /**
     * This method will be called when this ViewModel is no longer used and will be destroyed.
     */
    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }

    /**
     * Start to navigate to [com.cosmejosenp.studentdiary.ui.DetailElementFragment].
     *
     * @param idElement
     */
    fun startNavigateToDetailElement(idElement: Long) {
        _navigateToDetailElement.value = idElement
    }

    /**
     * Finish notification of navigation for prevent unwanted navigation.
     *
     */
    fun doneNavigatingToDetailElement() {
        _navigateToDetailElement.value = 0L
    }

    /**
     * Activate the action mode.
     *
     */
    fun activateActionMode() {
        _actionModeActivated.value = true
    }

    /**
     * Change the selected state of the [element].
     *
     * @param element to select or deselect.
     */
    fun toggleItemSelected(element: Element) {
        if (selectedItems.contains(element)) {
            selectedItems.remove(element)
            if (selectedItems.isEmpty()) {
                _actionModeActivated.value = false
            }
        } else {
            selectedItems.add(element)
        }
        notifyChangeTitleActionMode()
    }

    /**
     * Change the selected state of all elements to selected.
     *
     */
    fun selectAll() {
        selectedItems.clear()
        for (element: Element in elements.value!!) {
            selectedItems.add(element)
        }
        _selectAllItems.value = true
        notifyChangeTitleActionMode()
    }

    /**
     * Finish notification of selection for prevent unwanted behaviour.
     *
     */
    fun selectAllDone() {
        _selectAllItems.value = false
    }

    /**
     * Notify to change the title of the action mode when a new item is selected.
     *
     */
    fun notifyChangeTitleActionMode() {
        _changeTitleActionMode.value = true
    }

    /**
     * Finish notification of change the title for prevent unwanted behaviour.
     *
     */
    fun changeTitleActionModeDone() {
        _changeTitleActionMode.value = false
    }

    /**
     * Start the update service for [TYPE_EXAM] elements.
     *
     */
    fun startUpdateService() {
        uiScope.launch {
            withContext(Dispatchers.IO) {
                elementDao.updateSpecialElements()
            }
        }
    }

    /**
     * Delete all selected blocks.
     *
     */
    fun delete() {
        val selectedItemsOld = ArrayList<Element>(selectedItems)
        _actionModeActivated.value = false
        uiScope.launch {
            try {
                withContext(Dispatchers.IO) {
                    elementDao.delete(*selectedItemsOld.toTypedArray())
                }
            } catch (exception: Exception) {
                _showSnackBarMessage.value = resources.getString(R.string.errorDelete)
            }
        }
    }

    /**
     * Update all selected elements to a finished or active state.
     *
     */
    fun update(element: Element? = null) {
        val swap = ArrayList<Element>()
        if (element == null)
            selectedItemsOld = ArrayList(selectedItems)
        else {
            selectedItemsOld = ArrayList()
            selectedItemsOld.add(element)
        }
        for (e: Element in selectedItemsOld)
            swap.add(e.copy())
        _actionModeActivated.value = false
        if (menuFinishElement)
            markAsFinished(swap)
        else
            marAsIncomplete(swap)
    }

    /**
     * Mark as finished all this [selectedItemsOld].
     *
     * @param selectedItemsOld
     */
    private fun markAsFinished(selectedItemsOld: ArrayList<Element>) {
        uiScope.launch {
            try {
                for (element in selectedItemsOld) {
                    element.completedDate = Calendar.getInstance()
                    element.completed = true
                }
                withContext(Dispatchers.IO) {
                    elementDao.update(*selectedItemsOld.toTypedArray())
                }
                _showSnackBarMessage.value = resources.getString(R.string.markedElementAsComplete)
            } catch (exception: Exception) {
                _showSnackBarMessage.value = resources.getString(R.string.errorDelete)
            }
        }
    }

    /**
     * Mark as active all this [selectedItemsOld].
     *
     * @param selectedItemsOld
     */
    private fun marAsIncomplete(selectedItemsOld: ArrayList<Element>) {
        uiScope.launch {
            try {
                for (element in selectedItemsOld) {
                    element.completedDate = null
                    element.completed = false
                    if (element.type == TYPE_EXAM) {
                        element.deliveryDate = null
                    }
                }
                withContext(Dispatchers.IO) {
                    elementDao.update(*selectedItemsOld.toTypedArray())
                }
                _showSnackBarMessage.value = resources.getString(R.string.markedElementAsIncomplete)
            } catch (exception: Exception) {
                _showSnackBarMessage.value = resources.getString(R.string.errorDelete)
            }
        }
    }

    /**
     * Undo last action updating the old elements to their original state.
     *
     */
    fun undoLastAction() {
        if (selectedItemsOld.isNotEmpty())
            uiScope.launch {
                try {
                    withContext(Dispatchers.IO) {
                        elementDao.update(*selectedItemsOld.toTypedArray())
                    }
                    selectedItemsOld.clear()
                } catch (exception: Exception) {
                    _showSnackBarMessage.value = resources.getString(R.string.errorDelete)
                }
            }
    }

    /**
     * Finish action mode and restore the default behaviour.
     *
     */
    fun actionModeDone() {
        selectedItems.clear()
        _actionModeActivated.value = false
    }

    /**
     * Finish notification of showing snackBar for prevent unwanted messages.
     *
     */
    fun showSnackBarDone() {
        _showSnackBarMessage.value = null
    }

    /**
     * Start show the confirm delete.
     *
     */
    fun startDialogDelete() {
        _showDialogDelete.value = true
    }

    /**
     * Finish notification of showing dialog for prevent unwanted behaviour.
     *
     */
    fun dialogDeleteDone() {
        _showDialogDelete.value = false
    }

    /**
     * Set the page listener if it's not set yet.
     *
     * @param newListener
     */
    fun setListener(newListener: PageListener) {
        if (_listener == null) {
            _listener = newListener
        }
    }

    /**
     * Set the status bar colour for restore colours when the action mode finish.
     *
     * @param statusBarColor
     */
    fun setColour(statusBarColor: Int) {
        _colour = statusBarColor
    }
}