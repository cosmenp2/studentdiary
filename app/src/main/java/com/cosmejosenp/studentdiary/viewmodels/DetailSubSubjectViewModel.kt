/*
 * MIT License
 *
 * Copyright (c) 2022 Cosme José Nieto Pérez
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.cosmejosenp.studentdiary.viewmodels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.cosmejosenp.studentdiary.R
import com.cosmejosenp.studentdiary.data.dao.EvaluationDao
import com.cosmejosenp.studentdiary.data.dao.SubSubjectDao
import com.cosmejosenp.studentdiary.data.dao.SubjectDao
import kotlinx.coroutines.*

class
DetailSubSubjectViewModel(
    subjectDao: SubjectDao,
    private val subSubjectDao: SubSubjectDao,
    private val evaluationDao: EvaluationDao,
    idSubject: Long,
    var activeSchoolYear: Boolean,
    private val idSubSubject: Long,
    application: Application
) : AndroidViewModel(application) {
    private val resources = application.resources
    private var viewModelJob = SupervisorJob()
    private val uiScope = CoroutineScope(Dispatchers.Main + viewModelJob)
    private var _canExecuteFunction = false
    val subject = subjectDao.get(idSubject)
    val existOtherEvaluations =
        subSubjectDao.existOtherSubSubjectAndSubSubjectIsValid(idSubject, idSubSubject)
    val existOtherValidStructureEvaluations =
        subSubjectDao.existOtherSubSubjectWithValidStructure(idSubject, idSubSubject)
    val canAddElements = subSubjectDao.subSubjectCanAddElements(idSubSubject)
    private val subSubject = subSubjectDao.get(idSubSubject)
    private val _navigateToAddBlock = MutableLiveData<Long>()
    val navigateToAddBlock: LiveData<Long>
        get() = _navigateToAddBlock
    private val _navigateToDetailBlock = MutableLiveData<Long>()
    val navigateToDetailBlock: LiveData<Long>
        get() = _navigateToDetailBlock
    private val _navigateToCopyTo = MutableLiveData<Long>()
    val navigateToCopyTo: LiveData<Long>
        get() = _navigateToCopyTo
    private val _navigateToCopyFrom = MutableLiveData<Long>()
    val navigateToCopyFrom: LiveData<Long>
        get() = _navigateToCopyFrom
    private val _navigateToDetailElement = MutableLiveData<Long>()
    val navigateToDetailElement: LiveData<Long>
        get() = _navigateToDetailElement
    private val _navigateToAddElement = MutableLiveData<Long>()
    val navigateToAddElement: LiveData<Long>
        get() = _navigateToAddElement
    private val _showSnackBarMessage = MutableLiveData<String?>()
    val showSnackBarMessage: LiveData<String?>
        get() = _showSnackBarMessage
    private var _function: (() -> Unit)? = null
    val function
        get() = _function
    private var _oldPosition = 0
    val oldPosition: Int
        get() = _oldPosition
    val active = Transformations.map(subSubject) {
        it.active
    }
    private val _notifyUpdateUI = MutableLiveData<Boolean>()
    val notifyUpdateUI: LiveData<Boolean>
        get() = _notifyUpdateUI
    private val _hideButton = MutableLiveData<Boolean>()
    val hideButton: LiveData<Boolean>
        get() = _hideButton

    /**
     * This method will be called when this ViewModel is no longer used and will be destroyed.
     */
    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }

    /**
     * Calculate the action of the fab button depending of the actual page.
     *
     */
    fun onAdd() {
        when (oldPosition) {
            0 -> startNavigateToAddElement()
            4 -> startNavigateToAddBlock()
        }
    }

    /**
     * Start to navigate to [com.cosmejosenp.studentdiary.ui.AddBlockFragment].
     *
     */
    private fun startNavigateToAddBlock() {
        _navigateToAddBlock.value = idSubSubject
    }

    /**
     * Finish notification of navigation for prevent unwanted navigation.
     *
     */
    fun doneNavigatingToAddBlock() {
        _navigateToAddBlock.value = 0L
    }

    /**
     * Start to navigate to [com.cosmejosenp.studentdiary.ui.DetailBlockFragment].
     *
     * @param idBlock
     */
    fun startNavigateToDetailBlock(idBlock: Long) {
        uiScope.launch {
            _navigateToDetailBlock.value = idBlock
        }
    }

    /**
     * Finish notification of navigation for prevent unwanted navigation.
     *
     */
    fun doneNavigatingToDetailBlock() {
        _navigateToDetailBlock.value = 0L
    }

    /**
     * Start to navigate to [com.cosmejosenp.studentdiary.ui.CopyToFragment].
     *
     */
    fun startNavigateToCopyTo() {
        _navigateToCopyTo.value = idSubSubject

    }

    /**
     * Finish notification of navigation for prevent unwanted navigation.
     *
     */
    fun doneNavigatingToCopyTo() {
        _navigateToCopyTo.value = 0L
    }

    /**
     * Start to navigate to [com.cosmejosenp.studentdiary.ui.CopyFromFragment].
     *
     */
    fun startNavigateToCopyFrom() {
        _navigateToCopyFrom.value = idSubSubject

    }

    /**
     * Finish notification of navigation for prevent unwanted navigation.
     *
     */
    fun doneNavigatingToCopyFrom() {
        _navigateToCopyFrom.value = 0L
    }

    /**
     * Start to navigate to [com.cosmejosenp.studentdiary.ui.AddElementFragment].
     *
     */
    private fun startNavigateToAddElement() {
        _navigateToAddElement.value = idSubSubject
    }

    /**
     * Finish notification of navigation for prevent unwanted navigation.
     *
     */
    fun doneNavigatingToAddElement() {
        _navigateToAddElement.value = 0L
    }

    /**
     * Start to navigate to [com.cosmejosenp.studentdiary.ui.DetailElementFragment].
     *
     * @param idElement
     */
    fun startNavigateToDetailElement(idElement: Long) {
        _navigateToDetailElement.value = idElement
    }

    /**
     * Finish notification of navigation for prevent unwanted navigation.
     *
     */
    fun doneNavigatingToDetailElement() {
        _navigateToDetailElement.value = 0L
    }

    /**
     * Finish notification of update ui for prevent unwanted behaviour.
     *
     */
    fun doneUpdateUI() {
        _notifyUpdateUI.value = false
    }

    /**
     * Start show message on snackBar and check the if the undo action should be shown.
     *
     * @param message
     */
    fun startSnackBarMessage(message: String) {
        if (_canExecuteFunction) {
            _canExecuteFunction = false
        } else {
            _function = null
        }
        _showSnackBarMessage.value = message
    }

    /**
     * Finish notification of showing snackBar for prevent unwanted messages.
     *
     */
    fun showSnackBarDone() {
        _showSnackBarMessage.value = null
    }

    /**
     * Update the actual page position.
     *
     * @param position of the viewPager.
     */
    fun setLastPosition(position: Int) {
        _oldPosition = position
    }

    /**
     * Set the hide state of the fab button making necessary checks.
     *
     * @param hide status of fab button.
     */
    fun hideFabButton(hide: Boolean) {
        if (activeSchoolYear && (addElementCondition() || addBlockCondition())
            || hide
        )
            _hideButton.value = hide
    }

    /**
     * Condition for show the fab button in the element page.
     *
     */
    private fun addElementCondition() =
        oldPosition == 0 && canAddElements.value != null && canAddElements.value!!


    /**
     * Condition for show the fab button in the block page.
     *
     */
    private fun addBlockCondition() =
        oldPosition == 4

    /**
     * Update the undo function to execute when the snackBar message is shown.
     *
     * @param function
     */
    fun updateFunction(function: (() -> Unit)?) {
        _function = function
        _canExecuteFunction = true
    }

    /**
     * Set to null [_function] to prevent repeated actions.
     *
     */
    fun doneUseFunction() {
        _function = null
    }

    /**
     * Update the status of this subSubject to finished or active.
     *
     */
    fun startUpdate() {
        val subSubject = subSubject.value!!
        val oldValue = activeSchoolYear
        uiScope.launch {
            try {
                if (subSubject.active) {
                    withContext(Dispatchers.IO) {
                        subSubjectDao.finishAndActiveNext(subSubject)
                        if (!evaluationDao.haveActiveSubSubject(subSubject.evaluation)) {
                            evaluationDao.markAsFinished(evaluationDao.getOnly(subSubject.evaluation))
                            activeSchoolYear =
                                evaluationDao.isSchoolYearActiveOnly(
                                    evaluationDao.getOnly(
                                        subSubject.evaluation
                                    ).schoolYear
                                )
                        }
                    }
                    _notifyUpdateUI.value = true
                } else {
                    withContext(Dispatchers.IO) {
                        subSubjectDao.active(subSubject)
                        val evaluation = evaluationDao.getOnly(subSubject.evaluation)
                        if (evaluation.finishDate != null) {
                            evaluationDao.markAsActive(evaluation)
                            activeSchoolYear =
                                evaluationDao.isSchoolYearActiveOnly(
                                    evaluationDao.getOnly(
                                        subSubject.evaluation
                                    ).schoolYear
                                )
                        }
                    }
                    _notifyUpdateUI.value = true
                }
                if (activeSchoolYear != oldValue) {
                    _showSnackBarMessage.value =
                        if (activeSchoolYear)
                            resources.getString(R.string.schoolYearActivated)
                        else
                            resources.getString(R.string.schoolYearFinished)
                }
            } catch (exception: Exception) {
                _showSnackBarMessage.value = resources.getString(R.string.errorUpdate)
            }
        }
    }
}