/*
 * MIT License
 *
 * Copyright (c) 2022 Cosme José Nieto Pérez
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.cosmejosenp.studentdiary.viewmodels

import android.app.Application
import android.view.View
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.cosmejosenp.studentdiary.R
import com.cosmejosenp.studentdiary.data.db.EduAppDatabase
import com.cosmejosenp.studentdiary.data.entity.*
import com.cosmejosenp.studentdiary.util.*
import kotlinx.coroutines.*

class AddSubjectViewModel(
    private val dataSource: EduAppDatabase,
    private val idSchoolYear: Long,
    private val idSubject: Long,
    application: Application
) : AndroidViewModel(application) {
    private val STANDARD_STRUCTURE: Int
        get() = 1
    private val STANDARD_UNITS_STRUCTURE: Int
        get() = 2
    private val PERSONALIZED_STRUCTURE: Int
        get() = 3
    private val EXTRAORDINARY_STANDARD_STRUCTURE: Int
        get() = 1
    private val EXTRAORDINARY_EXAMS_WORK_STRUCTURE: Int
        get() = 2
    private val resources = application.resources
    private val context = application.applicationContext
    private var viewModelJob = SupervisorJob()
    private val uiScope = CoroutineScope(Dispatchers.Main + viewModelJob)
    val evaluations =
        if (idSubject == 0L) dataSource.evaluationDao.getAllFromSchoolYear(idSchoolYear)
        else dataSource.evaluationDao.getAllPendingEvaluationsFromSubject(idSubject)
    val subject =
        if (idSubject != 0L) dataSource.subjectDao.get(idSubject)
        else MutableLiveData<Subject?>(null)
    private val _validData = MutableLiveData(false)
    val validData: LiveData<Boolean>
        get() = _validData
    private val _restoreSelectedItems = MutableLiveData(false)
    val restoreSelectedItems: LiveData<Boolean>
        get() = _restoreSelectedItems
    private val _navigateToDetailSubject = MutableLiveData<Long>()
    val navigateToDetailSubject: LiveData<Long>
        get() = _navigateToDetailSubject
    private val _showSnackBarMessage = MutableLiveData<String?>()
    val showSnackBarMessage: LiveData<String?>
        get() = _showSnackBarMessage
    private val _showToastMessage = MutableLiveData<String?>()
    val showToastMessage: LiveData<String?>
        get() = _showToastMessage
    private val _showDialog = MutableLiveData(false)
    val showDialog: LiveData<Boolean>
        get() = _showDialog
    private val _navigateToBackStack = MutableLiveData(false)
    val navigateToBackStack: LiveData<Boolean>
        get() = _navigateToBackStack
    var selectedItems = ArrayList<Evaluation>()
    private val _selectedColour = MutableLiveData<Colour?>(null)
    val selectedColour: LiveData<Colour?>
        get() = _selectedColour
    val subjectAttributesVisibility = Transformations.map(subject) {
        if (it == null)
            View.VISIBLE
        else
            View.GONE
    }
    val recyclerVisibility = Transformations.map(evaluations) {
        if (it.isEmpty()) {
            View.GONE
        } else {
            View.VISIBLE
        }
    }
    private val _groupStructureVisibility = MutableLiveData(false)
    val groupStructureVisibility = Transformations.map(_groupStructureVisibility) {
        if (it)
            View.VISIBLE
        else
            View.GONE
    }
    private val _groupExtraordinaryStructureVisibility = MutableLiveData(false)
    val groupExtraordinaryStructureVisibility =
        Transformations.map(_groupExtraordinaryStructureVisibility) {
            if (it)
                View.VISIBLE
            else
                View.GONE
        }
    private val _percentageGroupVisibility = MutableLiveData(true)
    val percentageGroupVisibility = Transformations.map(_percentageGroupVisibility) {
        if (it)
            View.VISIBLE
        else
            View.GONE
    }
    private val _percentageExtraordinaryGroupVisibility = MutableLiveData(false)
    val percentageExtraordinaryGroupVisibility =
        Transformations.map(_percentageExtraordinaryGroupVisibility) {
            if (it)
                View.VISIBLE
            else
                View.GONE
        }
    private val _showErrorPercentages = MutableLiveData(false)
    val showErrorPercentages: LiveData<Boolean>
        get() = _showErrorPercentages
    private val _showErrorExtPercentages = MutableLiveData(false)
    val showErrorExtPercentages: LiveData<Boolean>
        get() = _showErrorExtPercentages
    private val _selectAll = MutableLiveData(true)
    val selectAll: LiveData<Boolean>
        get() = _selectAll

    /**
     * This method will be called when this ViewModel is no longer used and will be destroyed.
     */
    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }

    /**
     * Evaluate if the data are different and if this are valid.
     *
     * @param title
     * @param percentageExams
     * @param percentageWorks
     * @param percentageHomeWork
     * @param percentageExtExams
     * @param percentageExtWork
     */
    fun evaluateData(
        title: String,
        percentageExams: String,
        percentageWorks: String,
        percentageHomeWork: String,
        percentageExtExams: String,
        percentageExtWork: String
    ) {
        var valid = title.trim().isNotEmpty()
        if (idSubject != 0L) {
            valid = selectedItems.isNotEmpty()
        }
        if (valid) {
            if (_groupStructureVisibility.value!! && _percentageGroupVisibility.value!!) {
                val exam =
                    if (percentageExams.isNotEmpty()) Integer.valueOf(percentageExams) else 70
                val work =
                    if (percentageWorks.isNotEmpty()) Integer.valueOf(percentageWorks) else 20
                val homework =
                    if (percentageHomeWork.isNotEmpty()) Integer.valueOf(percentageHomeWork) else 10
                if (exam + work + homework != 100) {
                    valid = false
                    _showErrorPercentages.value = true
                } else
                    _showErrorPercentages.value = false
            }
            if (_groupExtraordinaryStructureVisibility.value!! && _percentageExtraordinaryGroupVisibility.value!!) {
                val examExt =
                    if (percentageExtExams.isNotEmpty()) Integer.valueOf(percentageExtExams) else 80
                val workExt =
                    if (percentageExtWork.isNotEmpty()) Integer.valueOf(percentageExtWork) else 20
                if (examExt + workExt != 100) {
                    valid = false
                    _showErrorExtPercentages.value = true
                } else
                    _showErrorExtPercentages.value = false
            }
        }
        _validData.value = valid
    }

    fun doneNavigatingToDetailSubject() {
        _navigateToDetailSubject.value = 0L
    }

    /**
     * Start to navigate to back stack.
     *
     */
    fun startNavigateToBackStack() {
        _navigateToBackStack.value = true
    }

    /**
     * Finish notification of navigation for prevent unwanted navigation.
     *
     */
    fun doneNavigatingToBackStack() {
        _navigateToBackStack.value = false
    }

    /**
     * Start show the confirm dialog.
     *
     */
    fun startShowDialog() {
        _showDialog.value = true
    }

    /**
     * Finish notification of showing dialog for prevent unwanted behaviour.
     *
     */
    fun showDialogDone() {
        _showDialog.value = false
    }

    /**
     * Finish notification of showing snackBar for prevent unwanted messages.
     *
     */
    fun showSnackBarMessageDone() {
        _showSnackBarMessage.value = null
    }

    /**
     * Finish notification of showing snackBar for prevent unwanted messages.
     *
     */
    fun showToastMessageDone() {
        _showToastMessage.value = null
    }

    /**
     * Notify to start restore the ui data content when the configuration change.
     *
     */
    fun startRestoreContent() {
        _restoreSelectedItems.value = true
        reloadColour()
    }

    /**
     * Finish restore content for prevent unwanted behaviour.
     *
     */
    fun restoreContentDone() {
        _restoreSelectedItems.value = false
    }

    /**
     * Change the selected state of the [evaluation].
     *
     * @param evaluation to select or deselect.
     */
    fun toggleItemSelected(evaluation: Evaluation) {
        if (selectedItems.contains(evaluation)) {
            selectedItems.remove(evaluation)
        } else {
            selectedItems.add(evaluation)
        }
        checkVisibility()
    }

    /**
     * Select a new colour value.
     *
     * @param colour
     */
    fun selectColour(colour: Colour) {
        _selectedColour.value = colour
    }

    /**
     * Change the selected state of all evaluations to selected.
     * This function is called in the first load.
     *
     */
    fun selectAllEvaluations() {
        selectedItems.addAll(evaluations.value!!)
        _restoreSelectedItems.value = true
        checkVisibility()
    }

    /**
     * Finish notification of selection for prevent unwanted behaviour.
     *
     */
    fun doneSelectAll() {
        _selectAll.value = false
    }

    /**
     * reload the [_selectedColour] to update the colour of the ui.
     *
     */
    private fun reloadColour() {
        _selectedColour.value = _selectedColour.value
    }

    /**
     * Check the visibility of the user interface items.
     *
     */
    private fun checkVisibility() {
        var foundExtraordinaryEvaluation = false
        var foundNormalEvaluation = false
        var i = 0

        while (i < selectedItems.size && (!foundExtraordinaryEvaluation || !foundNormalEvaluation)) {
            if (selectedItems[i].extraOrdinary) {
                foundExtraordinaryEvaluation = true
            } else {
                foundNormalEvaluation = true
            }
            i++
        }
        _groupStructureVisibility.value = foundNormalEvaluation
        _groupExtraordinaryStructureVisibility.value = foundExtraordinaryEvaluation
    }

    /**
     * Check the visibility of structure group.
     *
     */
    fun checkVisibilityStandard(personalized: Boolean) {
        _percentageGroupVisibility.value = !personalized
    }

    /**
     * Check the visibility of extraordinary structure group.
     *
     */
    fun checkVisibilityExtraordinary(workWithExams: Boolean) {
        _percentageExtraordinaryGroupVisibility.value = workWithExams
    }

    /**
     * Save the subject or link the subject with the selected evaluations
     * depending of constructor parameters.
     *
     * @param name of the subject.
     * @param continuousAssessment if the subject is a continuous assessment.
     * @param createStandardStructure of this subject in normal evaluations.
     * @param createStandardStructureWithUnits of this subject in normal evaluations.
     * @param createStandardExtraordinaryStructure of this subject in extraordinary evaluations.
     * @param percentageExams in standard structure.
     * @param percentageWorks in standard structure.
     * @param percentageHomeWork in standard structure.
     * @param percentageExtExams in extraordinary structure.
     * @param percentageExtWork in extraordinary structure.
     */
    fun saveSubject(
        name: String,
        continuousAssessment: Boolean,
        createStandardStructure: Boolean,
        createStandardStructureWithUnits: Boolean,
        createStandardExtraordinaryStructure: Boolean,
        percentageExams: String,
        percentageWorks: String,
        percentageHomeWork: String,
        percentageExtExams: String,
        percentageExtWork: String
    ) {
        val exam =
            if (percentageExams.isNotEmpty()) Integer.valueOf(percentageExams) else 70
        val work =
            if (percentageWorks.isNotEmpty()) Integer.valueOf(percentageWorks) else 20
        val homework =
            if (percentageHomeWork.isNotEmpty()) Integer.valueOf(percentageHomeWork) else 10
        val examExt =
            if (percentageExtExams.isNotEmpty()) Integer.valueOf(percentageExtExams) else 80
        val workExt =
            if (percentageExtWork.isNotEmpty()) Integer.valueOf(percentageExtWork) else 20
        val oldSelectedItems = ArrayList<Evaluation>(selectedItems)
        oldSelectedItems.sortBy { it.position }
        val oldColour = obtainTypeColour(selectedColour.value!!, context)
        val typeStructure =
            when {
                createStandardStructure -> STANDARD_STRUCTURE
                createStandardStructureWithUnits -> STANDARD_UNITS_STRUCTURE
                else -> PERSONALIZED_STRUCTURE
            }
        val typeExtraordinaryStructure =
            when {
                createStandardExtraordinaryStructure -> EXTRAORDINARY_STANDARD_STRUCTURE
                else -> EXTRAORDINARY_EXAMS_WORK_STRUCTURE
            }

        uiScope.launch {
            try {
                val id =
                    if (idSubject == 0L) saveSubject(name.trim(), continuousAssessment, oldColour)
                    else idSubject
                if (oldSelectedItems.isNotEmpty()) {
                    try {
                        val subSubjects = saveSubSubject(oldSelectedItems, id)
                        try {
                            val blocksWithSubBlocks = createStructure(
                                oldSelectedItems,
                                subSubjects,
                                typeStructure,
                                typeExtraordinaryStructure,
                                exam,
                                work,
                                homework,
                                examExt,
                                workExt
                            )
                            withContext(Dispatchers.IO) {
                                dataSource.blockDao.addWithSubBlocks(*blocksWithSubBlocks.toTypedArray())
                            }
                        } catch (exception: Exception) {
                            _showToastMessage.value =
                                resources.getString(R.string.errorCreateStructure)
                        }
                    } catch (exception: Exception) {
                        _showToastMessage.value = resources.getString(R.string.errorSaveSubSubjects)
                    }
                }
                _navigateToDetailSubject.value = id
            } catch (exception: Exception) {
                _showSnackBarMessage.value = resources.getString(R.string.errorUpdate)
            }
        }
    }

    /**
     * Save Subject into database.
     *
     * @param name
     * @param continuousAssessment
     * @param colour
     * @return subject cod.
     */
    private suspend fun saveSubject(
        name: String,
        continuousAssessment: Boolean,
        colour: Int
    ): Long =
        withContext(Dispatchers.IO) {
            val subject = Subject(idSchoolYear, name, continuousAssessment, colour = colour)
            return@withContext dataSource.subjectDao.add(subject)[0]
        }

    /**
     * Save the links of this [idSubject] with all selected [evaluations]
     *
     * @param evaluations
     * @param idSubject
     * @return a list of subSubjects.
     */
    private suspend fun saveSubSubject(
        evaluations: ArrayList<Evaluation>,
        idSubject: Long
    ): ArrayList<SubSubject> =
        withContext(Dispatchers.IO) {
            val subSubjects = ArrayList<SubSubject>()
            var firstTimeActivated = false
            for (evaluation: Evaluation in evaluations) {
                val subSubject = SubSubject(
                    idSubject,
                    evaluation.id,
                    active = (evaluation.startDate != null && evaluation.finishDate == null && !firstTimeActivated)
                )
                if (subSubject.active)
                    firstTimeActivated = true
                subSubjects.add(subSubject)
            }
            val ids = dataSource.subSubjectDao.add(*subSubjects.toTypedArray())
            for ((i, subSubject) in subSubjects.withIndex())
                subSubject.id = ids[i]
            return@withContext subSubjects
        }

    /**
     * Create the structure fo subSubjects.
     *
     * @param evaluations of subSubjects. Previous selected.
     * @param subSubjects previous generated.
     * @param typeStructure to create.
     * @param typeExtraordinaryStructure to create.
     * @param exam percentage.
     * @param work percentage.
     * @param homework percentage.
     * @param examExt percentage.
     * @param workExt percentage.
     */
    private suspend fun createStructure(
        evaluations: ArrayList<Evaluation>,
        subSubjects: ArrayList<SubSubject>,
        typeStructure: Int,
        typeExtraordinaryStructure: Int,
        exam: Int,
        work: Int,
        homework: Int,
        examExt: Int,
        workExt: Int
    ): List<BlockWithSubBlocks> =
        withContext(Dispatchers.IO) {
            val blocksWithSubBlocks = ArrayList<BlockWithSubBlocks>()
            val namesArray = listOf(
                resources.getString(R.string.examsName),
                resources.getString(R.string.worksName),
                resources.getString(R.string.homeworksName)
            )
            val percentageArray = listOf(exam, work, homework)
            val percentageExtArray = listOf(examExt, workExt)
            val typesArray = listOf(TYPE_EXAM, TYPE_WORK, TYPE_HOMEWORK)
            var unitCount = 1
            for (i in 0 until evaluations.size) {
                if (idSubject != 0L)
                    unitCount = 1
                if (evaluations[i].extraOrdinary) {
                    when (typeExtraordinaryStructure) {
                        EXTRAORDINARY_STANDARD_STRUCTURE -> {
                            val block = createBlock(
                                subSubjects[i].id,
                                resources.getString(R.string.examsName),
                                100,
                                TYPE_EXAM
                            )
                            blocksWithSubBlocks.add(BlockWithSubBlocks(block, ArrayList()))
                        }
                        EXTRAORDINARY_EXAMS_WORK_STRUCTURE -> {
                            for (j in 0 until 2) {
                                val block = createBlock(
                                    subSubjects[i].id,
                                    namesArray[j],
                                    percentageExtArray[j],
                                    typesArray[j]
                                )
                                blocksWithSubBlocks.add(BlockWithSubBlocks(block, ArrayList()))
                            }
                        }
                    }
                } else {
                    when (typeStructure) {
                        STANDARD_STRUCTURE -> {
                            for (j in 0 until 3) {
                                val block = createBlock(
                                    subSubjects[i].id,
                                    namesArray[j],
                                    percentageArray[j],
                                    typesArray[j]
                                )
                                blocksWithSubBlocks.add(BlockWithSubBlocks(block, ArrayList()))
                            }
                        }
                        STANDARD_UNITS_STRUCTURE -> {
                            for (j in 1..3) {
                                val name =
                                    if (idSubject == 0L) resources.getString(
                                        R.string.nameOfUnit,
                                        unitCount
                                    )
                                    else "${evaluations[i].name} ${resources.getString(
                                        R.string.nameOfUnit,
                                        unitCount
                                    )}"
                                val block = createBlock(
                                    subSubjects[i].id,
                                    name
                                )
                                val subBlocks = ArrayList<SubBlock>()
                                for (l in 0 until 3) {
                                    val subBlock = createSubBlock(
                                        namesArray[l],
                                        percentageArray[l],
                                        typesArray[l]
                                    )
                                    subBlocks.add(subBlock)
                                }
                                unitCount++
                                blocksWithSubBlocks.add(BlockWithSubBlocks(block, subBlocks))
                            }
                        }
                    }
                }
            }
            return@withContext blocksWithSubBlocks
        }

    /**
     * Create a block for this [idSubSubject].
     *
     * @param idSubSubject
     * @param name of the block.
     * @param percentage of the block.
     * @param type of the block.
     * @return the generated block.
     */
    private fun createBlock(
        idSubSubject: Long,
        name: String,
        percentage: Int? = null,
        type: Int? = null
    ): Block {
        return Block(idSubSubject, name, percent = percentage, type = type)
    }

    /**
     * create a subBlock.
     * @param name of the subBlock.
     * @param percentage of the subBlock.
     * @param type of the subBlock.
     * @return the generated subBlock.
     */
    private fun createSubBlock(
        name: String,
        percentage: Int,
        type: Int
    ): SubBlock {
        return SubBlock(0, name, percent = percentage, type = type)
    }
}

