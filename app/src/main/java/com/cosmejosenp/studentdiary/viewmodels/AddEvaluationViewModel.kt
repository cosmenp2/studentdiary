/*
 * MIT License
 *
 * Copyright (c) 2022 Cosme José Nieto Pérez
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.cosmejosenp.studentdiary.viewmodels

import android.app.Application
import android.view.View
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.cosmejosenp.studentdiary.R
import com.cosmejosenp.studentdiary.data.db.EduAppDatabase
import com.cosmejosenp.studentdiary.data.entity.BlockWithSubBlocks
import com.cosmejosenp.studentdiary.data.entity.Evaluation
import com.cosmejosenp.studentdiary.data.entity.SubSubject
import com.cosmejosenp.studentdiary.data.entity.Subject
import kotlinx.coroutines.*

class AddEvaluationViewModel(
    private val dataSource: EduAppDatabase,
    private val idSchoolYear: Long,
    private val idEvaluation: Long,
    application: Application
) : AndroidViewModel(application) {
    private val resources = application.resources
    private var viewModelJob = SupervisorJob()
    private val uiScope = CoroutineScope(Dispatchers.Main + viewModelJob)
    val subjects =
        if (idEvaluation == 0L) dataSource.subjectDao.getAllFromSchoolYear(idSchoolYear)
        else dataSource.subjectDao.getAllPendingFromEvaluation(idEvaluation)
    val evaluation =
        if (idEvaluation != 0L) dataSource.evaluationDao.get(idEvaluation)
        else MutableLiveData<Evaluation?>(null)
    val numberOfEvaluations = dataSource.evaluationDao.countAllFromSchoolYear(idSchoolYear)
    private val _validData = MutableLiveData(false)
    private var _extraordinaryEvaluation = false
    val extraordinaryEvaluation: Boolean
        get() = _extraordinaryEvaluation
    private var _positionSelected = 0
    val positionSelected: Int
        get() = _positionSelected
    val validData: LiveData<Boolean>
        get() = _validData
    private val _restoreSelectedItems = MutableLiveData(false)
    val restoreSelectedItems: LiveData<Boolean>
        get() = _restoreSelectedItems
    private var _generateName = true
    val generateName: Boolean
        get() = _generateName
    private val _navigateToDetailEvaluation = MutableLiveData<Long>()
    val navigateToDetailEvaluation: LiveData<Long>
        get() = _navigateToDetailEvaluation
    private val _showSnackBarMessage = MutableLiveData<String?>()
    val showSnackBarMessage: LiveData<String?>
        get() = _showSnackBarMessage
    private val _showToastMessage = MutableLiveData<String?>()
    val showToastMessage: LiveData<String?>
        get() = _showToastMessage
    private val _showDialog = MutableLiveData(false)
    val showDialog: LiveData<Boolean>
        get() = _showDialog
    private val _navigateToBackStack = MutableLiveData(false)
    val navigateToBackStack: LiveData<Boolean>
        get() = _navigateToBackStack
    var selectedItems = ArrayList<Subject>()
    val evaluationAttributesVisibility = Transformations.map(evaluation) {
        if (it == null)
            View.VISIBLE
        else
            View.GONE
    }
    val recyclerVisibility = Transformations.map(subjects) {
        if (it.isEmpty()) {
            View.GONE
        } else {
            View.VISIBLE
        }
    }
    private val _checkBoxCopyVisibility = MutableLiveData(false)
    val checkBoxCopyVisibility = Transformations.map(_checkBoxCopyVisibility) {
        if (it)
            View.VISIBLE
        else
            View.GONE
    }
    private val _checkBoxExtraordinaryCopyVisibility = MutableLiveData(false)
    val checkBoxExtraordinaryCopyVisibility =
        Transformations.map(_checkBoxExtraordinaryCopyVisibility) {
            if (it)
                View.VISIBLE
            else
                View.GONE
        }
    private val _selectAll = MutableLiveData(true)
    val selectAll: LiveData<Boolean>
        get() = _selectAll

    /**
     * This method will be called when this ViewModel is no longer used and will be destroyed.
     */
    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }

    /**
     * Update the checked state of the evaluation.
     *
     * @param checked
     */
    fun updateExtraordinaryEvaluationCheck(checked: Boolean) {
        _extraordinaryEvaluation = checked
        checkVisibility()
    }

    /**
     * Finish notification of generation name for prevent unwanted behaviour.
     *
     */
    fun doneGenerateName() {
        _generateName = false
    }

    /**
     * Save the selected position for restore in a configuration change.
     *
     * @param position
     */
    fun savePositionSelected(position: Int) {
        _positionSelected = position
    }

    /**
     * Evaluate if the data are valid.
     *
     * @param title
     */
    fun evaluateData(title: String) {
        var valid = title.trim().isNotEmpty()
        if (idEvaluation != 0L) {
            valid = selectedItems.isNotEmpty()
        }
        _validData.value = valid
    }

    /**
     * Finish notification of navigation for prevent unwanted navigation.
     *
     */
    fun doneNavigatingToDetailEvaluation() {
        _navigateToDetailEvaluation.value = 0L
    }

    /**
     * Start to navigate to back stack.
     *
     */
    fun startNavigateToBackStack() {
        _navigateToBackStack.value = true
    }

    /**
     * Finish notification of navigation for prevent unwanted navigation.
     *
     */
    fun doneNavigatingToBackStack() {
        _navigateToBackStack.value = false
    }

    /**
     * Start show the confirm dialog.
     *
     */
    fun startShowDialog() {
        _showDialog.value = true
    }

    /**
     * Finish notification of showing dialog for prevent unwanted behaviour.
     *
     */
    fun showDialogDone() {
        _showDialog.value = false
    }

    /**
     * Finish notification of showing snackBar for prevent unwanted messages.
     *
     */
    fun showSnackBarMessageDone() {
        _showSnackBarMessage.value = null
    }

    /**
     * Finish notification of showing dialog for prevent unwanted messages.
     *
     */
    fun showToastMessageDone() {
        _showToastMessage.value = null
    }

    /**
     * Notify to start restore the ui data content when the configuration change.
     *
     */
    fun startRestoreContent() {
        _restoreSelectedItems.value = true
    }

    /**
     * Finish restore content for prevent unwanted behaviour.
     *
     */
    fun restoreContentDone() {
        _restoreSelectedItems.value = false
    }

    /**
     * Change the selected state of the [subject].
     *
     * @param subject to select or deselect.
     */
    fun toggleItemSelected(subject: Subject) {
        if (selectedItems.contains(subject)) {
            selectedItems.remove(subject)
        } else {
            selectedItems.add(subject)
        }
        checkVisibility()
    }

    /**
     * Change the selected state of all subjects to selected.
     * This function is called in the first load.
     *
     */
    fun selectAllSubjects() {
        selectedItems.addAll(subjects.value!!)
        _restoreSelectedItems.value = true
        checkVisibility()
    }

    /**
     * Finish notification of selection for prevent unwanted behaviour.
     *
     */
    fun doneSelectAll() {
        _selectAll.value = false
    }

    /**
     * Check the visibility of the user interface items.
     *
     */
    private fun checkVisibility() {
        var extraordinaryEvaluation = this.extraordinaryEvaluation
        if (idEvaluation != 0L && evaluation.value != null) {
            extraordinaryEvaluation = evaluation.value!!.extraOrdinary
        }
        _checkBoxCopyVisibility.value = !extraordinaryEvaluation && selectedItems.isNotEmpty()
        _checkBoxExtraordinaryCopyVisibility.value =
            extraordinaryEvaluation && selectedItems.isNotEmpty()
    }

    /**
     * Prepare evaluation to save this evaluation or this subSubject,
     * depending of the constructor parameters, and then save the items.
     *
     * @param name of the evaluation.
     * @param positionSelected of the evaluation.
     * @param copyStructure if the user want to copy the previous available structure.
     * @param copyExtraordinaryStructure if the user want to copy the previous extraordinary available structure.
     */
    fun saveEvaluation(
        name: String,
        positionSelected: Int,
        copyStructure: Boolean,
        copyExtraordinaryStructure: Boolean
    ) {
        val oldSelectedItems = ArrayList<Subject>(selectedItems)
        val isExtraordinary = extraordinaryEvaluation
        val position =
            if (idEvaluation != 0L) evaluation.value!!.position
            else positionSelected
        uiScope.launch {
            try {
                val id =
                    if (idEvaluation == 0L) saveEvaluation(name.trim(), position, isExtraordinary)
                    else idEvaluation
                if (oldSelectedItems.isNotEmpty()) {
                    for (subject in oldSelectedItems) {
                        try {
                            val idSubSubject = saveSubSubject(id, subject.id)
                            try {
                                val blocksWithSubBlocks =
                                    searchStructure(
                                        subject.id,
                                        position,
                                        isExtraordinary,
                                        copyStructure,
                                        copyExtraordinaryStructure
                                    )
                                if (blocksWithSubBlocks.isNotEmpty()) {
                                    saveStructure(blocksWithSubBlocks, name.trim(), idSubSubject)
                                }
                            } catch (exception: Exception) {
                                _showToastMessage.value =
                                    resources.getString(
                                        R.string.errorCreateStructureOf,
                                        subject.name
                                    )
                            }
                        } catch (exception: Exception) {
                            _showToastMessage.value =
                                resources.getString(R.string.errorSaveSubSubject, subject.name)
                        }
                    }
                }
                _navigateToDetailEvaluation.value = id
            } catch (exception: Exception) {
                _showSnackBarMessage.value = resources.getString(R.string.errorUpdate)
            }
        }
    }

    /**
     * Save the evaluation into database.
     *
     * @param name of the evaluation.
     * @param position of the evaluation.
     * @param extraordinary if this evaluation is an extraordinary evaluation.
     * @return evaluation cod.
     */
    private suspend fun saveEvaluation(
        name: String,
        position: Int,
        extraordinary: Boolean
    ): Long =
        withContext(Dispatchers.IO) {
            val evaluation =
                Evaluation(idSchoolYear, name, position, extraOrdinary = extraordinary)
            return@withContext dataSource.evaluationDao.addAndReorder(evaluation)
        }

    /**
     * Save the link of subject with this evaluation.
     *
     * @param idEvaluation to link.
     * @param idSubject to link.
     * @return cod of subSubject.
     */
    private suspend fun saveSubSubject(
        idEvaluation: Long,
        idSubject: Long
    ): Long =
        withContext(Dispatchers.IO) {
            val subSubject = SubSubject(
                idSubject,
                idEvaluation
            )
            return@withContext dataSource.subSubjectDao.add(subSubject)[0]
        }

    /**
     * Search the suitable structure for this [idSubject].
     *
     * @param idSubject to search.
     * @param position position of actual evaluation.
     * @param extraordinary if this evaluation is extraordinary.
     * @param copyStructure if the user want to copy the previous available structure.
     * @param copyExtraordinaryStructure if the user want to copy the previous extraordinary available structure.
     * @return subSubject cod.
     */
    private suspend fun searchStructure(
        idSubject: Long,
        position: Int,
        extraordinary: Boolean,
        copyStructure: Boolean,
        copyExtraordinaryStructure: Boolean
    ): List<BlockWithSubBlocks> =
        withContext(Dispatchers.IO) {
            return@withContext if (extraordinary && copyExtraordinaryStructure)
                dataSource.blockDao.getAllWithSubBlocksFromSubjectAndEvaluation(
                    idSubject,
                    extraordinary = true
                )
            else if (!extraordinary && copyStructure)
                dataSource.blockDao.getAllWithSubBlocksFromSubjectAndEvaluation(
                    idSubject,
                    position = position
                )
            else
                ArrayList()
        }

    /**
     * Save the subject structure.
     *
     * @param blocksWithSubBlocks the structure of the subject.
     * @param name of the evaluation
     * @param idSubSubject
     */
    private suspend fun saveStructure(
        blocksWithSubBlocks: List<BlockWithSubBlocks>,
        name: String,
        idSubSubject: Long
    ) {
        withContext(Dispatchers.IO) {
            updateBlockWithSubBlocks(blocksWithSubBlocks, name, idSubSubject)
            dataSource.blockDao.addWithSubBlocks(*blocksWithSubBlocks.toTypedArray())
        }
    }

    /**
     * Update the blocks and subBlocks according to the new evaluation.
     *
     * @param blocksWithSubBlocks to update codes.
     * @param name of the evaluation.
     * @param idSubSubject
     */
    private fun updateBlockWithSubBlocks(
        blocksWithSubBlocks: List<BlockWithSubBlocks>,
        name: String,
        idSubSubject: Long
    ) {
        for (blockWithSubBlock in blocksWithSubBlocks) {
            blockWithSubBlock.block.subSubject = idSubSubject
            blockWithSubBlock.block.id = 0L
            if (blockWithSubBlock.subBlocks.isNotEmpty()) {
                blockWithSubBlock.block.name = "$name ${blockWithSubBlock.block.name}"
                for (subBlock in blockWithSubBlock.subBlocks) {
                    subBlock.id = 0L
                }
            }
        }
    }
}

