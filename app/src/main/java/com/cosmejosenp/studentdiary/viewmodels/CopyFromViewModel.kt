/*
 * MIT License
 *
 * Copyright (c) 2022 Cosme José Nieto Pérez
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.cosmejosenp.studentdiary.viewmodels

import android.app.Application
import android.view.View
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.cosmejosenp.studentdiary.R
import com.cosmejosenp.studentdiary.data.db.EduAppDatabase
import com.cosmejosenp.studentdiary.data.entity.BlockWithSubBlocks
import com.cosmejosenp.studentdiary.data.entity.Evaluation
import com.cosmejosenp.studentdiary.data.entity.SubBlock
import kotlinx.coroutines.*

class CopyFromViewModel(
    private val dataSource: EduAppDatabase,
    private val idSubject: Long,
    private val idSubSubject: Long,
    application: Application
) : AndroidViewModel(application) {
    private val resources = application.resources
    private var viewModelJob = SupervisorJob()
    private val uiScope = CoroutineScope(Dispatchers.Main + viewModelJob)
    val evaluations =
        dataSource.evaluationDao.getOtherSubSubjectWithValidStructure(idSubject, idSubSubject)
    val subject = dataSource.subjectDao.get(idSubject)
    private val _structure = MutableLiveData<List<BlockWithSubBlocks>>(null)
    val structure: LiveData<List<BlockWithSubBlocks>>
        get() = _structure
    private val _selectedItem = MutableLiveData<Evaluation?>(null)
    val selectedItem: LiveData<Evaluation?>
        get() = _selectedItem
    val validData = Transformations.map(selectedItem) {
        it != null && it.id != 0L
    }
    private val _restoreSelectedItem = MutableLiveData(false)
    val restoreSelectedItem: LiveData<Boolean>
        get() = _restoreSelectedItem
    private val _navigateToDetailSubSubject = MutableLiveData<Long?>()
    val navigateToDetailSubSubject: LiveData<Long?>
        get() = _navigateToDetailSubSubject
    private val _showSnackBarMessage = MutableLiveData<String?>()
    val showSnackBarMessage: LiveData<String?>
        get() = _showSnackBarMessage
    private val _showToastMessage = MutableLiveData<String?>()
    val showToastMessage: LiveData<String?>
        get() = _showToastMessage
    private val _disableCloneMode = MutableLiveData(false)
    val disableCloneMode: LiveData<Boolean>
        get() = _disableCloneMode
    private val _showDialog = MutableLiveData(false)
    val showDialog: LiveData<Boolean>
        get() = _showDialog
    private val _navigateToBackStack = MutableLiveData(false)
    val navigateToBackStack: LiveData<Boolean>
        get() = _navigateToBackStack
    val structurePreviewVisibility = Transformations.map(selectedItem) {
        if (it == null || it.id == 0L)
            View.GONE
        else
            View.VISIBLE
    }

    /**
     * This method will be called when this ViewModel is no longer used and will be destroyed.
     */
    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }

    /**
     * Finish notification of navigation for prevent unwanted navigation.
     *
     */
    fun doneNavigatingToDetailSubject() {
        _navigateToDetailSubSubject.value = null
    }

    /**
     * Start to navigate to back stack.
     *
     */
    fun startNavigateToBackStack() {
        _navigateToBackStack.value = true
    }

    /**
     * Finish notification of navigation for prevent unwanted navigation.
     *
     */
    fun doneNavigatingToBackStack() {
        _navigateToBackStack.value = false
    }

    /**
     * Start show the confirm dialog.
     *
     */
    fun startShowDialog() {
        _showDialog.value = true
    }

    /**
     * Finish notification of showing dialog for prevent unwanted behaviour.
     *
     */
    fun showDialogDone() {
        _showDialog.value = false
    }

    /**
     * Finish clone mode
     *
     */
    fun startDisableCloneMode() {
        _disableCloneMode.value = true
    }

    /**
     * Finish notification of finish clone mode for prevent unwanted behaviour.
     *
     */
    fun doneDisableCloneMode() {
        _disableCloneMode.value = false
    }

    /**
     * Finish notification of showing snackBar for prevent unwanted messages.
     *
     */
    fun showSnackBarMessageDone() {
        _showSnackBarMessage.value = null
    }

    /**
     * Finish notification of showing snackBar for prevent unwanted messages.
     *
     */
    fun showToastMessageDone() {
        _showToastMessage.value = null
    }

    /**
     * Notify to start restore the ui data content when the configuration change.
     *
     */
    fun startRestoreContent() {
        _restoreSelectedItem.value = true
    }

    /**
     * Finish restore content for prevent unwanted behaviour.
     *
     */
    fun restoreContentDone() {
        _restoreSelectedItem.value = false
    }

    /**
     * Update the preview of the structure when clone mode change the selected state.
     *
     * @param cloneMode
     */
    fun updateSelectedItem(cloneMode: Boolean) {
        _selectedItem.value?.let { updateSelectedItem(it, cloneMode) }
    }

    /**
     * Update the preview of the structure, when the user select an evaluation.
     *
     * @param evaluation
     * @param cloneMode
     */
    fun updateSelectedItem(evaluation: Evaluation, cloneMode: Boolean) {
        _selectedItem.value = evaluation
        if (evaluation.id != 0L)
            uiScope.launch {
                val sourceStructure = withContext(Dispatchers.IO) {
                    val subSubject = dataSource.subSubjectDao.get(evaluation.id, idSubject)
                    dataSource.blockDao.getAllWithSubBlocksListFromSubSubject(subSubject.id)
                }
                if (cloneMode)
                    _structure.value = sourceStructure.sortedBy { it.block.name }
                else
                    _structure.value = processStructure(sourceStructure)
            }
        else
            _structure.value = ArrayList()
    }

    /**
     * Calculate the final structure.
     *
     * @param _source of blockWithSubBlocks for search the selected subSubject.
     * @return the result structure.
     */
    private suspend fun processStructure(_source: List<BlockWithSubBlocks>): List<BlockWithSubBlocks> {
        val source = ArrayList<BlockWithSubBlocks>()
        for (block in _source) {
            source.add(block.copyImpl())
        }
        val resultStructure = ArrayList<BlockWithSubBlocks>()
        val target = ArrayList<BlockWithSubBlocks>(
            withContext(Dispatchers.IO) {
                dataSource.blockDao.getAllWithSubBlocksListFromSubSubject(idSubSubject)
            }
        )
        withContext(Dispatchers.IO) {
            for (blockWithSubBlocks in source) {
                resultStructure.add(processBlockWithSubBlocks(blockWithSubBlocks, target))
            }
        }
        if (target.isNotEmpty())
            resultStructure.addAll(target)

        return resultStructure.sortedBy { it.block.name }
    }

    /**
     * Calculate the result blockWithSubBlocks structure.
     *
     * @param source starter structure of the block.
     * @param target structure to reach.
     * @return the result blockWithSubBlock structure.
     */
    private fun processBlockWithSubBlocks(
        source: BlockWithSubBlocks,
        target: ArrayList<BlockWithSubBlocks>
    ): BlockWithSubBlocks {
        lateinit var blockWithSubBlocks: BlockWithSubBlocks
        var found = false
        var i = 0
        while (i < target.size && !found) {
            if ((source.block.type == null && target[i].block.name.equals(source.block.name, true))
                || (target[i].block.type != null && target[i].block.type == source.block.type)
            ) {
                target[i].block.let {
                    it.percent = source.block.percent
                    it.name = source.block.name
                }
                if (source.subBlocks.isNotEmpty()) {
                    target[i].subBlocks =
                        processSubBlocks(source.subBlocks, target[i].subBlocks, target[i].block.id)
                }
                blockWithSubBlocks = target[i]
                target.removeAt(i)
                found = true
            }
            i++
        }
        if (!found) {
            blockWithSubBlocks = updateBlockWithSubBlocks(source, idSubSubject)
        }
        return blockWithSubBlocks
    }

    /**
     * Calculate the result subBlocks structure.
     *
     * @param source starter structure of the subBlocks.
     * @param _target structure to reach.
     * @param idBlock of the subBlock.
     * @return the result subBlocks structure.
     */
    private fun processSubBlocks(
        source: List<SubBlock>,
        _target: List<SubBlock>,
        idBlock: Long
    ): List<SubBlock> {
        val target = ArrayList<SubBlock>()
        for (subBlock in _target)
            target.add(subBlock.copy())
        val result = ArrayList<SubBlock>()
        for (subBlock in source) {
            var found = false
            var i = 0
            while (i < target.size && !found) {
                if ((subBlock.type == null && target[i].name.equals(subBlock.name, true))
                    || (target[i].type != null && target[i].type == subBlock.type)
                ) {
                    target[i].let {
                        it.percent = subBlock.percent
                        it.name = subBlock.name
                    }
                    found = true
                    result.add(target[i])
                    target.removeAt(i)
                }
                i++
            }
            if (!found) {
                subBlock.id = 0L
                subBlock.block = idBlock
                result.add(subBlock)
            }
        }
        if (target.isNotEmpty()) {
            result.addAll(target)
        }
        return result.sortedBy { it.name }
    }

    /**
     * Save the previewStructure to persistence.
     *
     * @param cloneMode if recreate with the new structure.
     */
    fun startCopy(cloneMode: Boolean) {
        val selectedStructure = structure.value!!
        uiScope.launch {
            if (cloneMode)
                cloneEvaluation(selectedStructure)
            else
                copyStructureTo(selectedStructure)
        }
    }

    /**
     * Delete the old structure of the subject and create the new one.
     *
     * @param structure to create.
     */
    private suspend fun cloneEvaluation(
        structure: List<BlockWithSubBlocks>
    ) {
        try {
            withContext(Dispatchers.IO) {
                dataSource.blockDao.delete(dataSource.subSubjectDao.getOnly(idSubSubject))
            }
            createStructureAndSave(structure, idSubSubject)
            _navigateToDetailSubSubject.value = idSubSubject
        } catch (_: Exception) {
            _showSnackBarMessage.value = resources.getString(R.string.errorUpdate)
        }
    }

    /**
     * Create the new structure and save it.
     *
     * @param blocksWithSubBlocks to create.
     * @param idSubSubject for update codes.
     */
    private suspend fun createStructureAndSave(
        blocksWithSubBlocks: List<BlockWithSubBlocks>,
        idSubSubject: Long
    ) {
        withContext(Dispatchers.IO) {
            updateBlocksWithSubBlocks(blocksWithSubBlocks, idSubSubject)
            dataSource.blockDao.addWithSubBlocks(*blocksWithSubBlocks.toTypedArray())
        }
    }

    /**
     * Update the structure of the blockWithSubBlocks list according with the new structure.
     *
     * @param blocksWithSubBlocks to update.
     * @param idSubSubject of the blocks.
     */
    private fun updateBlocksWithSubBlocks(
        blocksWithSubBlocks: List<BlockWithSubBlocks>,
        idSubSubject: Long
    ) {
        for (blockWithSubBlock in blocksWithSubBlocks) {
            updateBlockWithSubBlocks(blockWithSubBlock, idSubSubject)
        }
    }

    /**
     * Update the blocksWithSubBlocks structure according with the new structure.
     *
     * @param blockWithSubBlock to update.
     * @param idSubSubject of the block.
     * @return the updated blockWithSubBlocks structure.
     */
    private fun updateBlockWithSubBlocks(
        blockWithSubBlock: BlockWithSubBlocks,
        idSubSubject: Long
    ): BlockWithSubBlocks {
        blockWithSubBlock.block.subSubject = idSubSubject
        blockWithSubBlock.block.id = 0L
        if (blockWithSubBlock.subBlocks.isNotEmpty()) {
            for (subBlock in blockWithSubBlock.subBlocks) {
                subBlock.id = 0L
                subBlock.block = 0L
            }
        }
        return blockWithSubBlock
    }

    /**
     * Save and update the preview structure into persistence.
     *
     * @param structure to save and update.
     */
    private suspend fun copyStructureTo(
        structure: List<BlockWithSubBlocks>
    ) {
        try {
            withContext(Dispatchers.IO) {
                dataSource.blockDao.addNewsAndUpdateOlder(structure)
            }
            _navigateToDetailSubSubject.value = idSubSubject
        } catch (_: Exception) {
            _showSnackBarMessage.value = resources.getString(R.string.errorUpdate)
        }
    }
}

