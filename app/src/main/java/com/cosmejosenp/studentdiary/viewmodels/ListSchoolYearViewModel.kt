/*
 * MIT License
 *
 * Copyright (c) 2022 Cosme José Nieto Pérez
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.cosmejosenp.studentdiary.viewmodels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData

/**
 * ViewModel for ListActiveSchoolYearFragment
 */
class ListSchoolYearViewModel(
    application: Application
) : AndroidViewModel(application) {
    private val _navigateToAddSchoolYear = MutableLiveData<Boolean>()
    val navigateToAddSchoolYear: LiveData<Boolean>
        get() = _navigateToAddSchoolYear
    private val _navigateToDetailSchoolYear = MutableLiveData<Long>()
    val navigateToDetailSchoolYear: LiveData<Long>
        get() = _navigateToDetailSchoolYear
    private val _showSnackBarMessage = MutableLiveData<String?>()
    val showSnackBarMessage: LiveData<String?>
        get() = _showSnackBarMessage
    private var _oldPosition = 0
    val oldPosition: Int
        get() = _oldPosition
    private val _hideButton = MutableLiveData<Boolean>()
    val hideButton: LiveData<Boolean>
        get() = _hideButton

    /**
     * Start to navigate to [com.cosmejosenp.studentdiary.ui.AddSchoolYearFragment].
     *
     */
    fun onAddSchoolYear() {
        _navigateToAddSchoolYear.value = true
    }

    /**
     * Finish notification of navigation for prevent unwanted navigation.
     *
     */
    fun doneNavigatingToAddSchoolYear() {
        _navigateToAddSchoolYear.value = false
    }

    /**
     * Start to navigate to [com.cosmejosenp.studentdiary.ui.DetailSchoolYearFragment].
     *
     * @param idSchoolYear
     */
    fun startNavigateToDetailSchoolYear(idSchoolYear: Long) {
        _navigateToDetailSchoolYear.value = idSchoolYear
    }

    /**
     * Finish notification of navigation for prevent unwanted navigation.
     *
     */
    fun doneNavigatingToDetailSchoolYear() {
        _navigateToDetailSchoolYear.value = 0L
    }

    /**
     * Start show message on snackBar.
     *
     * @param message
     */
    fun startSnackBarMessage(message: String) {
        _showSnackBarMessage.value = message
    }

    /**
     * Finish notification of showing snackBar for prevent unwanted messages.
     *
     */
    fun showSnackBarDone() {
        _showSnackBarMessage.value = null
    }

    /**
     * Update the actual page position.
     *
     * @param position of the viewPager.
     */
    fun setLastPosition(position: Int) {
        _oldPosition = position
    }

    /**
     * Set the hide state of the fab button making necessary checks.
     *
     * @param hide status of fab button.
     */
    fun hideFabButton(hide: Boolean) {
        if (hide || _oldPosition == 0)
            _hideButton.value = hide
    }
}