/*
 * MIT License
 *
 * Copyright (c) 2022 Cosme José Nieto Pérez
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.cosmejosenp.studentdiary.viewmodels.pages

import android.app.Application
import android.view.View
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.cosmejosenp.studentdiary.R
import com.cosmejosenp.studentdiary.data.dao.EvaluationDao
import com.cosmejosenp.studentdiary.data.dao.SubSubjectDao
import com.cosmejosenp.studentdiary.data.entity.Evaluation
import com.cosmejosenp.studentdiary.util.PageListener
import kotlinx.coroutines.*

class PageEvaluationViewModel(
    private val evaluationDao: EvaluationDao,
    private val subSubjectDao: SubSubjectDao,
    private val schoolYear: Long,
    private val subject: Long,
    application: Application
) : AndroidViewModel(application) {
    private var viewModelJob = SupervisorJob()
    private val uiScope = CoroutineScope(Dispatchers.Main + viewModelJob)
    private val resources = application.resources
    val evaluations =
        if (schoolYear != 0L) evaluationDao.getAllFromSchoolYear(schoolYear)
        else evaluationDao.getAllFromSubject(subject)
    val messageDelete: String
        get() =
            if (schoolYear != 0L)
                resources.getString(R.string.deleteEvaluationExplanation)
            else
                resources.getString(R.string.deleteSubSubjectExplanationFromSubject)
    val activeItem =
        if (subject != 0L) evaluationDao.getActiveEvaluationFromSubject(subject)
        else MutableLiveData<Evaluation?>(null)
    private val _navigateToDetailEvaluation = MutableLiveData<Long>()
    val navigateToDetailEvaluation: LiveData<Long>
        get() = _navigateToDetailEvaluation
    private val _showSnackBarMessage = MutableLiveData<String?>()
    val showSnackBarMessage: LiveData<String?>
        get() = _showSnackBarMessage
    private val _actionModeActivated = MutableLiveData<Boolean>()
    val actionModeActivated: LiveData<Boolean>
        get() = _actionModeActivated
    private val _selectAllItems = MutableLiveData<Boolean>()
    val selectAllItems: LiveData<Boolean>
        get() = _selectAllItems
    private val _showDialogDelete = MutableLiveData<Boolean>()
    val showDialogDelete: LiveData<Boolean>
        get() = _showDialogDelete
    var selectedItems = ArrayList<Evaluation>()
    val emptyTextViewVisibility = Transformations.map(evaluations) {
        if (it.isEmpty()) {
            View.VISIBLE
        } else {
            View.GONE
        }
    }
    val messageEmptyList: String
        get() =
            if (schoolYear != 0L)
                resources.getString(R.string.emptyEvaluationsSchoolYear)
            else
                resources.getString(R.string.emptyEvaluationsSubject)
    private val _changeTitleActionMode = MutableLiveData(false)
    val changeTitleActionMode: LiveData<Boolean>
        get() = _changeTitleActionMode
    private var _colour = 0
    val colour
        get() = _colour
    val titleActionMode: Int
        get() = selectedItems.size
    private var _listener: PageListener? = null
    val listener: PageListener?
        get() = _listener

    /**
     * This method will be called when this ViewModel is no longer used and will be destroyed.
     */
    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }

    /**
     * Start to navigate to [com.cosmejosenp.studentdiary.ui.DetailEvaluationFragment].
     *
     * @param idEvaluation
     */
    fun startNavigateToDetailEvaluation(idEvaluation: Long) {
        _navigateToDetailEvaluation.value = idEvaluation
    }

    /**
     * Finish notification of navigation for prevent unwanted navigation.
     *
     */
    fun doneNavigatingToDetailEvaluation() {
        _navigateToDetailEvaluation.value = 0L
    }

    /**
     * Activate the action mode.
     *
     */
    fun activateActionMode() {
        _actionModeActivated.value = true
    }

    /**
     * Change the selected state of the [evaluation].
     *
     * @param evaluation to select or deselect.
     */
    fun toggleItemSelected(evaluation: Evaluation) {
        if (selectedItems.contains(evaluation)) {
            selectedItems.remove(evaluation)
            if (selectedItems.isEmpty()) {
                _actionModeActivated.value = false
            }
        } else {
            selectedItems.add(evaluation)
        }
        notifyChangeTitleActionMode()
    }

    /**
     * Change the selected state of all evaluations to selected.
     *
     */
    fun selectAll() {
        selectedItems.clear()
        for (evaluation: Evaluation in evaluations.value!!) {
            selectedItems.add(evaluation)
        }
        _selectAllItems.value = true
        notifyChangeTitleActionMode()
    }

    /**
     * Finish notification of selection for prevent unwanted behaviour.
     *
     */
    fun selectAllDone() {
        _selectAllItems.value = false
    }

    /**
     * Notify to change the title of the action mode when a new item is selected.
     *
     */
    fun notifyChangeTitleActionMode() {
        _changeTitleActionMode.value = true
    }

    /**
     * Finish notification of change the title for prevent unwanted behaviour.
     *
     */
    fun changeTitleActionModeDone() {
        _changeTitleActionMode.value = false
    }

    /**
     * Delete all selected evaluations or subjects
     * depending of the constructor parameters of this class.
     *
     */
    fun delete() {
        val selectedItemsOld = ArrayList<Evaluation>(selectedItems)
        _actionModeActivated.value = false
        if (schoolYear != 0L)
            deleteEvaluation(selectedItemsOld)
        else
            deleteSubSubject(selectedItemsOld)
    }

    /**
     * Delete all selected evaluations
     *
     * @param selectedItemsOld
     */
    private fun deleteEvaluation(selectedItemsOld: ArrayList<Evaluation>) {
        uiScope.launch {
            try {
                withContext(Dispatchers.IO) {
                    evaluationDao.deleteAndReorder(*selectedItemsOld.toTypedArray())
                }
            } catch (exception: Exception) {
                _showSnackBarMessage.value = resources.getString(R.string.errorDelete)
            }
        }
    }

    /**
     * Delete all selected linked subjects of this evaluation
     *
     * @param selectedItemsOld
     */
    private fun deleteSubSubject(selectedItemsOld: ArrayList<Evaluation>) {
        uiScope.launch {
            try {
                withContext(Dispatchers.IO) {
                    subSubjectDao.delete(subject, *selectedItemsOld.toTypedArray())
                }
            } catch (exception: Exception) {
                _showSnackBarMessage.value = resources.getString(R.string.errorDelete)
            }
        }
    }

    /**
     * Finish action mode and restore the default behaviour.
     *
     */
    fun actionModeDone() {
        selectedItems.clear()
        _actionModeActivated.value = false
    }

    /**
     * Finish notification of showing snackBar for prevent unwanted messages.
     *
     */
    fun showSnackBarDone() {
        _showSnackBarMessage.value = null
    }

    /**
     * Start show the confirm delete.
     *
     */
    fun startDialogDelete() {
        _showDialogDelete.value = true
    }

    /**
     * Finish notification of showing dialog for prevent unwanted behaviour.
     *
     */
    fun dialogDeleteDone() {
        _showDialogDelete.value = false
    }

    /**
     * Set the page listener if it's not set yet.
     *
     * @param newListener
     */
    fun setListener(newListener: PageListener) {
        if (_listener == null) {
            _listener = newListener
        }
    }

    /**
     * Set the status bar colour for restore colours when the action mode finish.
     *
     * @param statusBarColor
     */
    fun setColour(statusBarColor: Int) {
        _colour = statusBarColor
    }
}