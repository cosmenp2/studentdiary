/*
 * MIT License
 *
 * Copyright (c) 2022 Cosme José Nieto Pérez
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.cosmejosenp.studentdiary.viewmodels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.cosmejosenp.studentdiary.data.dao.EvaluationDao
import com.cosmejosenp.studentdiary.data.dao.SubSubjectDao
import com.cosmejosenp.studentdiary.data.dao.SubjectDao
import com.cosmejosenp.studentdiary.data.entity.SubSubject
import com.cosmejosenp.studentdiary.data.entity.Subject
import kotlinx.coroutines.*

class DetailSubjectViewModel(
    subjectDao: SubjectDao,
    evaluationDao: EvaluationDao,
    private val subSubjectDao: SubSubjectDao,
    private val idSubject: Long,
    idSchoolYear: Long,
    application: Application
) : AndroidViewModel(application) {
    private var viewModelJob = SupervisorJob()
    private val uiScope = CoroutineScope(Dispatchers.Main + viewModelJob)
    val subject = subjectDao.get(idSubject)
    val subSubjectForAddElements = subjectDao.getSubSubjectIdThatCanAddElements(idSubject)
    private val listAvailableEvaluations =
        evaluationDao.getAllPendingEvaluationsFromSubject(idSubject)
    private var _canExecuteFunction = false
    val toolbarTitle = Transformations.map(subject) {
        it.name
    }
    private val _navigateToAddSubSubjectFromSubject = MutableLiveData<Subject?>()
    val navigateToAddSubSubjectFromSubject: LiveData<Subject?>
        get() = _navigateToAddSubSubjectFromSubject
    private val _navigateToDetailSubSubject = MutableLiveData<SubSubject?>()
    val navigateToDetailSubSubject: LiveData<SubSubject?>
        get() = _navigateToDetailSubSubject
    private val _navigateToDetailElement = MutableLiveData<Long>()
    val navigateToDetailElement: LiveData<Long>
        get() = _navigateToDetailElement
    private val _navigateToAddElement = MutableLiveData<Long>()
    val navigateToAddElement: LiveData<Long>
        get() = _navigateToAddElement
    private val _showEditDialog = MutableLiveData(false)
    val showEditDialog: LiveData<Boolean>
        get() = _showEditDialog
    private val _showSnackBarMessage = MutableLiveData<String?>()
    val showSnackBarMessage: LiveData<String?>
        get() = _showSnackBarMessage
    val active = Transformations.map(listAvailableEvaluations) {
        it.isNotEmpty()
    }
    val activeSchoolYear = evaluationDao.isSchoolYearActive(idSchoolYear)
    private var _function: (() -> Unit)? = null
    val function
        get() = _function
    private var _oldPosition = 0
    val oldPosition: Int
        get() = _oldPosition
    private val _hideButton = MutableLiveData<Boolean>()
    val hideButton: LiveData<Boolean>
        get() = _hideButton

    /**
     * This method will be called when this ViewModel is no longer used and will be destroyed.
     */
    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }

    /**
     * Calculate the action of the fab button depending of the actual page.
     *
     */
    fun onAdd() {
        when (oldPosition) {
            0 -> startNavigateToAddElement()
            4 -> startNavigateToAddSubSubjectFromSubject()
        }
    }

    /**
     * Start to navigate to [com.cosmejosenp.studentdiary.ui.AddSubjectFragment].
     *
     */
    private fun startNavigateToAddSubSubjectFromSubject() {
        _navigateToAddSubSubjectFromSubject.value = subject.value
    }

    /**
     * Finish notification of navigation for prevent unwanted navigation.
     *
     */
    fun doneNavigatingToAddSubSubjectFromSubject() {
        _navigateToAddSubSubjectFromSubject.value = null
    }

    /**
     * Start to navigate to [com.cosmejosenp.studentdiary.ui.DetailSubSubjectFragment].
     *
     * @param idEvaluation
     */
    fun startNavigateToDetailSubSubject(idEvaluation: Long) {
        uiScope.launch {
            val subSubject = withContext(Dispatchers.IO) {
                return@withContext subSubjectDao.get(idEvaluation, idSubject)
            }
            _navigateToDetailSubSubject.value = subSubject
        }
    }

    /**
     * Finish notification of navigation for prevent unwanted navigation.
     *
     */
    fun doneNavigatingToDetailSubSubject() {
        _navigateToDetailSubSubject.value = null
    }

    /**
     * Start to navigate to [com.cosmejosenp.studentdiary.ui.AddElementFragment].
     *
     */
    private fun startNavigateToAddElement() {
        _navigateToAddElement.value = subSubjectForAddElements.value!!
    }

    /**
     * Finish notification of navigation for prevent unwanted navigation.
     *
     */
    fun doneNavigatingToAddElement() {
        _navigateToAddElement.value = 0L
    }

    /**
     * Start to navigate to [com.cosmejosenp.studentdiary.ui.DetailElementFragment].
     *
     * @param idElement
     */
    fun startNavigateToDetailElement(idElement: Long) {
        _navigateToDetailElement.value = idElement
    }

    /**
     * Finish notification of navigation for prevent unwanted navigation.
     *
     */
    fun doneNavigatingToDetailElement() {
        _navigateToDetailElement.value = 0L
    }

    /**
     * Start show the edit dialog.
     *
     */
    fun startShowEditDialog() {
        _showEditDialog.value = true
    }

    /**
     * Finish notification of showing dialog for prevent unwanted behaviour.
     *
     */
    fun doneShowEditDialog() {
        _showEditDialog.value = false
    }

    /**
     * Start show message on snackBar and check the if the undo action should be shown.
     *
     * @param message
     */
    fun startSnackBarMessage(message: String) {
        if (_canExecuteFunction)
            _canExecuteFunction = false
        else {
            _function = null
        }
        _showSnackBarMessage.value = message
    }

    /**
     * Finish notification of showing snackBar for prevent unwanted messages.
     *
     */
    fun showSnackBarDone() {
        _showSnackBarMessage.value = null
    }

    /**
     * Update the actual page position.
     *
     * @param position of the viewPager.
     */
    fun setLastPosition(position: Int) {
        _oldPosition = position
    }

    /**
     * Update the undo function to execute when the snackBar message is shown.
     *
     * @param function
     */
    fun updateFunction(function: (() -> Unit)?) {
        _function = function
        _canExecuteFunction = true
    }

    /**
     * Set to null [_function] to prevent repeated actions.
     *
     */
    fun doneUseFunction() {
        _function = null
    }

    /**
     * Set the hide state of the fab button making necessary checks.
     *
     * @param hide status of fab button.
     */
    fun hideFabButton(hide: Boolean) {
        if (activeSchoolYear.value != null && activeSchoolYear.value!!
            && (addSubSubjectFromSubject() || addElementCondition())
            || hide
        )
            _hideButton.value = hide
    }

    /**
     * Condition for show the fab button in the subSubject page.
     *
     */
    private fun addSubSubjectFromSubject() =
        active.value != null && active.value!! && oldPosition == 4

    /**
     * Condition for show the fab button in the element page.
     *
     */
    private fun addElementCondition() =
        oldPosition == 0 && subSubjectForAddElements.value != null
}
