/*
 * MIT License
 *
 * Copyright (c) 2022 Cosme José Nieto Pérez
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.cosmejosenp.studentdiary.viewmodels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.cosmejosenp.studentdiary.data.dao.BlockDao
import com.cosmejosenp.studentdiary.data.dao.SubBlockDao
import com.cosmejosenp.studentdiary.data.dao.SubjectDao
import com.cosmejosenp.studentdiary.util.TYPE_EXAM
import com.cosmejosenp.studentdiary.util.TYPE_HOMEWORK
import com.cosmejosenp.studentdiary.util.TYPE_WORK
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.launch

class DetailBlockViewModel(
    subjectDao: SubjectDao,
    blockDao: BlockDao,
    subBlockDao: SubBlockDao,
    val active: Boolean,
    idSubject: Long,
    private val idBlock: Long,
    application: Application
) : AndroidViewModel(application) {
    private var viewModelJob = SupervisorJob()
    private val uiScope = CoroutineScope(Dispatchers.Main + viewModelJob)
    val subject = subjectDao.get(idSubject)
    val block = blockDao.get(idBlock)
    val subBlocks = subBlockDao.getAllFromBlock(idBlock)
    private var _canExecuteFunction = false
    private val _navigateToAddSubBlock = MutableLiveData<Long>()
    val navigateToAddSubBlock: LiveData<Long>
        get() = _navigateToAddSubBlock
    private val _navigateToDetailSubBlock = MutableLiveData<Long>()
    val navigateToDetailSubBlock: LiveData<Long>
        get() = _navigateToDetailSubBlock
    private val _navigateToDetailElement = MutableLiveData<Long>()
    val navigateToDetailElement: LiveData<Long>
        get() = _navigateToDetailElement
    private val _navigateToAddElement = MutableLiveData<Long>()
    val navigateToAddElement: LiveData<Long>
        get() = _navigateToAddElement
    private val _showEditDialog = MutableLiveData(false)
    val showEditDialog: LiveData<Boolean>
        get() = _showEditDialog
    private val _showSnackBarMessage = MutableLiveData<String?>()
    val showSnackBarMessage: LiveData<String?>
        get() = _showSnackBarMessage
    val toolbarTitle = Transformations.map(block) {
        it.name
    }
    private var _viewPagerLoad = true
    val viewPageLoad: Boolean
        get() = _viewPagerLoad
    private var _function: (() -> Unit)? = null
    val function
        get() = _function
    private var _oldPosition = 0
    val oldPosition: Int
        get() = _oldPosition
    private val _hideButton = MutableLiveData<Boolean>()
    val hideButton: LiveData<Boolean>
        get() = _hideButton
    private val _isAddSubBlockAvailable = MutableLiveData(true)
    private val isAddSubBlockAvailable: LiveData<Boolean>
        get() = _isAddSubBlockAvailable

    /**
     * This method will be called when this ViewModel is no longer used and will be destroyed.
     */
    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }

    /**
     * Make checks to verify if this the fragment can add items.
     */
    fun evaluateData() {
        if (block.value != null && subBlocks.value != null) {
            var available = true
            if (block.value!!.type == null) {
                var examFound = false
                var workFound = false
                var homeworkFound = false
                val listSubBlocks = subBlocks.value!!
                var i = 0
                while (i < listSubBlocks.size && (!examFound || !workFound || !homeworkFound)) {
                    when (listSubBlocks[i].type) {
                        TYPE_EXAM -> examFound = true
                        TYPE_WORK -> workFound = true
                        TYPE_HOMEWORK -> homeworkFound = true
                    }
                    i++
                }
                if (examFound && workFound && homeworkFound)
                    available = false
            }
            _isAddSubBlockAvailable.value = available
            hideFabButton(true)
            hideFabButton(false)
        }
    }

    /**
     * Calculate the action of the fab button depending of the actual page.
     *
     */
    fun onAdd() {
        when (oldPosition) {
            0 -> startNavigateToAddElement()
            3 -> if (block.value!!.type == TYPE_HOMEWORK) startNavigateToAddSubBlock()
            4 -> startNavigateToAddSubBlock()
        }
    }

    /**
     * Start to navigate to [com.cosmejosenp.studentdiary.ui.AddBlockFragment].
     *
     */
    private fun startNavigateToAddSubBlock() {
        _navigateToAddSubBlock.value = idBlock
    }

    /**
     * Finish notification of navigation for prevent unwanted navigation.
     *
     */
    fun doneNavigatingToAddSubBlock() {
        _navigateToAddSubBlock.value = 0L
    }

    /**
     * Start to navigate to [com.cosmejosenp.studentdiary.ui.DetailSubBlockFragment].
     *
     * @param idBlock
     */
    fun startNavigateToDetailSubBlock(idBlock: Long) {
        uiScope.launch {
            _navigateToDetailSubBlock.value = idBlock
        }
    }

    /**
     * Finish notification of navigation for prevent unwanted navigation.
     *
     */
    fun doneNavigatingToDetailSubBlock() {
        _navigateToDetailSubBlock.value = 0L
    }

    /**
     * Start to navigate to [com.cosmejosenp.studentdiary.ui.AddElementFragment].
     *
     */
    private fun startNavigateToAddElement() {
        _navigateToAddElement.value = idBlock
    }

    /**
     * Finish notification of navigation for prevent unwanted navigation.
     *
     */
    fun doneNavigatingToAddElement() {
        _navigateToAddElement.value = 0L
    }

    /**
     * Start to navigate to [com.cosmejosenp.studentdiary.ui.DetailElementFragment].
     *
     * @param idElement
     */
    fun startNavigateToDetailElement(idElement: Long) {
        _navigateToDetailElement.value = idElement
    }

    /**
     * Finish notification of navigation for prevent unwanted navigation.
     *
     */
    fun doneNavigatingToDetailElement() {
        _navigateToDetailElement.value = 0L
    }

    /**
     * Start show the edit dialog.
     *
     */
    fun startShowEditDialog() {
        _showEditDialog.value = true
    }

    /**
     * Finish notification of showing dialog for prevent unwanted behaviour.
     *
     */
    fun doneShowEditDialog() {
        _showEditDialog.value = false
    }

    /**
     * Start show message on snackBar and check the if the undo action should be shown.
     *
     * @param message
     */
    fun startSnackBarMessage(message: String) {
        if (_canExecuteFunction) {
            _canExecuteFunction = false
        } else {
            _function = null
        }
        _showSnackBarMessage.value = message
    }

    /**
     * Finish notification of showing snackBar for prevent unwanted messages.
     *
     */
    fun showSnackBarDone() {
        _showSnackBarMessage.value = null
    }

    /**
     * Finish to load dynamic viewPager to prevent unwanted behaviour until a configuration change.
     *
     */
    fun doneLoadViewPager() {
        _viewPagerLoad = false
    }

    /**
     * Update the actual page position.
     *
     * @param position of the viewPager.
     */
    fun setLastPosition(position: Int) {
        _oldPosition = position
        hideFabButton(!active)
    }

    /**
     * Update the undo function to execute when the snackBar message is shown.
     *
     * @param function
     */
    fun updateFunction(function: (() -> Unit)?) {
        _function = function
        _canExecuteFunction = true
    }

    /**
     * Set to null [_function] to prevent repeated actions.
     *
     */
    fun doneUseFunction() {
        _function = null
    }

    /**
     * Set the hide state of the fab button making necessary checks.
     *
     * @param hide status of fab button.
     */
    fun hideFabButton(hide: Boolean) {
        if (active && (addSubBlockCondition() || addElementCondition()) || hide)
            _hideButton.value = hide
    }

    /**
     * Condition for show the fab button in the subBlock page.
     *
     */
    private fun addSubBlockCondition() =
        isAddSubBlockAvailable.value != null && isAddSubBlockAvailable.value!!
                && (oldPosition == 4 || oldPosition == 3 && block.value != null && block.value!!.type == TYPE_HOMEWORK)

    /**
     * Condition for show the fab button in the element page.
     *
     */
    private fun addElementCondition() =
        oldPosition == 0 && block.value != null
                && (
                block.value!!.type != null
                        || block.value!!.type == null && subBlocks.value != null && subBlocks.value!!.isNotEmpty()
                )
}