/*
 * MIT License
 *
 * Copyright (c) 2022 Cosme José Nieto Pérez
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.cosmejosenp.studentdiary.viewmodels

import android.app.Application
import android.view.View
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.cosmejosenp.studentdiary.R
import com.cosmejosenp.studentdiary.data.dao.BlockDao
import com.cosmejosenp.studentdiary.data.dao.SubBlockDao
import com.cosmejosenp.studentdiary.data.dao.SubjectDao
import com.cosmejosenp.studentdiary.data.entity.Block
import com.cosmejosenp.studentdiary.data.entity.SubBlock
import com.cosmejosenp.studentdiary.util.TYPE_EXAM
import com.cosmejosenp.studentdiary.util.TYPE_HOMEWORK
import com.cosmejosenp.studentdiary.util.TYPE_OTHER
import com.cosmejosenp.studentdiary.util.TYPE_WORK
import kotlinx.coroutines.*

class AddBlockViewModel(
    subjectDao: SubjectDao,
    private val blockDao: BlockDao,
    private val subBlockDao: SubBlockDao,
    idSubject: Long,
    private val idSubSubject: Long,
    private val idBlock: Long,
    application: Application
) : AndroidViewModel(application) {
    private val resources = application.resources
    private var viewModelJob = SupervisorJob()
    private val uiScope = CoroutineScope(Dispatchers.Main + viewModelJob)
    private var cacheSum: Int? = null
    val subject = subjectDao.get(idSubject)
    val blocks =
        if (idSubSubject != 0L) blockDao.getAllFromSubSubject(idSubSubject)
        else MutableLiveData<List<Block>>(null)
    val subBlocks =
        if (idBlock != 0L) subBlockDao.getAllFromBlock(idBlock)
        else MutableLiveData<List<SubBlock>>(null)
    val block =
        if (idBlock != 0L) blockDao.get(idBlock)
        else MutableLiveData<Block?>(null)
    private val _validData = MutableLiveData(false)
    val validData: LiveData<Boolean>
        get() = _validData
    private val _navigateToDetailBlock = MutableLiveData<Long>()
    val navigateToDetailBlock: LiveData<Long>
        get() = _navigateToDetailBlock
    private val _navigateToDetailSubBlock = MutableLiveData<Long>()
    val navigateToDetailSubBlock: LiveData<Long>
        get() = _navigateToDetailSubBlock
    private val _showSnackBarMessage = MutableLiveData<String?>()
    val showSnackBarMessage: LiveData<String?>
        get() = _showSnackBarMessage
    private val _showDialog = MutableLiveData(false)
    val showDialog: LiveData<Boolean>
        get() = _showDialog
    private val _notifyUIUpdate = MutableLiveData(false)
    val notifyUIUpdate: LiveData<Boolean>
        get() = _notifyUIUpdate
    private var _firstCheck = true
    val firstCheck: Boolean
        get() = _firstCheck
    private val _navigateToBackStack = MutableLiveData(false)
    val navigateToBackStack: LiveData<Boolean>
        get() = _navigateToBackStack
    private val _showErrorPercentages = MutableLiveData(false)
    val showErrorPercentages: LiveData<Boolean>
        get() = _showErrorPercentages
    private val _isExamAvailable = MutableLiveData(true)
    val isExamAvailable: LiveData<Boolean>
        get() = _isExamAvailable
    private val _isWorkAvailable = MutableLiveData(true)
    val isWorkAvailable: LiveData<Boolean>
        get() = _isWorkAvailable
    private val _isHomeworkAvailable = MutableLiveData(true)
    val isHomeworkAvailable: LiveData<Boolean>
        get() = _isHomeworkAvailable
    val isUnitAvailable = Transformations.map(block) {
        it == null || it.type != null
    }
    val examVisibility = Transformations.map(isExamAvailable) {
        if (it)
            View.VISIBLE
        else
            View.GONE
    }
    val workVisibility = Transformations.map(isWorkAvailable) {
        if (it)
            View.VISIBLE
        else
            View.GONE
    }
    val homeworkVisibility = Transformations.map(isHomeworkAvailable) {
        if (it)
            View.VISIBLE
        else
            View.GONE
    }
    val unitVisibility = Transformations.map(isUnitAvailable) {
        if (it)
            View.VISIBLE
        else
            View.GONE
    }
    val messageTextView = MutableLiveData(resources.getString(R.string.percentageSuggestion))

    /**
     * This method will be called when this ViewModel is no longer used and will be destroyed.
     */
    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }

    /**
     * Evaluate if the data are valid.
     *
     * @param title
     * @param percentage
     */
    fun evaluateData(
        title: String,
        percentage: String
    ) {
        var valid = title.trim().isNotEmpty()
        if (percentage.isNotEmpty()) {
            val numberValid = Integer.valueOf(percentage) in 0..100
            if (valid)
                valid = numberValid
            uiScope.launch {
                if (cacheSum == null)
                    if (blocks.value != null) {
                        cacheSum = 0
                        for (block in blocks.value!!) {
                            if (block.percent != null)
                                cacheSum = cacheSum!! + block.percent!!
                        }
                    } else if (subBlocks.value != null) {
                        cacheSum = 0
                        for (subBlock in subBlocks.value!!) {
                            if (subBlock.percent != null)
                                cacheSum = cacheSum!! + subBlock.percent!!
                        }
                    }
                if (cacheSum != null)
                    messageTextView.value = resources.getString(
                        R.string.percentageAdd,
                        cacheSum!! + Integer.valueOf(percentage)
                    )
            }
            _showErrorPercentages.value = !numberValid
        } else {
            messageTextView.value = resources.getString(R.string.percentageSuggestion)
        }
        _validData.value = valid
    }

    /**
     * Check the visibility of the user interface items.
     *
     */
    fun checkVisibility() {
        var examFound = false
        var workFound = false
        var homeworkFound = false
        var notify = false
        fun checkType(type: Int?) {
            when (type) {
                TYPE_EXAM -> examFound = true
                TYPE_WORK -> workFound = true
                TYPE_HOMEWORK -> homeworkFound = true
            }
        }

        if (idSubSubject != 0L && blocks.value != null) {
            val listBlocks = blocks.value!!
            var i = 0
            while (i < listBlocks.size && (!examFound || !workFound || !homeworkFound)) {
                checkType(listBlocks[i].type)
                i++
            }
            notify = true
        } else if (idBlock != 0L && block.value != null && subBlocks.value != null) {
            if (block.value!!.type != null) {
                examFound = true
                workFound = true
                homeworkFound = true
            } else {
                val listSubBlocks = subBlocks.value!!
                var i = 0
                while (i < listSubBlocks.size && (!examFound || !workFound || !homeworkFound)) {
                    checkType(listSubBlocks[i].type)
                    i++
                }
            }
            notify = true
        }
        _isExamAvailable.value = !examFound
        _isWorkAvailable.value = !workFound
        _isHomeworkAvailable.value = !homeworkFound
        _notifyUIUpdate.value = notify
    }

    /**
     * Finish notification of ui update for prevent unwanted navigation.
     *
     */
    fun doneNotifyUIUpdate() {
        _notifyUIUpdate.value = false
    }

    /**
     * Finish notification of navigation for prevent unwanted navigation.
     *
     */
    fun doneNavigatingToDetailBlock() {
        _navigateToDetailBlock.value = 0L
    }

    /**
     * Finish first check for prevent unwanted behaviour.
     *
     */
    fun doneFirstCheck() {
        _firstCheck = false
    }

    /**
     * Finish notification of navigation for prevent unwanted navigation.
     *
     */
    fun doneNavigatingToDetailSubBlock() {
        _navigateToDetailSubBlock.value = 0L
    }

    /**
     * Start to navigate to back stack.
     *
     */
    fun startNavigateToBackStack() {
        _navigateToBackStack.value = true
    }

    /**
     * Finish notification of navigation for prevent unwanted navigation.
     *
     */
    fun doneNavigatingToBackStack() {
        _navigateToBackStack.value = false
    }

    /**
     * Start show the confirm dialog.
     *
     */
    fun startShowDialog() {
        _showDialog.value = true
    }

    /**
     * Finish notification of showing dialog for prevent unwanted behaviour.
     *
     */
    fun showDialogDone() {
        _showDialog.value = false
    }

    /**
     * Finish notification of showing snackBar for prevent unwanted messages.
     *
     */
    fun showSnackBarMessageDone() {
        _showSnackBarMessage.value = null
    }

    /**
     * Save the new block, or subBlock depending of constructor parameters, with this information.
     *
     * @param name of the block.
     * @param examType if this block is [TYPE_EXAM].
     * @param workType if this block is [TYPE_WORK].
     * @param homeworkType if this block is [TYPE_HOMEWORK].
     * @param unitType if this block don't have any type specified.
     * @param percentageString the percentage string.
     */
    fun save(
        name: String,
        examType: Boolean,
        workType: Boolean,
        homeworkType: Boolean,
        unitType: Boolean,
        percentageString: String
    ) {
        val percentage =
            if (percentageString.trim().isNotEmpty())
                Integer.valueOf(percentageString)
            else null
        val type = when {
            examType -> TYPE_EXAM
            workType -> TYPE_WORK
            homeworkType -> TYPE_HOMEWORK
            unitType -> null
            else -> TYPE_OTHER
        }
        if (idSubSubject != 0L) {
            saveBlock(name.trim(), percentage, type)
        } else {
            saveSubBlock(name.trim(), percentage, type)
        }
    }

    /**
     * Save new block.
     *
     * @param name of the block.
     * @param percentage
     * @param type of the block.
     */
    private fun saveBlock(name: String, percentage: Int?, type: Int?) {
        uiScope.launch {
            try {
                _navigateToDetailBlock.value = withContext(Dispatchers.IO) {
                    val block = Block(idSubSubject, name, percent = percentage, type = type)
                    return@withContext blockDao.add(block)[0]
                }
            } catch (exception: Exception) {
                _showSnackBarMessage.value = resources.getString(R.string.errorUpdate)
            }
        }
    }

    /**
     * Save new SubBlock.
     *
     * @param name of the subBlock.
     * @param percentage
     * @param type of the subBlock.
     */
    private fun saveSubBlock(name: String, percentage: Int?, type: Int?) {
        uiScope.launch {
            try {
                _navigateToDetailSubBlock.value = withContext(Dispatchers.IO) {
                    val subBlock = SubBlock(idBlock, name, percent = percentage, type = type)
                    return@withContext subBlockDao.add(subBlock)[0]
                }
            } catch (exception: Exception) {
                _showSnackBarMessage.value = resources.getString(R.string.errorUpdate)
            }
        }
    }
}

