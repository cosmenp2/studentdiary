/*
 * MIT License
 *
 * Copyright (c) 2022 Cosme José Nieto Pérez
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */



package com.cosmejosenp.studentdiary.data.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.cosmejosenp.studentdiary.data.entity.Evaluation
import com.cosmejosenp.studentdiary.data.entity.SchoolYear
import com.cosmejosenp.studentdiary.data.entity.SchoolYearWithEvaluations
import com.cosmejosenp.studentdiary.data.entity.SchoolYearWithSubjects
import java.util.*
import kotlin.collections.ArrayList

@Dao
abstract class SchoolYearDao {
    /**
     * @param id of the schoolYear.
     * @return an observable schoolYear.
     */
    @Query("SELECT * FROM SCHOOL_YEARS WHERE id=:id")
    abstract fun get(id: Long): LiveData<SchoolYear>

    /**
     * @param id of the schoolYear.
     * @return an observable schoolYear with their subjects.
     */
    @Transaction
    @Query("SELECT * FROM SCHOOL_YEARS WHERE id=:id")
    abstract fun getWithSubjects(id: Long): LiveData<SchoolYearWithSubjects>

    /**
     * @param id of the schoolYear.
     * @return an observable schoolYear with their evaluations.
     */
    @Transaction
    @Query("SELECT * FROM SCHOOL_YEARS WHERE id=:id")
    abstract fun getWithEvaluations(id: Long): LiveData<SchoolYearWithEvaluations>

    /**
     * @return an observable schoolYears list.
     */
    @Query("SELECT * FROM SCHOOL_YEARS")
    abstract fun getAll(): LiveData<List<SchoolYear>>

    /**
     * @param active If schoolYear is active.
     * @return an observable schoolYears list.
     */
    @Query("SELECT * FROM SCHOOL_YEARS WHERE active=:active")
    abstract fun getActive(active: Boolean = true): LiveData<List<SchoolYear>>

    /**
     * @param active if schoolYear is active.
     * @return an observable schoolYears list with their subjects.
     */
    @Transaction
    @Query("SELECT * FROM SCHOOL_YEARS WHERE active=:active")
    abstract fun getActiveWithSubjects(active: Boolean = true): LiveData<List<SchoolYearWithSubjects>>

    /**
     * @param schoolYear to add.
     */
    @Insert
    abstract fun add(vararg schoolYear: SchoolYear): List<Long>

    /**
     * @param evaluation to add.
     */
    @Insert
    abstract fun addEvaluations(vararg evaluation: Evaluation)

    /**
     * @param schoolYear to update.
     */
    @Update
    abstract fun update(vararg schoolYear: SchoolYear)

    /**
     * @param evaluation to update.
     */
    @Update
    protected abstract fun updateEvaluations(vararg evaluation: Evaluation)

    /**
     * @param schoolYear to delete.
     */
    @Delete
    abstract fun delete(vararg schoolYear: SchoolYear)

    /**
     * Finish the subjects of this evaluations.
     *
     * @param evaluations of this school year.
     */
    @Query("UPDATE SUB_SUBJECTS SET active=0 WHERE evaluation IN (:evaluations)")
    protected abstract fun finishSubjects(vararg evaluations: Long)

    /**
     * @param schoolYear id.
     * @return an evaluations list of this [schoolYear].
     */
    @Query("SELECT * FROM EVALUATIONS WHERE school_year IN (:schoolYear)")
    protected abstract fun obtainEvaluationsFromSchoolYear(vararg schoolYear: Long): List<Evaluation>

    /**
     * Add a schoolYear with their evaluations.
     *
     * @param schoolYear to add.
     * @param evaluations list.
     */
    @Transaction
    open fun addWithEvaluations(schoolYear: SchoolYear, evaluations: List<Evaluation>): Long {
        val id = add(schoolYear)[0]
        for (evaluation in evaluations)
            evaluation.schoolYear = id
        addEvaluations(*evaluations.toTypedArray())
        return id
    }

    /**
     * Mark this school year as completed and
     * make the necessary changes to evaluations and subjects.
     *
     * @param updatedSchoolYear to finish.
     */
    @Transaction
    open fun finishSchoolYear(vararg updatedSchoolYear: SchoolYear) {
        val idSchoolYears = ArrayList<Long>()
        val idEvaluations = ArrayList<Long>()
        for (schoolYear: SchoolYear in updatedSchoolYear) {
            schoolYear.active = false
            schoolYear.finishDate = Calendar.getInstance()
            idSchoolYears.add(schoolYear.id)
        }
        val evaluations = obtainEvaluationsFromSchoolYear(*idSchoolYears.toLongArray())
        if (evaluations.isNotEmpty()) {
            for (evaluation in evaluations) {
                if (evaluation.startDate == null) {
                    evaluation.startDate = Calendar.getInstance()
                }
                if (evaluation.finishDate == null) {
                    evaluation.finishDate = Calendar.getInstance()
                }
                idEvaluations.add(evaluation.id)
            }
            finishSubjects(*idEvaluations.toLongArray())
            updateEvaluations(*evaluations.toTypedArray())
        }
        update(*updatedSchoolYear)
    }

    /**
     * Mark this school year as active.
     *
     * @param schoolYears to activate.
     */
    @Transaction
    open fun markAsActiveSchoolYear(vararg schoolYears: SchoolYear) {
        for (schoolYear: SchoolYear in schoolYears) {
            schoolYear.finishDate = null
            schoolYear.active = true
        }
        update(*schoolYears)
    }
}