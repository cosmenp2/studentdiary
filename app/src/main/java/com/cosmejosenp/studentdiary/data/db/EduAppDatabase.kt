/*
 * MIT License
 *
 * Copyright (c) 2022 Cosme José Nieto Pérez
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */



package com.cosmejosenp.studentdiary.data.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.cosmejosenp.studentdiary.data.dao.*
import com.cosmejosenp.studentdiary.data.entity.*

@Database(
    entities = [
        Block::class,
        Element::class,
        Evaluation::class,
        Reminder::class,
        ReminderElement::class,
        Requirement::class,
        SchoolYear::class,
        SubBlock::class,
        Subject::class,
        SubjectSetting::class,
        SubSubject::class
    ],
    version = 1,
    exportSchema = false
)
@TypeConverters(DateConverter::class)
abstract class EduAppDatabase : RoomDatabase() {
    abstract val blockDao: BlockDao
    abstract val elementDao: ElementDao
    abstract val evaluationDao: EvaluationDao
    abstract val reminderDao: ReminderDao
    abstract val requirementDao: RequirementDao
    abstract val schoolYearDao: SchoolYearDao
    abstract val subBlockDao: SubBlockDao
    abstract val subjectDao: SubjectDao
    abstract val subjectSettingDao: SubjectSettingDao
    abstract val subSubjectDao: SubSubjectDao

    companion object {

        @Volatile
        private var INSTANCE: EduAppDatabase? = null

        /**
         * @param context
         * @return an instance of EduAppDatabase with all DAOs.
         */
        fun getInstance(context: Context): EduAppDatabase {
            synchronized(this) {
                var instance = INSTANCE
                if (instance == null) {
                    instance = Room.databaseBuilder(
                        context.applicationContext,
                        EduAppDatabase::class.java,
                        "edu_app_database"
                    )
                        .build()
                    INSTANCE = instance
                }
                return instance
            }
        }
    }
}