/*
 * MIT License
 *
 * Copyright (c) 2022 Cosme José Nieto Pérez
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */



package com.cosmejosenp.studentdiary.data.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.cosmejosenp.studentdiary.data.entity.Evaluation
import java.util.*
import kotlin.collections.ArrayList

@Dao
abstract class EvaluationDao {
    /**
     * @param schoolYear id.
     * @return an observable evaluations list.
     */
    @Query("SELECT * FROM EVALUATIONS WHERE school_year=:schoolYear ORDER BY position ASC")
    abstract fun getAllFromSchoolYear(schoolYear: Long): LiveData<List<Evaluation>>

    /**
     * @param subject id.
     * @return an observable evaluations list.
     */
    @Query(
        """SELECT * FROM EVALUATIONS 
        WHERE id IN (SELECT evaluation FROM SUB_SUBJECTS WHERE subject=:subject) 
        ORDER BY position"""
    )
    abstract fun getAllFromSubject(subject: Long): LiveData<List<Evaluation>>

    /**
     * @param subject id.
     * @return an observable active evaluation.
     */
    @Query(
        """SELECT * FROM EVALUATIONS 
        WHERE id IN (SELECT evaluation FROM SUB_SUBJECTS 
        WHERE subject=:subject
        AND active = 1
        ) 
        ORDER BY position"""
    )
    abstract fun getActiveEvaluationFromSubject(
        subject: Long
    ): LiveData<Evaluation?>

    /**
     * @param evaluation id.
     * @return if the evaluation have active subSubjects.
     */
    @Query(
        """SELECT COUNT(*)!=0 FROM SUB_SUBJECTS
            WHERE evaluation = :evaluation
            AND active = 1"""
    )
    abstract fun haveActiveSubSubject(evaluation: Long): Boolean

    /**
     * @param subject id.
     * @param subSubject id to exclude that evaluation.
     * @return an observable evaluations list of this [subject]
     * without the excluded evaluation of this [subSubject].
     */
    @Query(
        """SELECT * FROM EVALUATIONS 
        WHERE id IN (SELECT evaluation FROM SUB_SUBJECTS 
        WHERE subject=:subject
        AND id != :subSubject
        ) 
        ORDER BY position"""
    )
    abstract fun getAllFromSubjectWithoutSubSubject(
        subject: Long,
        subSubject: Long
    ): LiveData<List<Evaluation>>

    /**
     * @param subject id.
     * @param subSubject id to exclude that evaluation.
     * @return an observable evaluations list of this [subject]
     * that have a valid structure, without the excluded
     * evaluation of this [subSubject].
     */
    @Query(
        """SELECT * FROM EVALUATIONS
        WHERE id IN (SELECT evaluation FROM SUB_SUBJECTS 
        WHERE subject=:subject 
        AND id != :subSubject
        AND id IN (SELECT DISTINCT sub_subject FROM BLOCKS)
        )
    """
    )
    abstract fun getOtherSubSubjectWithValidStructure(
        subject: Long,
        subSubject: Long
    ): LiveData<List<Evaluation>>

    /**
     * @param subject id.
     * @return an observable evaluations list that don't have any subSubject of this [subject].
     */
    @Query(
        """SELECT * FROM EVALUATIONS 
        WHERE finish_date IS NULL
        AND school_year= (SELECT school_year FROM SUBJECTS WHERE id=:subject)
        AND id NOT IN (SELECT evaluation FROM SUB_SUBJECTS WHERE subject=:subject)
        ORDER BY position"""
    )
    abstract fun getAllPendingEvaluationsFromSubject(
        subject: Long
    ): LiveData<List<Evaluation>>

    /**
     * @param schoolYear id.
     * @return how many evaluations have this [schoolYear].
     */
    @Query("SELECT COUNT(*) FROM EVALUATIONS WHERE school_year=:schoolYear")
    abstract fun countAllFromSchoolYear(schoolYear: Long): LiveData<Int>

    /**
     * @param schoolYear id.
     * @return if this [schoolYear] is active.
     * @see isSchoolYearActive for get an observable.
     */
    @Query("SELECT active FROM SCHOOL_YEARS WHERE id=:schoolYear")
    abstract fun isSchoolYearActiveOnly(schoolYear: Long): Boolean

    /**
     * @param schoolYear id.
     * @return an observable value of if this [schoolYear] is active.
     * @see isSchoolYearActiveOnly for get only the value.
     */
    @Query("SELECT active FROM SCHOOL_YEARS WHERE id=:schoolYear")
    abstract fun isSchoolYearActive(schoolYear: Long): LiveData<Boolean>

    /**
     * @param schoolYear id.
     * @return an evaluations list of this [schoolYear].
     */
    @Query("SELECT * FROM EVALUATIONS WHERE school_year=:schoolYear ORDER BY position ASC")
    protected abstract fun getEvaluationsFromSchoolYear(schoolYear: Long): List<Evaluation>

    /**
     * @param schoolYear id.
     * @param position to start.
     * @return a previous evaluations list of this [position] from this [schoolYear].
     */
    @Query("SELECT * FROM EVALUATIONS WHERE school_year=:schoolYear AND position < :position")
    protected abstract fun getPreviousEvaluations(
        schoolYear: Long,
        position: Int
    ): List<Evaluation>

    /**
     * @param schoolYear id.
     * @param position to start.
     * @return a next evaluations list of this [position] from this [schoolYear].
     */
    @Query("SELECT * FROM EVALUATIONS WHERE school_year=:schoolYear AND position > :position")
    protected abstract fun getNextEvaluations(
        schoolYear: Long,
        position: Int
    ): List<Evaluation>

    /**
     * @param id of evaluation.
     * @return an observable evaluation.
     * @see getOnly for get only the evaluation value.
     */
    @Query("SELECT * FROM EVALUATIONS WHERE id=:id")
    abstract fun get(id: Long): LiveData<Evaluation>

    /**
     * @param id of evaluation.
     * @return an evaluation.
     * @see get for get an observable evaluation value.
     */
    @Query("SELECT * FROM EVALUATIONS WHERE id=:id")
    abstract fun getOnly(id: Long): Evaluation

    /**
     * @param schoolYear id.
     * @param position to retrieve.
     * @return a nullable evaluation from this [schoolYear] with this [position].
     */
    @Query("SELECT * FROM EVALUATIONS WHERE school_year=:schoolYear AND position=:position")
    abstract fun getFromPositionAndSchoolYear(schoolYear: Long, position: Int): Evaluation?

    /**
     * Update the subSubjects state when an evaluation finish.
     *
     * @param evaluations ids.
     */
    @Query("UPDATE SUB_SUBJECTS SET active=0 WHERE evaluation IN (:evaluations)")
    protected abstract fun finishSubjects(vararg evaluations: Long)

    /**
     * Update this [schoolYear] state if it don't have more active evaluation.
     *
     * @param schoolYear id.
     * @param finishDate default now.
     */
    @Query("UPDATE SCHOOL_YEARS SET active=0, finish_date=:finishDate WHERE id =:schoolYear")
    protected abstract fun finishSchoolYear(
        schoolYear: Long,
        finishDate: Calendar = Calendar.getInstance()
    )

    /**
     * Update this [schoolYear] state if it have an active evaluation.
     *
     * @param schoolYear id.
     */
    @Query("UPDATE SCHOOL_YEARS SET active=1, finish_date=null WHERE id =:schoolYear")
    protected abstract fun activeSchoolYear(
        schoolYear: Long
    )

    /**
     * @param schoolYear id.
     * @return a list of ids of subject that don't have any subSubject active.
     */
    @Query(
        """SELECT id FROM SUBJECTS
        WHERE school_year = :schoolYear
        AND id NOT IN (SELECT DISTINCT subject FROM SUB_SUBJECTS WHERE active = 1)"""
    )
    protected abstract fun getSubjectsInactive(schoolYear: Long): List<Long>

    /**
     * Active subSubject of this [evaluation] and [subjects] list.
     *
     * @param evaluation id.
     * @param subjects to active.
     */
    @Query(
        """UPDATE SUB_SUBJECTS SET active=1 
        WHERE evaluation=:evaluation 
        AND subject IN (:subjects)
    """
    )
    protected abstract fun activeSubSubject(evaluation: Long, vararg subjects: Long)

    /**
     * @param evaluation to add.
     */
    @Insert
    abstract fun add(vararg evaluation: Evaluation): List<Long>

    /**
     * @param evaluation to update.
     */
    @Update
    abstract fun update(vararg evaluation: Evaluation)

    /**
     * @param evaluation to delete.
     */
    @Delete
    protected abstract fun delete(vararg evaluation: Evaluation)

    /**
     * Delete and reorder the evaluations.
     *
     * @param evaluation to delete.
     */
    @Transaction
    open fun deleteAndReorder(vararg evaluation: Evaluation) {
        delete(*evaluation)
        reorder(evaluation[0].schoolYear)
    }

    /**
     * Reorder the evaluations of this [schoolYear].
     *
     * @param schoolYear for reorder evaluations.
     */
    protected open fun reorder(schoolYear: Long) {
        var evaluations = getEvaluationsFromSchoolYear(schoolYear)
        evaluations = evaluations.sortedByDescending { it.id }
        evaluations = evaluations.sortedBy { it.position }
        for (i in 1..evaluations.size) {
            evaluations[i - 1].position = i
        }
        update(*evaluations.toTypedArray())
    }

    /**
     * Add a new evaluation and reorder evaluations.
     *
     * @param evaluation to add.
     */
    @Transaction
    open fun addAndReorder(evaluation: Evaluation): Long {
        val id = add(evaluation)[0]
        reorder(evaluation.schoolYear)
        return id
    }

    /**
     * Mark an evaluation as finished and make the necessary changes and checks.
     *
     * @param evaluation to mark as complete.
     */
    @Transaction
    open fun markAsFinished(evaluation: Evaluation) {
        val nextEvaluation =
            getFromPositionAndSchoolYear(evaluation.schoolYear, evaluation.position + 1)
        val previousEvaluations =
            getPreviousEvaluations(evaluation.schoolYear, evaluation.position)
        val idEvaluations = ArrayList<Long>()

        if (evaluation.startDate == null)
            evaluation.startDate = Calendar.getInstance()
        evaluation.finishDate = Calendar.getInstance()
        idEvaluations.add(evaluation.id)
        for (prevEvaluation in previousEvaluations) {
            if (prevEvaluation.startDate == null)
                prevEvaluation.startDate = Calendar.getInstance()
            if (prevEvaluation.finishDate == null) {
                prevEvaluation.finishDate = Calendar.getInstance()
            }
            idEvaluations.add(prevEvaluation.id)
        }
        finishSubjects(*idEvaluations.toLongArray())
        update(evaluation, *previousEvaluations.toTypedArray())
        if (nextEvaluation != null) {
            if (nextEvaluation.startDate == null) {
                nextEvaluation.startDate = Calendar.getInstance()
                update(nextEvaluation)
                activeNextSubSubject(nextEvaluation)
            }
        } else {
            finishSchoolYear(evaluation.schoolYear)
        }
    }

    /**
     * Active subSubjects of the [nextEvaluation].
     *
     * @param nextEvaluation
     */
    protected open fun activeNextSubSubject(nextEvaluation: Evaluation) {
        val list = getSubjectsInactive(nextEvaluation.schoolYear)
        activeSubSubject(nextEvaluation.id, *list.toLongArray())
    }

    /**
     * Mark an evaluation as active and make the necessary changes and checks
     *
     * @param evaluation to mark as active
     */
    @Transaction
    open fun markAsActive(evaluation: Evaluation) {
        val nextEvaluations = getNextEvaluations(evaluation.schoolYear, evaluation.position)
        evaluation.finishDate = null
        for (nextEvaluation in nextEvaluations)
            nextEvaluation.finishDate = null
        update(evaluation, *nextEvaluations.toTypedArray())
        activeSchoolYear(evaluation.schoolYear)
    }
}