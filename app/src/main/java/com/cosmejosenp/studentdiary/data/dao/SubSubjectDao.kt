/*
 * MIT License
 *
 * Copyright (c) 2022 Cosme José Nieto Pérez
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */



package com.cosmejosenp.studentdiary.data.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.cosmejosenp.studentdiary.data.entity.Evaluation
import com.cosmejosenp.studentdiary.data.entity.SubSubject
import com.cosmejosenp.studentdiary.data.entity.Subject
import com.cosmejosenp.studentdiary.util.TYPE_EXAM
import com.cosmejosenp.studentdiary.util.TYPE_HOMEWORK
import com.cosmejosenp.studentdiary.util.TYPE_WORK
import java.util.*
import kotlin.collections.ArrayList

@Dao
abstract class SubSubjectDao {
    /**
     * @param subSubject id.
     * @return an observable subSubject.
     * @see getOnly for get only the value.
     */
    @Query("SELECT * FROM SUB_SUBJECTS WHERE id=:subSubject")
    abstract fun get(subSubject: Long): LiveData<SubSubject>

    /**
     * @param subSubject id.
     * @return a subSubject.
     * @see get for get an observable.
     */
    @Query("SELECT * FROM SUB_SUBJECTS WHERE id=:subSubject")
    abstract fun getOnly(subSubject: Long): SubSubject

    /**
     * @param subject id.
     * @return the active subSubject of this [subject].
     */
    @Query("SELECT * FROM SUB_SUBJECTS WHERE subject=:subject AND active = 1")
    abstract fun getActiveFromSubject(subject: Long): SubSubject?

    /**
     * @param subject id.
     * @param subSubject id for evaluate if this subSubject is valid.
     * @return an observable boolean value that return if exists other subSubjects of this [subject] and
     * if this [subSubject] have a valid structure.
     */
    @Query(
        """SELECT COUNT(*)>1 FROM SUB_SUBJECTS 
            WHERE subject = :subject
            AND :subSubject IN (SELECT DISTINCT sub_subject FROM BLOCKS)"""
    )
    abstract fun existOtherSubSubjectAndSubSubjectIsValid(
        subject: Long,
        subSubject: Long
    ): LiveData<Boolean>

    /**
     * @param subSubject id.
     * @param types of the valid blocks or subBlocks.
     * @return an observable boolean value that return if the subSubject can add elements.
     */
    @Query(
        """SELECT COUNT(*)>0 FROM BLOCKS 
            WHERE sub_subject = :subSubject
            AND (type IS NOT NULL AND type IN (:types)
            OR id IN (SELECT block FROM SUB_BLOCKS WHERE type IS NOT NULL AND type IN (:types)))"""
    )
    abstract fun subSubjectCanAddElements(
        subSubject: Long,
        types: List<Int> = listOf(TYPE_EXAM, TYPE_WORK, TYPE_HOMEWORK)
    ): LiveData<Boolean>

    /**
     * @param subject id for get other subSubjects.
     * @param subSubject id to exclude.
     * @return an observable boolean value that return if exists other subSubjects that
     * have a valid structure.
     */
    @Query(
        """SELECT COUNT(*)!=0 FROM SUB_SUBJECTS
        WHERE subject = :subject
        AND 0 < (SELECT COUNT(*) FROM SUB_SUBJECTS 
        WHERE subject=:subject 
        AND id != :subSubject
        AND id IN (SELECT DISTINCT sub_subject FROM BLOCKS)
        )
    """
    )
    abstract fun existOtherSubSubjectWithValidStructure(
        subject: Long,
        subSubject: Long
    ): LiveData<Boolean>

    /**
     * @param evaluation id of the subSubject.
     * @param subject id of the subSubject.
     * @return a subSubject of this [subject] and [evaluation].
     */
    @Query("SELECT * FROM SUB_SUBJECTS WHERE subject=:subject AND evaluation=:evaluation")
    abstract fun get(evaluation: Long, subject: Long): SubSubject

    /**
     * @param subject id.
     * @return an observable subSubject list of this [subject].
     */
    @Query("SELECT * FROM SUB_SUBJECTS WHERE subject=:subject")
    abstract fun getAllFromSubject(subject: Long): LiveData<List<SubSubject>>

    /**
     * @param evaluation id.
     * @return an observable subSubject list of this [evaluation].
     */
    @Query("SELECT * FROM SUB_SUBJECTS WHERE evaluation=:evaluation")
    abstract fun getAllFromEvaluation(evaluation: Long): LiveData<List<SubSubject>>

    /**
     * @param subject id.
     * @return an evaluation list of this [subject].
     */
    @Query(
        """SELECT * FROM EVALUATIONS 
            WHERE id IN (SELECT evaluation FROM SUB_SUBJECTS WHERE subject=:subject)
            ORDER BY position"""
    )
    protected abstract fun getEvaluationsFromSubject(subject: Long): List<Evaluation>

    /**
     * Update subSubject state of this [subject] to inactive.
     *
     * @param subject id.
     */
    @Query(
        """UPDATE SUB_SUBJECTS SET active=0 WHERE subject=:subject"""
    )
    protected abstract fun resetActive(subject: Long)

    /**
     * @param subSubject to add.
     */
    @Insert
    abstract fun add(vararg subSubject: SubSubject): List<Long>

    /**
     * @param subSubject to update.
     */
    @Update
    abstract fun update(vararg subSubject: SubSubject)

    /**
     * @param evaluations to update.
     */
    @Update
    protected abstract fun update(vararg evaluations: Evaluation)

    /**
     * @param subSubject to delete.
     */
    @Delete
    abstract fun delete(vararg subSubject: SubSubject)

    /**
     * Delete the subSubject of this [evaluation] and this [subjects].
     *
     * @param evaluation id.
     * @param subjects
     */
    open fun delete(evaluation: Long, vararg subjects: Subject) {
        val subSubjects = ArrayList<SubSubject>()
        for (subject in subjects) {
            subSubjects.add(get(evaluation, subject.id))
        }
        delete(*subSubjects.toTypedArray())
    }

    /**
     * Delete the subSubject of this [subject] and this [evaluations].
     *
     * @param subject id.
     * @param evaluations
     */
    open fun delete(subject: Long, vararg evaluations: Evaluation) {
        val subSubjects = ArrayList<SubSubject>()
        for (evaluation in evaluations) {
            subSubjects.add(get(evaluation.id, subject))
        }
        delete(*subSubjects.toTypedArray())
    }

    /**
     * Mark the [subSubject] as complete and active next.
     *
     * @param subSubject to complete.
     */
    @Transaction
    open fun finishAndActiveNext(subSubject: SubSubject) {
        val evaluations = getEvaluationsFromSubject(subSubject.subject)
        var evaluationToActive: Evaluation? = null
        var found = false
        var i = 0
        while (i < evaluations.size && !found) {
            if (evaluations[i].id == subSubject.evaluation) {
                if (i + 1 < evaluations.size)
                    evaluationToActive = evaluations[i + 1]
                found = true
            }
            i++
        }
        subSubject.active = false
        update(subSubject)
        evaluationToActive?.let {
            if (it.startDate == null) {
                it.startDate = Calendar.getInstance()
                update(it)
            }
            active(get(it.id, subSubject.subject))
        }
    }

    /**
     * Mark as active this subSubject.
     *
     * @param subSubject to active.
     */
    @Transaction
    open fun active(subSubject: SubSubject) {
        resetActive(subSubject.subject)
        subSubject.active = true
        update(subSubject)
    }
}