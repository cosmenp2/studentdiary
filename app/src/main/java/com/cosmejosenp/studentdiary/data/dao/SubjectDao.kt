/*
 * MIT License
 *
 * Copyright (c) 2022 Cosme José Nieto Pérez
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */



package com.cosmejosenp.studentdiary.data.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.cosmejosenp.studentdiary.data.entity.Subject
import com.cosmejosenp.studentdiary.data.entity.SubjectWithSettings
import com.cosmejosenp.studentdiary.data.entity.SubjectWithSubSubject
import com.cosmejosenp.studentdiary.util.TYPE_EXAM
import com.cosmejosenp.studentdiary.util.TYPE_HOMEWORK
import com.cosmejosenp.studentdiary.util.TYPE_WORK

@Dao
abstract class SubjectDao {
    /**
     * @param schoolYear id.
     * @return an observable subjects list ordered by name.
     */
    @Query("SELECT * FROM SUBJECTS WHERE school_year=:schoolYear ORDER BY name")
    abstract fun getAllFromSchoolYear(schoolYear: Long): LiveData<List<Subject>>

    /**
     * @param evaluation id.
     * @return an observable subjects list ordered by name.
     */
    @Query(
        """SELECT * FROM SUBJECTS 
        WHERE id IN (SELECT subject FROM SUB_SUBJECTS WHERE evaluation=:evaluation) 
        ORDER BY name"""
    )
    abstract fun getAllFromEvaluation(evaluation: Long): LiveData<List<Subject>>

    /**
     * @param evaluation id.
     * @return an observable active subjects list ordered by name.
     */
    @Query(
        """SELECT * FROM SUBJECTS 
        WHERE id IN (SELECT subject FROM SUB_SUBJECTS 
        WHERE evaluation=:evaluation
        AND active = 1
        ) 
        ORDER BY name"""
    )
    abstract fun getAllActiveFromEvaluation(evaluation: Long): LiveData<List<Subject>>

    /**
     * @param evaluation id.
     * @return an observable subjects list ordered by name
     * that don't have any subSubject in this [evaluation].
     */
    @Query(
        """SELECT * FROM SUBJECTS 
        WHERE school_year= (SELECT school_year FROM EVALUATIONS WHERE id=:evaluation)
        AND id NOT IN (SELECT subject FROM SUB_SUBJECTS WHERE evaluation=:evaluation)
        ORDER BY name"""
    )
    abstract fun getAllPendingFromEvaluation(
        evaluation: Long
    ): LiveData<List<Subject>>

    /**
     * @param types of the valid blocks or subBlocks.
     * @return an observable subjects list that can add elements because
     * they have an active subSubject with a valid structure.
     */
    @Query(
        """SELECT * FROM SUBJECTS
             WHERE id IN (SELECT DISTINCT s.subject FROM SUB_SUBJECTS as s INNER JOIN BLOCKS as b
             ON s.id = b.sub_subject
             WHERE s.active = 1
             AND (type IS NOT NULL AND type IN (:types)
             OR b.id IN (SELECT block FROM SUB_BLOCKS WHERE type IS NOT NULL AND type IN (:types))))"""
    )
    abstract fun getAllValidForAddElements(
        types: List<Int> = listOf(TYPE_EXAM, TYPE_WORK, TYPE_HOMEWORK)
    ): LiveData<List<Subject>>

    /**
     * @param types of the valid blocks or subBlocks.
     * @return if exist any subject that can add elements.
     */
    @Query(
        """SELECT COUNT(*)>0 FROM SUBJECTS
             WHERE id IN (SELECT DISTINCT s.subject FROM SUB_SUBJECTS as s INNER JOIN BLOCKS as b
             ON s.id = b.sub_subject
             WHERE s.active = 1
             AND (type IS NOT NULL AND type IN (:types)
             OR b.id IN (SELECT block FROM SUB_BLOCKS WHERE type IS NOT NULL AND type IN (:types))))"""
    )
    abstract fun getAnySubjectCanAddElements(
        types: List<Int> = listOf(TYPE_EXAM, TYPE_WORK, TYPE_HOMEWORK)
    ): LiveData<Boolean>

    /**
     * @param id of subject.
     * @return an observable subject.
     * @see getOnly for get only the value.
     */
    @Query("SELECT * FROM SUBJECTS WHERE id=:id")
    abstract fun get(id: Long): LiveData<Subject>

    /**
     * @param id of subject.
     * @return a nullable subject.
     * @see get for get an observable.
     */
    @Query("SELECT * FROM SUBJECTS WHERE id=:id")
    abstract fun getOnly(id: Long): Subject?

    /**
     * @param element id.
     * @return the subject of the [element].
     */
    @Query(
        """ SELECT * FROM SUBJECTS
            WHERE id = (SELECT subject FROM SUB_SUBJECTS as s INNER JOIN BLOCKS as b
            ON s.id = b.sub_subject
            INNER JOIN ELEMENTS as e
            ON b.id = e.block
            WHERE e.id=:element)"""
    )
    abstract fun getFromElement(element: Long): LiveData<Subject>

    /**
     * @param element id.
     * @return the colour of the subject of the [element].
     */
    @Query(
        """ SELECT colour FROM SUBJECTS
            WHERE id = (SELECT subject FROM SUB_SUBJECTS as s INNER JOIN BLOCKS as b
            ON s.id = b.sub_subject
            INNER JOIN ELEMENTS as e
            ON b.id = e.block
            WHERE e.id=:element)"""
    )
    abstract fun getTypeColourFromElement(element: Long): Int

    /**
     * @param subject id.
     * @param types of the valid blocks or subBlocks.
     * @return a ids list of subSubjects that can add elements.
     */
    @Query(
        """SELECT sub_subject FROM BLOCKS 
            WHERE sub_subject IN (SELECT id FROM SUB_SUBJECTS 
            WHERE subject=:subject 
            AND active = 1 )
            AND (type IS NOT NULL AND type IN (:types)
            OR id IN (SELECT block FROM SUB_BLOCKS WHERE type IS NOT NULL AND type IN (:types)))
            LIMIT 1"""
    )
    abstract fun getSubSubjectIdThatCanAddElements(
        subject: Long,
        types: List<Int> = listOf(TYPE_EXAM, TYPE_WORK, TYPE_HOMEWORK)
    ): LiveData<Long?>

    /**
     * @param subject id.
     * @return an observable subject with settings.
     */
    @Transaction
    @Query("SELECT * FROM SUBJECTS WHERE id=:subject")
    abstract fun getWithSettings(subject: Long): LiveData<SubjectWithSettings>

    /**
     * @param subject id.
     * @return an observable subject with subSubjects.
     */
    @Transaction
    @Query("SELECT * FROM SUBJECTS WHERE id=:subject")
    abstract fun getWithSubSubject(subject: Long): LiveData<SubjectWithSubSubject>

    /**
     * @param subject to add.
     */
    @Insert
    abstract fun add(vararg subject: Subject): List<Long>

    /**
     * @param subject to update.
     */
    @Update
    abstract fun update(vararg subject: Subject)

    /**
     * @param subject to delete.
     */
    @Delete
    abstract fun delete(vararg subject: Subject)
}