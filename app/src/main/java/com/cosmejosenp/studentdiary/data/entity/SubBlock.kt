/*
 * MIT License
 *
 * Copyright (c) 2022 Cosme José Nieto Pérez
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */



package com.cosmejosenp.studentdiary.data.entity

import androidx.annotation.NonNull
import androidx.room.*

@Entity(
    tableName = "SUB_BLOCKS",
    indices = [
        Index("block"),
        Index("repeat_sub_block")
    ],
    foreignKeys = [
        ForeignKey(
            entity = Block::class,
            parentColumns = ["id"],
            childColumns = ["block"],
            onUpdate = ForeignKey.CASCADE,
            onDelete = ForeignKey.CASCADE
        ),
        ForeignKey(
            entity = SubBlock::class,
            parentColumns = ["id"],
            childColumns = ["repeat_sub_block"],
            onUpdate = ForeignKey.CASCADE,
            onDelete = ForeignKey.SET_NULL
        )
    ]
)
data class SubBlock(
    @NonNull @ColumnInfo(name = "block") var block: Long,
    @NonNull var name: String,
    @PrimaryKey(autoGenerate = true) var id: Long = 0L,
    var percent: Int? = null,
    var type: Int? = null,
    var grade: Double? = null,
    @ColumnInfo(name = "repeat_sub_block") var repeatSubBlock: Long? = null,
    @NonNull @ColumnInfo(name = "mode_repeat") var modeRepeat: Boolean = false,
    @ColumnInfo(name = "grade_comment") var gradeComment: String? = null
) {
    override fun toString(): String {
        return name
    }
}