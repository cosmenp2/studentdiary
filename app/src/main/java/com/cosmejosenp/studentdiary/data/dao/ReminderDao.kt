/*
 * MIT License
 *
 * Copyright (c) 2022 Cosme José Nieto Pérez
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */



package com.cosmejosenp.studentdiary.data.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.cosmejosenp.studentdiary.data.entity.Reminder
import com.cosmejosenp.studentdiary.data.entity.ReminderElement
import com.cosmejosenp.studentdiary.data.entity.ReminderWithElements

@Dao
abstract class ReminderDao {
    /**
     * @param date of the reminder
     * @return an observable reminders with elements list of this [date]
     */
    @Transaction
    @Query("SELECT * FROM REMINDERS WHERE date=:date")
    abstract fun get(date: Long): LiveData<ReminderWithElements?>

    /**
     * @param reminder to add
     */
    @Insert
    abstract fun add(vararg reminder: Reminder): List<Long>

    /**
     * @param reminder to add
     */
    @Transaction
    @Insert
    abstract fun add(vararg reminder: ReminderElement): List<Long>

    /**
     * @param reminder to update
     */
    @Update
    abstract fun update(vararg reminder: Reminder)

    /**
     * @param reminder to update
     */
    @Transaction
    @Update
    abstract fun update(vararg reminder: ReminderElement)

    /**
     * @param reminder to delete
     */
    @Delete
    abstract fun delete(vararg reminder: Reminder)

    /**
     * @param reminder to delete
     */
    @Transaction
    @Delete
    abstract fun delete(vararg reminder: ReminderElement)
}