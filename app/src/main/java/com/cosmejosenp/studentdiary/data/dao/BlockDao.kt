/*
 * MIT License
 *
 * Copyright (c) 2022 Cosme José Nieto Pérez
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */



package com.cosmejosenp.studentdiary.data.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.cosmejosenp.studentdiary.data.entity.Block
import com.cosmejosenp.studentdiary.data.entity.BlockWithSubBlocks
import com.cosmejosenp.studentdiary.data.entity.SubBlock
import com.cosmejosenp.studentdiary.data.entity.SubSubject

@Dao
abstract class BlockDao {
    /**
     * @param block id.
     * @return an observable block.
     * @see getOnly for get only the value.
     */
    @Query("SELECT * FROM BLOCKS WHERE id=:block")
    abstract fun get(block: Long): LiveData<Block>

    /**
     * @param block id.
     * @return a nullable block.
     * @see get for get an observable.
     */
    @Query("SELECT * FROM BLOCKS WHERE id=:block")
    abstract fun getOnly(block: Long): Block?

    /**
     * @param subBlock id.
     * @return an nullable subBlock.
     */
    @Query("SELECT * FROM SUB_BLOCKS WHERE id=:subBlock")
    protected abstract fun getSubBlock(subBlock: Long): SubBlock?

    /**
     * @param subBlock id.
     * @return an observable block.
     */
    @Query("SELECT * FROM BLOCKS WHERE id=(SELECT block FROM SUB_BLOCKS WHERE id=:subBlock)")
    abstract fun getFromSubBlock(subBlock: Long): LiveData<Block>

    /**
     * @param subSubject id.
     * @return an observable blocks list ordered by name.
     * @see getAllFromSubSubjectList for get only the value.
     */
    @Query("SELECT * FROM BLOCKS WHERE sub_subject=:subSubject ORDER BY name")
    abstract fun getAllFromSubSubject(subSubject: Long): LiveData<List<Block>>

    /**
     * @param subSubject id.
     * @return a block list.
     * @see getAllFromSubSubject for get an observable.
     */
    @Query("SELECT * FROM BLOCKS WHERE sub_subject=:subSubject")
    abstract fun getAllFromSubSubjectList(subSubject: Long): List<Block>

    /**
     * @param block id.
     * @return a nullable block with subBlocks.
     */
    @Transaction
    @Query("SELECT * FROM BLOCKS WHERE id=:block")
    abstract fun getWithSubBlocksOnly(block: Long): BlockWithSubBlocks?

    /**
     * @param subSubject id.
     * @return an observable block with subBlocks list ordered by name.
     * @see getAllWithSubBlocksListFromSubSubject for get only the value.
     */
    @Transaction
    @Query("SELECT * FROM BLOCKS WHERE sub_subject=:subSubject ORDER BY name")
    abstract fun getAllWithSubBlocksFromSubSubject(subSubject: Long): LiveData<List<BlockWithSubBlocks>>

    /**
     * @param subSubject id.
     * @return a block with subBlocks list.
     * @see getAllWithSubBlocksFromSubSubject for get an observable.
     */
    @Transaction
    @Query("SELECT * FROM BLOCKS WHERE sub_subject=:subSubject")
    abstract fun getAllWithSubBlocksListFromSubSubject(subSubject: Long): List<BlockWithSubBlocks>

    /**
     * @param subject id.
     * @param position max position to search subSubject based in evaluation position and [subject].
     * Default 0.
     * @param extraordinary if that evaluation is extraordinary. If true position will be ignored.
     * Default false.
     * @return a block with subBlocks list of the subSubject that have a minor position than [position]
     * or whose evaluation is an [extraordinary] evaluation and have at least one block.
     */
    @Transaction
    @Query(
        """SELECT * FROM BLOCKS 
        WHERE sub_subject = (SELECT s.id FROM SUB_SUBJECTS as s INNER JOIN EVALUATIONS as e
         ON s.evaluation=e.id
        WHERE s.subject=:subject 
        AND ((e.extra_ordinary = 1 AND :extraordinary)
        OR (e.position < :position
        AND extra_ordinary = 0 AND NOT :extraordinary)
        AND s.id IN (SELECT DISTINCT sub_subject FROM BLOCKS))
        ORDER BY e.position DESC
        LIMIT 1)
    """
    )
    abstract fun getAllWithSubBlocksFromSubjectAndEvaluation(
        subject: Long,
        position: Int = 0,
        extraordinary: Boolean = false
    ): List<BlockWithSubBlocks>

    /**
     * @param block to add.
     */
    @Insert
    abstract fun add(vararg block: Block): List<Long>

    /**
     * @param subBlocks to add.
     */
    @Insert
    protected abstract fun add(vararg subBlocks: SubBlock): List<Long>

    /**
     * @param block to update.
     */
    @Update
    abstract fun update(vararg block: Block)

    /**
     * @param subBlocks to update.
     */
    @Update
    protected abstract fun update(vararg subBlocks: SubBlock)

    /**
     * @param block to delete.
     */
    @Delete
    abstract fun delete(vararg block: Block)

    /**
     * Add a block with subBlocks.
     *
     * @param blocksWithSubBlocks to add.
     */
    @Transaction
    open fun addWithSubBlocks(vararg blocksWithSubBlocks: BlockWithSubBlocks) {
        for (blockWithSubBlocks: BlockWithSubBlocks in blocksWithSubBlocks) {
            val id = add(blockWithSubBlocks.block)[0]
            for (subBlock in blockWithSubBlocks.subBlocks)
                subBlock.block = id
            if (blockWithSubBlocks.subBlocks.isNotEmpty())
                add(*blockWithSubBlocks.subBlocks.toTypedArray())
        }
    }

    /**
     * @param subSubjects to delete.
     */
    @Transaction
    open fun delete(vararg subSubjects: SubSubject) {
        for (subSubject in subSubjects) {
            delete(*getAllFromSubSubjectList(subSubject.id).toTypedArray())
        }
    }

    /**
     * Add new structure of blocks with subBlocks and update the older structure
     * depending if it already exists.
     *
     * @param blocksWithSubBlocks to add and update.
     */
    @Transaction
    open fun addNewsAndUpdateOlder(blocksWithSubBlocks: List<BlockWithSubBlocks>) {
        for (blockWithSubBlocks in blocksWithSubBlocks) {
            if (getOnly(blockWithSubBlocks.block.id) == null) {
                addWithSubBlocks(blockWithSubBlocks)
            } else {
                update(blockWithSubBlocks.block)
                for (subBock in blockWithSubBlocks.subBlocks) {
                    if (getSubBlock(subBock.id) == null)
                        add(subBock)
                    else
                        update(subBock)
                }
            }
        }
    }

}