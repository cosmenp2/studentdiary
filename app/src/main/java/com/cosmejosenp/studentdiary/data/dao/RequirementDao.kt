/*
 * MIT License
 *
 * Copyright (c) 2022 Cosme José Nieto Pérez
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */



package com.cosmejosenp.studentdiary.data.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.cosmejosenp.studentdiary.data.entity.Requirement

@Dao
abstract class RequirementDao {

    /**
     * @param requirement id.
     * @return an observable requirement.
     */
    @Query("SELECT * FROM REQUIREMENTS WHERE id=:requirement")
    abstract fun get(requirement: Long): LiveData<Requirement>

    /**
     * @param element id.
     * @return an observable requirement list of this [element].
     */
    @Query("SELECT * FROM REQUIREMENTS WHERE element=:element")
    abstract fun getAllFromElement(element: Long): LiveData<List<Requirement>>

    /**
     * @param element id.
     * @param completed if the requirement is completed.
     * @return an observable requirement list of this [element] with this [completed] state.
     */
    @Query("SELECT * FROM REQUIREMENTS WHERE element=:element AND completed=:completed")
    abstract fun getAllFromElement(element: Long, completed: Boolean): LiveData<List<Requirement>>

    /**
     * @param requirement to add.
     */
    @Insert
    abstract fun add(vararg requirement: Requirement): List<Long>

    /**
     * @param requirement to update.
     */
    @Update
    abstract fun update(vararg requirement: Requirement)

    /**
     * @param requirement to delete.
     */
    @Delete
    abstract fun delete(vararg requirement: Requirement)
}