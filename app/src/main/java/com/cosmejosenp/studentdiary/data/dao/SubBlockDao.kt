/*
 * MIT License
 *
 * Copyright (c) 2022 Cosme José Nieto Pérez
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */



package com.cosmejosenp.studentdiary.data.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.cosmejosenp.studentdiary.data.entity.SubBlock

@Dao
abstract class SubBlockDao {
    /**
     * @param subBlock id.
     * @return an observable subBlock.
     * @see getOnly for get only the value.
     */
    @Query("SELECT * FROM SUB_BLOCKS WHERE id=:subBlock")
    abstract fun get(subBlock: Long): LiveData<SubBlock>

    /**
     * @param subBlock id.
     * @return a nullable subBlock.
     * @see get for get an observable.
     */
    @Query("SELECT * FROM SUB_BLOCKS WHERE id=:subBlock")
    abstract fun getOnly(subBlock: Long): SubBlock

    /**
     * @param block id.
     * @return an observable subBlocks list ordered by name.
     * @see getAllFromBlockList for get only the subBlocks list.
     */
    @Query("SELECT * FROM SUB_BLOCKS WHERE block=:block ORDER BY name")
    abstract fun getAllFromBlock(block: Long): LiveData<List<SubBlock>>

    /**
     * @param block id.
     * @return a subBlocks list.
     * @see getAllFromBlock for get an observable.
     */
    @Query("SELECT * FROM SUB_BLOCKS WHERE block=:block")
    abstract fun getAllFromBlockList(block: Long): List<SubBlock>

    /**
     * @param subBlock to add.
     */
    @Insert
    abstract fun add(vararg subBlock: SubBlock): List<Long>

    /**
     * @param subBlock to update.
     */
    @Update
    abstract fun update(vararg subBlock: SubBlock)

    /**
     * @param subBlock to delete.
     */
    @Delete
    abstract fun delete(vararg subBlock: SubBlock)
}