/*
 * MIT License
 *
 * Copyright (c) 2022 Cosme José Nieto Pérez
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */



package com.cosmejosenp.studentdiary.data.entity

import androidx.annotation.NonNull
import androidx.room.*

@Entity(
    tableName = "SUBJECTS",
    indices = [
        Index("school_year"),
        Index("previous_subject"),
        Index("repeat_subject")
    ],
    foreignKeys = [
        ForeignKey(
            entity = SchoolYear::class,
            parentColumns = ["id"],
            childColumns = ["school_year"],
            onUpdate = ForeignKey.CASCADE,
            onDelete = ForeignKey.CASCADE
        ),
        ForeignKey(
            entity = Subject::class,
            parentColumns = ["id"],
            childColumns = ["previous_subject"],
            onUpdate = ForeignKey.CASCADE,
            onDelete = ForeignKey.SET_NULL
        ),
        ForeignKey(
            entity = Subject::class,
            parentColumns = ["id"],
            childColumns = ["repeat_subject"],
            onUpdate = ForeignKey.CASCADE,
            onDelete = ForeignKey.SET_NULL
        )
    ]
)
data class Subject(
    @NonNull @ColumnInfo(name = "school_year") var schoolYear: Long,
    @NonNull var name: String,
    @NonNull @ColumnInfo(name = "continuous_assessment") var continuousAssessment: Boolean = true,
    @PrimaryKey(autoGenerate = true) var id: Long = 0L,
    @ColumnInfo(name = "final_grade") var finalGrade: Double? = null,
    @ColumnInfo(name = "grade_comment") var gradeComment: String? = null,
    @ColumnInfo(name = "previous_subject") var previousSubject: Long? = null,
    @NonNull var colour: Int = 0,
    @ColumnInfo(name = "repeat_subject") var repeatSubject: Long? = null,
    @NonNull @ColumnInfo(name = "repeat_mode") var repeatMode: Boolean = false
) {
    override fun toString(): String {
        return name
    }
}