/*
 * MIT License
 *
 * Copyright (c) 2022 Cosme José Nieto Pérez
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */



package com.cosmejosenp.studentdiary.data.dao

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.room.*
import com.cosmejosenp.studentdiary.data.entity.Element
import com.cosmejosenp.studentdiary.data.entity.ElementWithAllInformation
import com.cosmejosenp.studentdiary.util.ListElementsTypes
import com.cosmejosenp.studentdiary.util.TYPE_EXAM
import com.cosmejosenp.studentdiary.util.TYPE_WORK
import java.util.*

@Dao
abstract class ElementDao {
    /**
     * @param element id.
     * @return an observable element.
     */
    @Query("SELECT * FROM ELEMENTS WHERE id=:element")
    abstract fun get(element: Long): LiveData<Element>

    /**
     * @param element id.
     * @return an observable element with all information.
     */
    @Transaction
    @Query("SELECT * FROM ELEMENTS WHERE id=:element")
    abstract fun getWithAllInformation(element: Long): LiveData<ElementWithAllInformation>

    // All active lists
    /**
     * @param date default now.
     * @return an observable active element list from this [date].
     */
    @Query(
        """SELECT * FROM ELEMENTS WHERE 
            completed=0 AND (delivery_date>:date OR delivery_date IS NULL) 
            AND block IN (
        SELECT b.id FROM BLOCKS as b 
        LEFT JOIN SUB_SUBJECTS as ssb ON b.sub_subject=ssb.id 
        LEFT JOIN SUBJECTS as sb ON ssb.subject=sb.id 
        LEFT JOIN SCHOOL_YEARS as sy ON sb.school_year=sy.id 
        WHERE ssb.active=1 AND sy.active=1)
        ORDER BY delivery_date DESC"""
    )
    abstract fun getAllActive(
        date: Calendar = Calendar.getInstance()
    ): LiveData<List<Element>>

    /**
     * @param subject id.
     * @param date default now.
     * @return an observable active element list of this [subject] from this [date].
     */
    @Query(
        """SELECT * FROM ELEMENTS WHERE 
            completed=0 AND (delivery_date>:date OR delivery_date IS NULL)
            AND block IN (
        SELECT id FROM BLOCKS WHERE sub_subject IN (
        SELECT id FROM SUB_SUBJECTS WHERE subject=:subject))
        ORDER BY delivery_date DESC"""
    )
    abstract fun getAllActiveFromSubject(
        subject: Long,
        date: Calendar = Calendar.getInstance()
    ): LiveData<List<Element>>

    /**
     * @param subSubject id.
     * @param date default now.
     * @return an observable active element list of this [subSubject] from this [date].
     */
    @Query(
        """SELECT * FROM ELEMENTS WHERE 
            completed=0 AND (delivery_date>:date OR delivery_date IS NULL)
            AND block IN (
        SELECT id FROM BLOCKS WHERE sub_subject = :subSubject)
        ORDER BY delivery_date DESC"""
    )
    abstract fun getAllActiveFromSubSubject(
        subSubject: Long,
        date: Calendar = Calendar.getInstance()
    ): LiveData<List<Element>>

    /**
     * @param block id.
     * @param date default now.
     * @return an observable active element list of this [block] from this [date].
     */
    @Query(
        """SELECT * FROM ELEMENTS WHERE block=:block 
        AND (delivery_date>:date OR delivery_date IS NULL)
        AND completed=0 ORDER BY delivery_date DESC"""
    )
    abstract fun getAllActiveFromBlock(
        block: Long,
        date: Calendar = Calendar.getInstance()
    ): LiveData<List<Element>>

    /**
     * @param subBlock id for search elements.
     * @param date default now.
     * @return an observable active element list of this [subBlock] from this [date].
     */
    @Query(
        """SELECT * FROM ELEMENTS WHERE sub_block=:subBlock 
        AND (delivery_date>:date OR delivery_date IS NULL)
        AND completed=0 ORDER BY delivery_date DESC"""
    )
    abstract fun getAllActiveFromSubBlock(
        subBlock: Long,
        date: Calendar = Calendar.getInstance()
    ): LiveData<List<Element>>

    // All active delayed lists
    /**
     * @param date default now.
     * @return an observable active and delayed element list from this [date].
     */
    @Query(
        """SELECT * FROM ELEMENTS WHERE completed=0 
            AND delivery_date IS NOT NULL AND delivery_date<:date 
            AND block IN (
        SELECT b.id FROM BLOCKS as b 
        LEFT JOIN SUB_SUBJECTS as ssb ON b.sub_subject=ssb.id 
        LEFT JOIN SUBJECTS as sb ON ssb.subject=sb.id 
        LEFT JOIN SCHOOL_YEARS as sy ON sb.school_year=sy.id 
        WHERE ssb.active=1 AND sy.active=1)
        ORDER BY delivery_date DESC"""
    )
    abstract fun getAllActiveDelayed(
        date: Calendar = Calendar.getInstance()
    ): LiveData<List<Element>>

    /**
     * @param subject id.
     * @param date default now.
     * @return an observable active and delayed element list of this [subject] from this [date].
     */
    @Query(
        """SELECT * FROM ELEMENTS WHERE completed=0 
            AND delivery_date IS NOT NULL AND delivery_date<:date 
            AND block IN (
        SELECT id FROM BLOCKS WHERE sub_subject IN (
        SELECT id FROM SUB_SUBJECTS WHERE subject=:subject))
        ORDER BY delivery_date DESC"""
    )
    abstract fun getAllActiveDelayedFromSubject(
        subject: Long,
        date: Calendar = Calendar.getInstance()
    ): LiveData<List<Element>>

    /**
     * @param subSubject id.
     * @param date default now.
     * @return an observable active and delayed element list of this [subSubject] from this [date].
     */
    @Query(
        """SELECT * FROM ELEMENTS WHERE completed=0 
            AND delivery_date IS NOT NULL AND delivery_date<:date 
            AND block IN (
        SELECT id FROM BLOCKS WHERE sub_subject=:subSubject)
        ORDER BY delivery_date DESC"""
    )
    abstract fun getAllActiveDelayedFromSubSubject(
        subSubject: Long,
        date: Calendar = Calendar.getInstance()
    ): LiveData<List<Element>>

    /**
     * @param block id.
     * @param date default now.
     * @return an observable active and delayed element list of this [block] from this [date].
     */
    @Query(
        """SELECT * FROM ELEMENTS WHERE completed=0 
            AND delivery_date IS NOT NULL AND delivery_date<:date 
            AND block =:block
        ORDER BY delivery_date DESC"""
    )
    abstract fun getAllActiveDelayedFromBlock(
        block: Long,
        date: Calendar = Calendar.getInstance()
    ): LiveData<List<Element>>

    /**
     * @param subBlock id.
     * @param date default now.
     * @return an observable active and delayed element list of this [subBlock] from this [date].
     */
    @Query(
        """SELECT * FROM ELEMENTS WHERE completed=0 
            AND delivery_date IS NOT NULL AND delivery_date<:date 
            AND sub_block =:subBlock
        ORDER BY delivery_date DESC"""
    )
    abstract fun getAllActiveDelayedFromSubBlock(
        subBlock: Long,
        date: Calendar = Calendar.getInstance()
    ): LiveData<List<Element>>

    // All finished lists
    /**
     * @param subject id.
     * @return an observable finished element list of this [subject].
     */
    @Query(
        """SELECT * FROM ELEMENTS WHERE 
            completed=1 AND block IN (
        SELECT id FROM BLOCKS WHERE sub_subject IN (
        SELECT id FROM SUB_SUBJECTS WHERE subject=:subject))
        ORDER BY delivery_date DESC"""
    )
    abstract fun getAllCompletedFromSubject(
        subject: Long
    ): LiveData<List<Element>>

    /**
     * @param subSubject id.
     * @return an observable finished element list of this [subSubject].
     */
    @Query(
        """SELECT * FROM ELEMENTS WHERE 
            completed=1 AND block IN (
        SELECT id FROM BLOCKS WHERE sub_subject=:subSubject)
        ORDER BY delivery_date DESC"""
    )
    abstract fun getAllCompletedFromSubSubject(
        subSubject: Long
    ): LiveData<List<Element>>

    /**
     * @param block id.
     * @return an observable finished element list of this [block].
     */
    @Query("SELECT * FROM ELEMENTS WHERE block=:block AND completed=1 ORDER BY delivery_date DESC")
    abstract fun getAllCompletedFromBlock(block: Long): LiveData<List<Element>>

    /**
     * @param subBlock id.
     * @return an observable finished element list of this [subBlock].
     */
    @Query("SELECT * FROM ELEMENTS WHERE sub_block=:subBlock AND completed=1 ORDER BY delivery_date DESC")
    abstract fun getAllCompletedFromSubBlock(subBlock: Long): LiveData<List<Element>>

    // All without grade lists
    /**
     * @param type of the elements.
     * @return an observable element without grade list of this [type].
     */
    @Query(
        """SELECT * FROM ELEMENTS WHERE 
            grade IS NULL AND type in (:type)
            AND completed = 1
            AND block IN (
        SELECT b.id FROM BLOCKS as b 
        LEFT JOIN SUB_SUBJECTS as ssb ON b.sub_subject=ssb.id 
        LEFT JOIN SUBJECTS as sb ON ssb.subject=sb.id 
        LEFT JOIN SCHOOL_YEARS as sy ON sb.school_year=sy.id 
        WHERE sy.active=1)
        ORDER BY delivery_date DESC"""
    )
    abstract fun getAllWithoutGrade(vararg type: Int): LiveData<List<Element>>

    /**
     * @param subject id.
     * @param type of the elements.
     * @return an observable element without grade list of this [subject] and [type].
     */
    @Query(
        """SELECT * FROM ELEMENTS WHERE 
            grade IS NULL AND type in (:type)
            AND completed = 1
            AND block IN (
        SELECT id FROM BLOCKS WHERE sub_subject IN (
        SELECT id FROM SUB_SUBJECTS WHERE subject=:subject))
        ORDER BY delivery_date DESC"""
    )
    abstract fun getAllWithoutGradeFromSubject(
        subject: Long,
        vararg type: Int
    ): LiveData<List<Element>>

    /**
     * @param subSubject id.
     * @param type of the elements.
     * @return an observable elements without grade list of this [subSubject] and [type].
     */
    @Query(
        """SELECT * FROM ELEMENTS WHERE 
            grade IS NULL AND type in (:type)
            AND completed = 1
            AND block IN (
        SELECT id FROM BLOCKS WHERE sub_subject =:subSubject)
        ORDER BY delivery_date DESC"""
    )
    abstract fun getAllWithoutGradeFromSubSubject(
        subSubject: Long,
        vararg type: Int
    ): LiveData<List<Element>>

    /**
     * @param block id.
     * @param type of the elements.
     * @return an observable element without grade list of this [block] and [type].
     */
    @Query(
        """SELECT * FROM ELEMENTS WHERE 
            grade IS NULL AND type in (:type)
            AND completed = 1
            AND block=:block
        ORDER BY delivery_date DESC"""
    )
    abstract fun getAllWithoutGradeFromBlock(
        block: Long,
        vararg type: Int
    ): LiveData<List<Element>>

    /**
     * @param subBlock id.
     * @param type of the elements.
     * @return an observable element without grade list of this [subBlock] and [type].
     */
    @Query(
        """SELECT * FROM ELEMENTS WHERE 
            grade IS NULL AND type in (:type)
            AND completed = 1
            AND sub_block=:subBlock
        ORDER BY delivery_date DESC"""
    )
    abstract fun getAllWithoutGradeFromSubBlock(
        subBlock: Long,
        vararg type: Int
    ): LiveData<List<Element>>

    // Average methods.
    /**
     * @param block of the elements to calculate the average.
     * @return average of this [block].
     */
    @Query("SELECT AVG(grade*multiplier) FROM ELEMENTS WHERE block=:block")
    abstract fun getAverage(block: Long): Double?

    /**
     * @param subBlock of the elements to calculate the average.
     * @return average of this [subBlock].
     */
    @Query("SELECT AVG(grade*multiplier) FROM ELEMENTS WHERE sub_block=:subBlock")
    abstract fun getAverageSubBlock(subBlock: Long): Double?

    /**
     * @param block of the elements to calculate the how much task are completed and calculate
     * the average.
     * @return average of this [block].
     */
    @Query(
        """SELECT (
        SELECT COUNT(*) FROM ELEMENTS WHERE block=:block 
        AND completed=1 AND (delivery_date IS NULL OR delivery_date>completed_date))
        *10/COUNT(*) FROM ELEMENTS WHERE block=:block"""
    )
    abstract fun getAverageTask(block: Long): Double?

    /**
     * @param subBlock of the elements to calculate the how much are completed and calculate
     * the average.
     * @return average of this [subBlock].
     */
    @Query(
        """SELECT (
        SELECT COUNT(*) FROM ELEMENTS WHERE sub_block=:subBlock 
        AND completed=1 AND (delivery_date IS NULL OR delivery_date>completed_date))
        *10/COUNT(*) FROM ELEMENTS WHERE sub_block=:subBlock"""
    )
    abstract fun getAverageTaskSubBlock(subBlock: Long): Double?

    // Service methods
    /**
     * @param date default now.
     * @param typeList types of the elements to search, default only [TYPE_EXAM].
     * @return the elements that are active and delayed of this [typeList] from this [date].
     */
    @Query(
        """SELECT * FROM ELEMENTS WHERE completed=0 
            AND delivery_date IS NOT NULL AND delivery_date<:date 
            AND type IN (:typeList)
            AND block IN (
        SELECT b.id FROM BLOCKS as b 
        LEFT JOIN SUB_SUBJECTS as ssb ON b.sub_subject=ssb.id 
        LEFT JOIN SUBJECTS as sb ON ssb.subject=sb.id 
        LEFT JOIN SCHOOL_YEARS as sy ON sb.school_year=sy.id 
        WHERE ssb.active=1 AND sy.active=1)
        ORDER BY delivery_date DESC"""
    )
    protected abstract fun getActiveDelayedFromType(
        date: Calendar = Calendar.getInstance(),
        typeList: List<Int> = listOf(TYPE_EXAM)
    ): List<Element>

    // Generic Methods
    /**
     * @param element to add
     */
    @Insert
    abstract fun add(vararg element: Element): List<Long>

    /**
     * @param element to update
     */
    @Update
    abstract fun update(vararg element: Element)

    /**
     * @param element to delete
     */
    @Delete
    abstract fun delete(vararg element: Element)

    /**
     * Generic method to obtain an observable list of elements depending of the [typeList] and
     * the optional values provided.
     *
     * @param typeList of the elements list.
     * @param subject id.
     * @param subSubject id.
     * @param block id.
     * @param subBlock id.
     * @return an observable element list of this [typeList] that depends of
     * the optional values provided.
     */
    open fun getAll(
        typeList: ListElementsTypes,
        subject: Long,
        subSubject: Long,
        block: Long,
        subBlock: Long
    ): LiveData<List<Element>> {
        return when (typeList) {
            ListElementsTypes.ACTIVE -> when {
                subject != 0L -> getAllActiveFromSubject(subject)
                subSubject != 0L -> getAllActiveFromSubSubject(subSubject)
                block != 0L -> getAllActiveFromBlock(block)
                subBlock != 0L -> getAllActiveFromSubBlock(subBlock)
                else -> getAllActive()
            }
            ListElementsTypes.PENDING -> when {
                subject != 0L -> getAllActiveDelayedFromSubject(subject)
                subSubject != 0L -> getAllActiveDelayedFromSubSubject(subSubject)
                block != 0L -> getAllActiveDelayedFromBlock(block)
                subBlock != 0L -> getAllActiveDelayedFromSubBlock(subBlock)
                else -> getAllActiveDelayed()
            }
            ListElementsTypes.PENDING_GRADES -> when {
                subject != 0L -> getAllWithoutGradeFromSubject(
                    subject,
                    TYPE_EXAM,
                    TYPE_WORK
                )
                subSubject != 0L -> getAllWithoutGradeFromSubSubject(
                    subSubject, TYPE_EXAM,
                    TYPE_WORK
                )
                block != 0L -> getAllWithoutGradeFromBlock(block, TYPE_EXAM, TYPE_WORK)
                subBlock != 0L -> getAllWithoutGradeFromSubBlock(
                    subBlock, TYPE_EXAM,
                    TYPE_WORK
                )
                else -> getAllWithoutGrade(TYPE_EXAM, TYPE_WORK)
            }
            ListElementsTypes.COMPLETED -> when {
                subject != 0L -> getAllCompletedFromSubject(subject)
                subSubject != 0L -> getAllCompletedFromSubSubject(subSubject)
                block != 0L -> getAllCompletedFromBlock(block)
                subBlock != 0L -> getAllCompletedFromSubBlock(subBlock)
                else -> MutableLiveData()
            }
        }
    }

    /**
     * Method that update the delivered date of elements of type [TYPE_EXAM]
     * and make it as complete before they will transform into a delayed item.
     */
    @Transaction
    open fun updateSpecialElements() {
        val elements = getActiveDelayedFromType()
        for (element in elements) {
            element.completed = true
            element.deliveryDate = element.completedDate
        }
        if (elements.isNotEmpty())
            update(*elements.toTypedArray())
    }
}