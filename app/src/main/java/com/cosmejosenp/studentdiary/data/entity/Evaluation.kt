/*
 * MIT License
 *
 * Copyright (c) 2022 Cosme José Nieto Pérez
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */



package com.cosmejosenp.studentdiary.data.entity

import androidx.annotation.NonNull
import androidx.room.*
import java.util.*

@Entity(
    tableName = "EVALUATIONS",
    indices = [Index("school_year")],
    foreignKeys = [ForeignKey(
        entity = SchoolYear::class, parentColumns = ["id"],
        childColumns = ["school_year"],
        onDelete = ForeignKey.CASCADE,
        onUpdate = ForeignKey.CASCADE
    )]
)
data class Evaluation(
    @NonNull @ColumnInfo(name = "school_year") var schoolYear: Long,
    @NonNull var name: String,
    @NonNull var position: Int,
    @PrimaryKey(autoGenerate = true) var id: Long = 0L,
    @ColumnInfo(name = "start_date") var startDate: Calendar? = null,
    @ColumnInfo(name = "finish_date") var finishDate: Calendar? = null,
    @NonNull @ColumnInfo(name = "extra_ordinary") var extraOrdinary: Boolean = false
) {
    override fun toString(): String {
        return name
    }
}