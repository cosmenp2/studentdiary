<?xml version="1.0" encoding="utf-8"?>
<!--
MIT License

Copyright (c) 2022 Cosme José Nieto Pérez

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
-->

<layout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools">

    <data>

        <variable
            name="viewModel"
            type="com.cosmejosenp.studentdiary.viewmodels.DetailElementViewModel" />
    </data>

    <androidx.coordinatorlayout.widget.CoordinatorLayout
        android:id="@+id/detailElementCoordinatorLayout"
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:background="@color/primaryColor">

        <com.google.android.material.appbar.AppBarLayout
            android:id="@+id/detailElementAppbar"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toTopOf="parent"
            app:liftOnScroll="true">

            <androidx.appcompat.widget.Toolbar
                android:id="@+id/detailElementToolbar"
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                app:title="@{viewModel.title}" />
        </com.google.android.material.appbar.AppBarLayout>

        <androidx.core.widget.NestedScrollView
            android:layout_width="match_parent"
            android:layout_height="match_parent"
            app:layout_behavior="com.google.android.material.appbar.AppBarLayout$ScrollingViewBehavior"
            app:layout_constraintBottom_toBottomOf="parent"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toBottomOf="@+id/detailElementAppbar">

            <androidx.constraintlayout.widget.ConstraintLayout
                android:id="@+id/linearLayout"
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:orientation="vertical">

                <TextView
                    android:id="@+id/detailElementName"
                    android:layout_width="0dp"
                    android:layout_height="wrap_content"
                    android:layout_marginStart="16dp"
                    android:layout_marginLeft="16dp"
                    android:layout_marginTop="16dp"
                    android:layout_marginEnd="16dp"
                    android:layout_marginRight="16dp"
                    android:ems="10"
                    android:textAppearance="@style/TextAppearance.AppCompat.Large"
                    app:layout_constraintEnd_toEndOf="parent"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toTopOf="parent"
                    app:textAndVisibility="@{viewModel.element.name}"
                    tools:layout_conversion_absoluteHeight="82dp"
                    tools:layout_conversion_absoluteWidth="0dp" />

                <TextView
                    android:id="@+id/detailElementDateTextView"
                    android:layout_width="0dp"
                    android:layout_height="wrap_content"
                    android:layout_marginStart="16dp"
                    android:layout_marginLeft="16dp"
                    android:layout_marginTop="16dp"
                    android:layout_marginEnd="16dp"
                    android:layout_marginRight="16dp"
                    android:textAppearance="@style/TextAppearance.AppCompat.Medium"
                    android:textColor="@{viewModel.delayed}"
                    app:deliveryDateString="@{viewModel.element.deliveryDate}"
                    app:layout_constraintEnd_toEndOf="parent"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toBottomOf="@+id/detailElementName" />

                <TextView
                    android:id="@+id/detailElementDateDeliveredTextView"
                    android:layout_width="0dp"
                    android:layout_height="wrap_content"
                    android:layout_marginStart="16dp"
                    android:layout_marginLeft="16dp"
                    android:layout_marginTop="16dp"
                    android:layout_marginEnd="16dp"
                    android:layout_marginRight="16dp"
                    android:textAppearance="@style/TextAppearance.AppCompat.Medium"
                    app:completedDateString="@{viewModel.element.completedDate}"
                    app:layout_constraintEnd_toEndOf="parent"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toBottomOf="@+id/detailElementBarrierDeliveredDate" />

                <TextView
                    android:id="@+id/detailElementDescription"
                    android:layout_width="0dp"
                    android:layout_height="wrap_content"
                    android:layout_marginStart="16dp"
                    android:layout_marginLeft="16dp"
                    android:layout_marginTop="16dp"
                    android:layout_marginEnd="16dp"
                    android:layout_marginRight="16dp"
                    android:ems="10"
                    android:textAppearance="@style/TextAppearance.AppCompat.Small"
                    app:layout_constraintEnd_toEndOf="parent"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toBottomOf="@+id/detailElementBarrier"
                    app:textAndVisibility="@{viewModel.element.description}" />

                <androidx.constraintlayout.widget.Barrier
                    android:id="@+id/detailElementBarrier"
                    android:layout_width="wrap_content"
                    android:layout_height="wrap_content"
                    app:barrierDirection="bottom"
                    app:constraint_referenced_ids="detailElementName,detailElementDateTextView,detailElementDateDeliveredTextView"
                    tools:layout_editor_absoluteY="121dp" />

                <androidx.constraintlayout.widget.Barrier
                    android:id="@+id/detailElementBarrierDeliveredDate"
                    android:layout_width="wrap_content"
                    android:layout_height="wrap_content"
                    app:barrierDirection="bottom"
                    app:constraint_referenced_ids="detailElementName,detailElementDateTextView"
                    tools:layout_editor_absoluteY="121dp" />

                <androidx.constraintlayout.widget.Barrier
                    android:id="@+id/detailElementDetailsBarrier"
                    android:layout_width="wrap_content"
                    android:layout_height="wrap_content"
                    app:barrierDirection="bottom"
                    app:constraint_referenced_ids="detailElementDescription,detailElementDateTextView,detailElementDateDeliveredTextView,detailElementName"
                    tools:layout_editor_absoluteY="121dp" />

            </androidx.constraintlayout.widget.ConstraintLayout>
        </androidx.core.widget.NestedScrollView>

    </androidx.coordinatorlayout.widget.CoordinatorLayout>


</layout>