<?xml version="1.0" encoding="utf-8"?>
<!--
MIT License

Copyright (c) 2022 Cosme José Nieto Pérez

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
-->


<layout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools">

    <data>

        <variable
            name="evaluation"
            type="com.cosmejosenp.studentdiary.data.entity.Evaluation" />
    </data>

    <androidx.constraintlayout.widget.ConstraintLayout
        android:id="@+id/listItemEvaluationItemContainer"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:background="@drawable/selectable_item_background">

        <TextView
            android:id="@+id/listItemEvaluationTitle"
            android:layout_width="0dp"
            android:layout_height="wrap_content"
            android:layout_marginStart="16dp"
            android:layout_marginLeft="16dp"
            android:layout_marginTop="16dp"
            android:layout_marginEnd="16dp"
            android:layout_marginRight="16dp"
            android:layout_marginBottom="16dp"
            android:ellipsize="end"
            android:singleLine="true"
            android:text="@{evaluation.name}"
            android:textAppearance="@style/TextAppearance.AppCompat.Large"
            android:textColor="@drawable/selectable_item_color"
            app:layout_constraintBottom_toBottomOf="parent"
            app:layout_constraintEnd_toStartOf="@+id/listItemEvaluationBarrier"
            app:layout_constraintStart_toEndOf="@+id/listItemEvaluationImageSelected"
            app:layout_constraintTop_toTopOf="parent"
            app:layout_constraintVertical_bias="0.0" />

        <TextView
            android:id="@+id/listItemEvaluationStartDate"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:layout_marginStart="16dp"
            android:layout_marginLeft="16dp"
            android:layout_marginTop="16dp"
            android:textAppearance="@style/TextAppearance.AppCompat.Small"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toBottomOf="@+id/listItemEvaluationTitle"
            app:startDateString="@{evaluation.startDate}" />

        <TextView
            android:id="@+id/listItemEvaluationActiveText"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:layout_marginEnd="16dp"
            android:layout_marginRight="16dp"
            android:text="@string/active"
            android:textAppearance="@style/TextAppearance.AppCompat.Medium"
            app:layout_constraintBottom_toBottomOf="@+id/listItemEvaluationTitle"
            app:layout_constraintEnd_toEndOf="@+id/listItemEvaluationTitle"
            app:layout_constraintEnd_toStartOf="@+id/listItemEvaluationExtraOrdinaryText"
            app:layout_constraintTop_toTopOf="@+id/listItemEvaluationTitle" />

        <TextView
            android:id="@+id/listItemEvaluationExtraOrdinaryText"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:layout_marginEnd="16dp"
            android:layout_marginRight="16dp"
            android:text="@string/extraOrdinaryAbv"
            android:textAppearance="@style/TextAppearance.AppCompat.Medium"
            app:layout_constraintBottom_toBottomOf="@+id/listItemEvaluationTitle"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintTop_toTopOf="@+id/listItemEvaluationTitle" />

        <TextView
            android:id="@+id/listItemEvaluationFinishDate"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:layout_marginTop="16dp"
            android:layout_marginEnd="16dp"
            android:layout_marginRight="16dp"
            android:textAppearance="@style/TextAppearance.AppCompat.Small"
            app:finishDateString="@{evaluation.finishDate}"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintTop_toBottomOf="@+id/listItemEvaluationTitle" />

        <View
            android:id="@+id/divider"
            android:layout_width="0dp"
            android:layout_height="1dp"
            android:background="?android:attr/listDivider"
            app:layout_constraintBottom_toBottomOf="parent"
            app:layout_constraintStart_toStartOf="parent" />

        <ImageView
            android:id="@+id/listItemEvaluationImageSelected"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:layout_marginStart="16dp"
            android:layout_marginLeft="16dp"
            android:layout_marginTop="16dp"
            android:contentDescription="@string/selected"
            android:tint="@color/secondaryColor"
            android:visibility="gone"
            app:layout_constraintBottom_toBottomOf="@+id/listItemEvaluationTitle"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toTopOf="parent"
            app:srcCompat="@drawable/check_circle_black_24dp" />

        <androidx.constraintlayout.widget.Barrier
            android:id="@+id/listItemEvaluationBarrier"
            android:layout_width="wrap_content"
            android:layout_height="0dp"
            app:barrierDirection="start"
            app:constraint_referenced_ids="listItemEvaluationExtraOrdinaryText,listItemEvaluationActiveText"
            tools:layout_editor_absoluteX="395dp" />
    </androidx.constraintlayout.widget.ConstraintLayout>
</layout>