<?xml version="1.0" encoding="utf-8"?>
<!--
MIT License

Copyright (c) 2022 Cosme José Nieto Pérez

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
-->

<resources>
    <string name="app_name">Student diary</string>
    <string name="title_activity_main">Student diary</string>
    <string name="next">Siguiente</string>
    <string name="previous">Anterior</string>
    <string name="addSchoolYear">Nuevo curso</string>
    <string name="emptyActiveSchoolYear">No hay ningún curso activo.\nPulse + para crear uno nuevo.</string>
    <string name="emptyFinishedSchoolYear">No hay ningún curso finalizado.</string>
    <string name="emptyEvaluationsSchoolYear">No hay ninguna evaluación en este curso.</string>
    <string name="emptyEvaluationsSubject">Esta asignatura no esta enlazada a ninguna evaluación.</string>
    <string name="emptySubjectsSchoolYear">No hay ninguna asignatura en este curso.</string>
    <string name="emptySubjectsEvaluation">No hay ninguna asignatura en esta evaluación.</string>
    <string name="emptyBlocks">No hay ningún bloque en esta evaluación de la asignatura.</string>
    <string name="emptySubBlocks">No hay ningún sub bloque en este bloque.</string>
    <string name="emptyActiveElements">¡¡Enhorabuena!! Parece que has terminado todo el trabajo.</string>
    <string name="emptyActiveDelayedElements">¡¡Enhorabuena!! Parece que estas al día.</string>
    <string name="emptyPendingGradesElements">Todas las notas están actualizadas.</string>
    <string name="emptyCompletedElements">No hay ningún elemento aquí.</string>
    <string name="startDate">Iniciado: %s</string>
    <string name="finishDate">Finalizado: %s</string>
    <string name="deliveryDateWithDate">Fecha de entrega: %s</string>
    <string name="deliveredDateWithDate">Entregado en: %s</string>
    <string name="dateFormatted">%2$d/%1$d/%3$d</string>
    <string name="delete">Eliminar</string>
    <string name="markAsComplete">Marcar como completado</string>
    <string name="markAsIncomplete">Marcar como incompleto</string>
    <string name="markAsActive">Activo</string>
    <string name="markAsFinishedAndActiveNext">Finalizar y activar el siguiente</string>
    <string name="deselectAll">Deseleccionar todo</string>
    <string name="selected">Elemento seleccionado</string>
    <string name="finishQuestion">¿Marcar como completado?</string>
    <string name="incompleteQuestion">¿Marcar como incompleto?</string>
    <string name="updateStatusSchoolYearToFinishedQuestion">¿Actualizar el estado del curso?</string>
    <string name="enableCloneModeQuestion">¿Activar modo clonación?</string>
    <string name="deleteQuestion">¿Eliminar?</string>
    <string name="selectedTitle">%d seleccionado</string>
    <string name="selectedTitlePlural">%d seleccionados</string>
    <string name="nameOfEvaluation">Evaluación %d</string>
    <string name="nameOfExtraordinaryEvaluation">Evaluación extraordinaria</string>
    <string name="discardQuestion">¿Descartar cambios?</string>
    <string name="discard">descartar</string>
    <string name="continueEditing">Continuar editando</string>
    <string name="defaultColour">Color por defecto</string>
    <string name="salmonColour">Color salmón</string>
    <string name="pinkColour">Color rosa</string>
    <string name="violetColour">Color violeta</string>
    <string name="blueColour">Color azul</string>
    <string name="greenColour">Color verde</string>
    <string name="yellowColour">Color amarillo</string>
    <string name="amberColour">Color ambar</string>
    <string name="orangeColour">Color naranja</string>
    <string name="deepOrangeColour">Color naranja oscuro</string>
    <string name="grayColour">Color gris</string>
    <string name="colorSelected">%s seleccionado</string>
    <string name="nameOfUnit">Unidad %d</string>
    <string name="examsName">Exámenes</string>
    <string name="examName">Examen</string>
    <string name="worksName">Trabajos</string>
    <string name="workName">Trabajo</string>
    <string name="homeworksName">Tareas</string>
    <string name="homeworkName">Tarea</string>
    <string name="otherName">Otros</string>
    <string name="percent">%d %%</string>

    <!--    Explanations-->
    <string name="deleteSchoolYearExplanation">Esta acción eliminará todas las asignaturas, evaluaciones y elementos
        relacionados con los cursos seleccionados.\nEsta acción no se puede deshacer.</string>
    <string name="deleteEvaluationExplanation">Esta acción eliminará todo el contenido relacionado
        con las evaluaciones seleccionadas.\nEsta acción no se puede deshacer.</string>
    <string name="deleteSubjectExplanation">Esta acción eliminará todo el contenido relacionado
        con las asignaturas seleccionadas.\nEsta acción no se puede deshacer.</string>
    <string name="deleteBlockExplanation">Esta acción eliminará todo el contenido relacionado
        con los bloques seleccionados.\nEsta acción no se puede deshacer.</string>
    <string name="deleteSubSubjectExplanationFromEvaluation">Esta acción desenlazará y eliminará todo el contenido
        relacionado con las asignaturas seleccionadas de esta evaluación.\nEsta acción no se puede deshacer.</string>
    <string name="deleteSubSubjectExplanationFromSubject">Esta acción desenlazará y eliminará todo el contenido
        relacionado con las evaluaciones seleccionadas de esta asignatura.\nEsta acción no se puede deshacer.</string>
    <string name="deleteElementExplanation">Esta acción no se puede deshacer.</string>
    <string name="updateToFinishSchoolYearExplanation">Esta acción marcará las asignaturas relacionadas con los cursos
        seleccionados como finalizados y los archivará con todo su contenido relacionado.</string>
    <string name="updateToActiveSchoolYearExplanation">Esta acción marcará solo los cursos seleccionados como incompletos
        y como activos.</string>
    <string name="updateToFinishSchoolYearExplanationSingular">Esta acción marcará las asignaturas relacionadas con
        este curso como finalizados y archivará este curso con todo su contenido relacionado.</string>
    <string name="updateToActiveSchoolYearExplanationSingular">Esta acción marcará solo el curso como incompleto
        y como activo.</string>
    <string name="updateToFinishEvaluationExplanation">Esta acción marcará las asignaturas relacionadas con esta
        evaluación como finalizadas y activará la siguiente evaluación y asignaturas si esta disponible o
        si no marcará el curso como finalizado.</string>
    <string name="updateToActiveEvaluationExplanation">Esta acción marcará esta y las siguientes
    evaluaciones de este curso como activas.</string>
    <string name="enableCloneModeExplanation">El modo clonación <b>eliminará</b> y creará la estructura
    para hacer que la estructura final sea identica a esta.</string>
    <string name="disabledCloneModeExplanation">Copia y modifica partes de la estructura para hacer que
        la estructura final se parezca a esta sin perder datos.</string>
    <string name="enabledCloneModeExplanation"><b>Eliminará</b> ay creará la estructura
    para hacer que la estructura final sea identica a esta.</string>
    <string name="errorSumPercentages">Los porcentajes deben de sumar 100</string>
    <string name="errorRangePercentage">Los porcentajes deben estar entre 0 y 100.
        Puedes dejar este campo vacio.</string>
    <string name="schoolYearActivated">Este curso se ha activado</string>
    <string name="schoolYearFinished">Este curso se ha completado</string>

    <!--    Handle errors-->
    <string name="errorUpdate">Ha ocurrido un error al guardar cambios</string>
    <string name="errorDelete">Ha ocurrido un error al borrar datos</string>
    <string name="errorSaveSubSubjects">Ha ocurrido un error al enlazar la asignatura con las evaluaciones</string>
    <string name="errorSaveSubSubject">Ha ocurrido un error al enlazar la evaluación con la asignatura %s</string>
    <string name="errorCreateStructure">Ha ocurrido un error al crear la estructura de la asignatura</string>
    <string name="errorCreateStructureOf">Ha ocurrido un error al crear la estructura de la asignatura %s</string>

    <!--    Titles and names of items-->
    <string name="SchoolYears">Cursos</string>
    <string name="newSchoolYear">Nuevo curso</string>
    <string name="showAllSchoolYear">Todos los cursos</string>
    <string name="finished">Finalizados</string>
    <string name="current">Actuales</string>
    <string name="titleAddSchoolYear">Nuevo curso</string>
    <string name="Name">Nombre</string>
    <string name="PickerEvaluations">Selecciona el número de evaluaciones</string>
    <string name="NumberEvaluation">Número de evaluaciones</string>
    <string name="CreateExtraordinaryEvaluation">Crear evaluacion extraordinaria</string>
    <string name="save">Guardar</string>
    <string name="evaluations">Evaluaciones</string>
    <string name="blocks">Bloques</string>
    <string name="subBlocks">Sub bloques</string>
    <string name="subjects">Asignaturas</string>
    <string name="extraOrdinaryAbv">Extra</string>
    <string name="extraOrdinary">Extraordinaria</string>
    <string name="edit">Editar</string>
    <string name="positionEvaluation">Posición de la evaluación</string>
    <string name="subjectStructure">Estructura general</string>
    <string name="standardStructure">Exámenes, trabajos y tareas</string>
    <string name="unitsWithStandardStructure">Unidades divididas en divided exámenes, trabajos y tareas</string>
    <string name="personalized">Personalizada</string>
    <string name="defaultValueExams">70</string>
    <string name="defaultValueExamsExtraordinary">80</string>
    <string name="percentExams">\% E</string>
    <string name="defaultValueWorks">20</string>
    <string name="defaultValueWorksExtraordinary">20</string>
    <string name="percentWorks">\% Tr</string>
    <string name="defaultValueHomework">10</string>
    <string name="percentHomework">\% Ta</string>
    <string name="subjectStructureExtraordinary">Estructura en las evaluaciones extraordinarias</string>
    <string name="standardStructureExtraordinary">Solo exámenes</string>
    <string name="workAndExamsExtraordinaryStructure">Trabajos y exámenes</string>
    <string name="continuousAssessment">Asignatura continua</string>
    <string name="newSubject">Nueva asignatura</string>
    <string name="copySubjectStructure">Copiar la estructura de la evaluación anterior si es posible</string>
    <string name="copySubjectExtraordinaryStructure">Copiar la estructura de alguna evaluación extraordinaria
        si es posible</string>
    <string name="newEvaluation">Nueva evaluación</string>
    <string name="linkNewEvaluations">Enlazar con evaluaciones</string>
    <string name="linkNewSubjects">Enlazar con asignaturas</string>
    <string name="evaluationStructure">Estructura de la evaluación</string>
    <string name="invisibleMenu">Menú invisible</string><!--¿Si es invisible por qué se traduce?-->
    <string name="copyStructureFrom">Copiar la estructura de la evaluación desde</string>
    <string name="copyStructureTo">Copiar la estructura de la evaluación a</string>
    <string name="copy">Copiar</string>
    <string name="structurePreview">Previsualización de la estructura</string>
    <string name="cloneMode">Modo clonación</string>
    <string name="copyToTitle">Copiar a</string>
    <string name="evaluationItem">Evaluación</string>
    <string name="StructureThisEvaluation">Estructura de esta evaluación</string>
    <string name="active">Activar</string>
    <string name="percentage">Porcentage</string>
    <string name="percentageHint">Porcentage (0 – 100)</string>
    <string name="percentageSuggestion">Si el bloque no tiene ningún porcentage la nota se calculará
        como la media de los bloques que no tengan porcentages respetando los bloques que tengan algún
        porcentaje definido.\nSimplemente deje este campo vacio.</string>
    <string name="typesAvailable">Tipos disponibles</string>
    <string name="unitsName">Unidad</string>
    <string name="newBlock">Nuevo bloque</string>
    <string name="percentageAdd">Suma total de los porcentages: %d%%</string>
    <string name="delayed">Atrasado</string>
    <string name="markedElementAsComplete">Marcar como completado</string>
    <string name="markedElementAsIncomplete">Marcar como incompleto</string>
    <string name="undo">Deshacer</string>
    <string name="home">Inicio</string>
    <string name="delayedActiveElements">Elementos atrasados</string>
    <string name="elementsWithoutGrade">Elementos sin nota</string>
    <string name="activePageElements">Activos</string>
    <string name="delayedPageElements">Atrasados</string>
    <string name="withoutGradePageElements">Sin nota</string>
    <string name="finishedPageElements">Finalizados</string>
    <string name="subject">Asignatura</string>
    <string name="block">Bloques</string>
    <string name="description">Descripción</string>
    <string name="addRequirement">Requisito</string>
    <string name="add">Añadir</string>
    <string name="date">Fecha</string>
    <string name="deliveryDate">Fecha de entrega</string>
    <string name="subBlock">Sub bloque</string>
    <string name="today">Hoy</string>
    <string name="tomorrow">Mañana</string>
    <string name="nextDay">Siguiente %s</string>
    <string name="dateOf">%s, %d de %s</string>
    <string name="pickADate">Seleccione fecha…</string>
    <string name="time">Hora</string>
    <string name="morning">Mañana</string>
    <string name="noon">Mediodía</string>
    <string name="afternoon">Tarde</string>
    <string name="night">Noche</string>
    <string name="pickATime">Seleccione hora…</string>
    <string name="gridMode">Vista de cuadrícula</string>
    <string name="listMode">Vista de lista</string>
    <string name="addGrade">Añadir nota</string>
    <string name="CopyFromTitle">Copiar desde</string>
    <string name="yes">Sí</string>
    <string name="no">No</string>
</resources>
